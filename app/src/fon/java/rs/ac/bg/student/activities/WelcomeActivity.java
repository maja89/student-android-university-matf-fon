package rs.ac.bg.student.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentActivity;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by maja on 4/21/18.
 */

public class WelcomeActivity extends BaseActivity {
    public static final String TAG = "WelcomeActivity";

    public static final String ACTION_SHOW_SECTION = "ACTION_SHOW_SECTION";

    /**
     * Screen delay
     */
    private static final int DELAY = 1000; // 1 second

    private LinearLayout mSectionLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mSectionLayout = (LinearLayout) findViewById(R.id.layoutSection);

        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                if (ACTION_SHOW_SECTION.equals(action)) {
                    mSectionLayout.setVisibility(View.VISIBLE);
                    return;
                }
            }
        }

        String sectionName = Preferences.getSectionName(getBaseContext());
        if (sectionName.isEmpty()) {
            mSectionLayout.setVisibility(View.VISIBLE);
        } else {
            showStudent();
        }
    }

    public void onSelectedSection1(View view) {
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            saveSectionName(Section.SECTION_1_ISIT);
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            saveSectionName(Section.SECTION_1_MATEMATIKA);
        }
    }

    public void onSelectedSection2(View view) {
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            saveSectionName(Section.SECTION_2_M);
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            saveSectionName(Section.SECTION_2_INFORMATIKA);
        }
    }

    public void onSelectedSection3(View view) {
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            saveSectionName(Section.SECTION_3_MKIS);
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            saveSectionName(Section.SECTION_3_AIA);
        }
    }

    public void onSelectedSection4(View view) {
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            saveSectionName(Section.SECTION_4_OM);
        }
    }

    private void saveSectionName(String sectionName) {
        Preferences.saveSectionName(getBaseContext(), sectionName);
        mSectionLayout.setVisibility(View.GONE);
        showStudent();
    }

    private void showStudent() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getBaseContext(), StudentActivity.class);
                startActivity(intent);

                finish();
            }
        }, DELAY);
    }

}
