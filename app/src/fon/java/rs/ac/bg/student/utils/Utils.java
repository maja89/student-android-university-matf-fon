package rs.ac.bg.student.utils;

import android.content.Context;

import java.util.ArrayList;

import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.Timetable;

/**
 * Created by maja on 5/15/18.
 */

public class Utils {

    public static void loadAssets(Context context) {
    }

    public static CalendarYear loadCalendarYear(Context context) {
        return null;
    }

    public static Timetable loadTimetable(Context context) {
        return null;
    }

    public static Section loadSection(Context context) {
        return null;
    }

    public static ArrayList<Term> loadMidterm(Context context) {
        return null;
    }

    public static ArrayList<Term> loadExams(Context context) {
        return null;
    }
}
