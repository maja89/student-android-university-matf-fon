package rs.ac.bg.student.utils;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rs.ac.bg.student.R;
import rs.ac.bg.student.model.AcademicYear;
import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Date;
import rs.ac.bg.student.model.ElectiveSubjects;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.ScheduleExam;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.model.Interval;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.WorkingWeekendDay;
import rs.ac.bg.student.utils.Resources;

/**
 * Created by mcekic on 3/21/18.
 */

public class AssetLoader {
    public static final String TAG = "AssetLoader";

    private static boolean DEBUG = false;

    private static final String CALENDAR = "data/kalendar/kalendar.txt";

    private static final String PATTERN_DATE = "d.M.yyyy.";
    private static final String PATTERN_DATE_TIME = "d-M-yyyy----hh:mm";
    private static final String PATTERN_DATE_TIME_2 = "d-M-yyyy----\thh:mm";

    private static final String SUBJECTS_ISIT = "data/predmeti/predmeti_informacioni_sistemi_i_tehnologije.txt";
    private static final String SUBJECTS_M = "data/predmeti/predmeti_menadzment.txt";
    private static final String SUBJECTS_MKIS = "data/predmeti/predmeti_menadzment_kvaliteta_i_standardizacija.txt";
    private static final String SUBJECTS_OM = "data/predmeti/predmeti_operacioni_menadzment.txt";

    private static final String SCHEDULE_1 = "data/raspored/raspored_prva_let_2018.txt";
    private static final String SCHEDULE_2 = "data/raspored/raspored_druga_let_2018.txt";
    private static final String SCHEDULE_3 = "data/raspored/raspored_treca_let_2018.txt";
    private static final String SCHEDULE_4 = "data/raspored/raspored_cetvrta_let_2018.txt";
    private static final String SCHEDULE_ENG = "data/raspored/raspored_eng_let_2018.txt";

    /**
     *
     */
    public static final int KEY_FIRST_YEAR = 1;
    public static final int KEY_SECOND_YEAR = 2;
    public static final int KEY_THIRD_YEAR = 3;
    public static final int KEY_FOURTH_YEAR = 4;

    /**
     *
     */
    private static final String KEY_SCHEDULE_ENG = "eng";
    private static final String KEY_SCHEDULE_1 = "1";
    private static final String KEY_SCHEDULE_2 = "2";
    private static final String KEY_SCHEDULE_3 = "3";
    private static final String KEY_SCHEDULE_4 = "4";

    private static final String EXAMS_JAN = "data/ispiti/januar.txt";
    private static final String EXAMS_FEB = "data/ispiti/februar.txt";
    private static final String EXAMS_JUN = "data/ispiti/jun.txt";
    private static final String EXAMS_JUL = "data/ispiti/jul.txt";
    private static final String EXAMS_SEP = "data/ispiti/septembar.txt";
    private static final String EXAMS_OKT = "data/ispiti/oktobar.txt";

    private static final String MIDTERM_FIRST = "data/kolokvijumi/prvi_letnji.txt";
    private static final String MIDTERM_SECOND = "data/kolokvijumi/drugi_letnji.txt";

    protected static CalendarYear loadCalendarYear(Context context) {
        CalendarYear calendarYear = new CalendarYear();
        String contentLoaded = loadContentFromAsset(context, CALENDAR);

        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE);

        for (String line : lines) {
            if (DEBUG)
                Log.d(TAG, "line: " + line);

            java.util.Date dateStart = null;
            java.util.Date dateEnd = null;

            try {
                if (containsWeekdays(context, line) || line.contains(context.getResources().getString(R.string.midterm))) {
                    if (line.contains("-")) {
                        // start date
                        String startInput = line.substring(0, line.indexOf('-')) + line.substring(line.indexOf("\t") - 5, line.indexOf("\t"));
                        if (DEBUG)
                            Log.d(TAG, "startInput: " + startInput);
                        dateStart = parser.parse(startInput);

                        // end date
                        String endInput = line.substring(line.indexOf('-') + 1, line.indexOf("\t"));
                        if (DEBUG)
                            Log.d(TAG, "endInput: " + endInput);
                        dateEnd = parser.parse(endInput);
                    } else {
                        // date
                        String input = line.substring(0, line.indexOf("\t"));
                        if (DEBUG)
                            Log.d(TAG, "input: " + input);
                        dateStart = parser.parse(input);
                        dateEnd = dateStart;
                    }
                } else if (line.contains(context.getResources().getString(R.string.examination_period))) {
                    // start date
                    int start = line.indexOf(": ");
                    String startInput = line.substring(start + 2, start + 13);
                    if (DEBUG)
                        Log.d(TAG, "startInput: " + startInput);
                    dateStart = parser.parse(startInput);

                    // end date
                    start = line.indexOf("до ");
                    String endInput = line.substring(start + 3, start + 14);
                    if (DEBUG)
                        Log.d(TAG, "endInput: " + endInput);
                    dateEnd = parser.parse(endInput);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (dateStart == null || dateEnd == null) continue;

            Date date1 = Date.getDate(dateStart);
            Date date2 = Date.getDate(dateEnd);

            Interval interval = new Interval(date1, date2);
            if (containsWeekdays(context, line)) {
                if (DEBUG)
                    Log.d(TAG, "workingWeeks, interval added: " + interval.toString());

                if (line.contains("*")) {
                    String description = CyrillicLatinConverter.latinToCyrillic(line.substring(line.indexOf('*')));
                    WorkingWeekendDay workingWeekendDay = new WorkingWeekendDay(interval.endDate, getWorkingDay(context, description));
                    if (DEBUG)
                        Log.d(TAG, "workingWeekendDay added: " + workingWeekendDay.toString());
                    calendarYear.workingWeekendDays.add(workingWeekendDay);
                }
                calendarYear.workingWeeks.add(interval);
            } else if (line.contains(context.getResources().getString(R.string.midterm))) {
                if (DEBUG)
                    Log.d(TAG, "midtermWeeks, interval added: " + interval.toString());

                calendarYear.midtermWeeks.add(interval);
            } else if (line.contains(context.getResources().getString(R.string.examination_period))) {
                if (DEBUG)
                    Log.d(TAG, "examWeeks, interval added: " + interval.toString());

                calendarYear.examWeeks.add(interval);
            }
        }
        return calendarYear;
    }

    /**
     * @param context Context
     * @param input   String
     * @return
     */
    private static boolean containsWeekdays(Context context, String input) {
        return input.contains(context.getResources().getString(R.string.day_monday))
                || input.contains(context.getResources().getString(R.string.day_tuesday))
                || input.contains(context.getResources().getString(R.string.day_wenesday))
                || input.contains(context.getResources().getString(R.string.day_thursday))
                || input.contains(context.getResources().getString(R.string.day_friday))
                || input.contains(context.getResources().getString(R.string.day_saturday))
                || input.contains(context.getResources().getString(R.string.day_sunday));
    }

    /**
     * description example:  *Субота	* Настава за Уторак
     *
     * @param context     Context
     * @param description String
     * @return
     */
    private static String getWorkingDay(Context context, String description) {
        // description contains Субота
        if (description.contains(Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_SATURDAY))
                || description.contains(Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_SATURDAY))) {

            String day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_MONDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_MONDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_TUESDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_TUESDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_WEDNESDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_WEDNESDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_THURSDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_THURSDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_FRIDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_FRIDAY;
            }
        }
        return null;
    }

    protected static Timetable loadTimetableAll(Context context) {

        ArrayList<ScheduleClass> schedules = new ArrayList<>();
        schedules.addAll(loadTimetableAll(context, KEY_SCHEDULE_1));
        schedules.addAll(loadTimetableAll(context, KEY_SCHEDULE_2));
        schedules.addAll(loadTimetableAll(context, KEY_SCHEDULE_3));
        schedules.addAll(loadTimetableAll(context, KEY_SCHEDULE_4));
        schedules.addAll(loadTimetableAll(context, KEY_SCHEDULE_ENG));

        //TODO:
        Interval interval = new Interval(new Date(2018, 2, 20), new Date(2018, 5, 30));
        Timetable timetable = new Timetable(interval, schedules);
        return timetable;
    }

    /**
     * Loads schedule for given sectionName.
     *
     * @param context Context
     * @return
     */
    private static ArrayList<ScheduleClass> loadTimetableAll(Context context, String year) {

        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "year: " + year);

        String filePath = null;
        if (KEY_SCHEDULE_1.equals(year)) {
            filePath = SCHEDULE_1;
        } else if (KEY_SCHEDULE_2.equals(year)) {
            filePath = SCHEDULE_2;
        } else if (KEY_SCHEDULE_3.equals(year)) {
            filePath = SCHEDULE_3;
        } else if (KEY_SCHEDULE_4.equals(year)) {
            filePath = SCHEDULE_4;
        } else if (KEY_SCHEDULE_ENG.equals(year)) {
            filePath = SCHEDULE_ENG;
        }
        if (filePath == null) return null;

        String contentLoaded = loadContentFromAsset(context, filePath);

        contentLoaded = CyrillicLatinConverter.latinToCyrillic(contentLoaded);

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");
        ArrayList<ScheduleClass> schedules = new ArrayList<>();

        String dayOfTheWeek = "";


        for (String line : lines) {
            if (line.contains(context.getResources().getString(R.string.day_monday))) {
                dayOfTheWeek = ScheduleClass.DAY_MONDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_tuesday))) {
                dayOfTheWeek = ScheduleClass.DAY_TUESDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_wenesday))) {
                dayOfTheWeek = ScheduleClass.DAY_WEDNESDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_thursday))) {
                dayOfTheWeek = ScheduleClass.DAY_THURSDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_friday))) {
                dayOfTheWeek = ScheduleClass.DAY_FRIDAY;
            } else {
                if (DEBUG)
                    Log.d(TAG, "dayOfTheWeek: " + dayOfTheWeek);

                ScheduleClass schedule = createSchedule(dayOfTheWeek, line, year);
                if (schedule != null) {
                    if (DEBUG)
                        Log.d(TAG, "schedule added: " + schedule);

                    schedules.add(schedule);
                }
            }
        }

        return schedules;
    }

    private static ScheduleClass createSchedule(String dayOfTheWeek, String line, String year) {

        DEBUG = true;

        if (DEBUG)
            Log.d(TAG, "createSchedule, line: {" + line + "}");

        // TODO: clean
        String groupPrefix = "Енг-";
        String hallPrefix = "Сала";
        String typeP = "\tП\t";
        String typeV = "\tВ\t";
        String type = "";

        if (line.contains(typeP)) {
            type = typeP;
        } else if (line.contains(typeV)) {
            type = typeV;
        }

        if (type.isEmpty()) return null;

        String subjectName = line.substring(0, line.indexOf(type));
        subjectName = subjectName.replaceAll("\\s+$", "");
        if (subjectName.contains("Ц#")) {
            // keep C#
            subjectName = subjectName.replace('Ц', 'C');
        }
        if (DEBUG)
            Log.d(TAG, "createSchedule, subjectName: {" + subjectName + "}");

        String group = line.substring(line.indexOf(type) + 3, line.indexOf(":") - 2);
        group = group.replaceAll("\\s+$", "");
        if (KEY_SCHEDULE_ENG.equals(year)) {
            group = groupPrefix + group;
        }

        String time = line.substring(line.indexOf(":") - 2, line.indexOf(":"));
        time = getTime(time);

        String hall = line.substring(line.indexOf(hallPrefix));
        hall = hall.replaceAll("\\s+$", "");

        int scheduleType = typeP.equals(type) ? ScheduleClass.TYPE_P : ScheduleClass.TYPE_V;

        return new ScheduleClass(dayOfTheWeek, subjectName, scheduleType, group, time, hall);
    }

    /**
     * @param time String
     * @return
     */
    private static String getTime(String time) {
        int t = Integer.parseInt(time);
        switch (t) {
            case 8:
                return ScheduleClass.TIME_8;
            case 10:
                return ScheduleClass.TIME_10;
            case 12:
                return ScheduleClass.TIME_12;
            case 14:
                return ScheduleClass.TIME_14;
            case 16:
                return ScheduleClass.TIME_16;
            case 18:
                return ScheduleClass.TIME_18;
            case 20:
                return ScheduleClass.TIME_20;
        }
        return "";
    }

    /**
     * Loads subjects for given sectionName.
     *
     * @param context     Context
     * @param sectionName String
     * @return Section object
     */
    protected static Section loadSection(Context context, String sectionName) {
        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "sectionName: " + sectionName);

        String filePath = null;
        if (Section.SECTION_1_ISIT.equals(sectionName)) {
            filePath = SUBJECTS_ISIT;
        } else if (Section.SECTION_2_M.equals(sectionName)) {
            filePath = SUBJECTS_M;
        } else if (Section.SECTION_3_MKIS.equals(sectionName)) {
            filePath = SUBJECTS_MKIS;
        } else if (Section.SECTION_4_OM.equals(sectionName)) {
            filePath = SUBJECTS_OM;
        }
        if (filePath == null) return null;

        String contentLoaded = loadContentFromAsset(context, filePath);

        if (DEBUG)
            Log.d(TAG, "loadSection: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        Section section = new Section(sectionName);
        int yearCounter = 1;
        boolean adding = false;
        AcademicYear academicYear = new AcademicYear(yearCounter);

        int semester = 0;
        ElectiveSubjects electiveSubjects = null;

        for (String line : lines) {
            if (line.contains(".") && line.contains("+")) {
                if (DEBUG)
                    Log.d(TAG, "Adding subject: " + line);

                adding = true;

                if (academicYear.subjects.size() == getSemesterWinterSubjectsSize(sectionName, yearCounter)) {
                    semester++;
                }
                academicYear.subjects.add(createSubjectObject(semester, line));

            } else if (yearCounter <= 4) {

                if (adding) {
                    adding = false;
                    section.academicYears.add(academicYear);
                    yearCounter++;
                    if (yearCounter <= 4)
                        academicYear = new AcademicYear(yearCounter);

                    if (DEBUG)
                        Log.d(TAG, "New academicYear: " + line);
                } else if (line.matches("[[0-9][\\s]+]+")) {
                    semester = Integer.parseInt(line.substring(0, 1));

                    if (DEBUG)
                        Log.d(TAG, "New semester: " + semester);
                } else {
                    if (DEBUG)
                        Log.d(TAG, "Skipping line: " + line);
                }
            } else if (line.contains("Листа изборних предмета")) {
                if (DEBUG)
                    Log.d(TAG, "New electiveSubjects: " + line);

                if (electiveSubjects != null) {
                    section.electiveSubjects.add(electiveSubjects);
                }
                // removes spaces at the end and beginning
                line = line.replaceAll("\\s+$", "");
                electiveSubjects = new ElectiveSubjects(line);

            } else if (!line.isEmpty() && electiveSubjects != null) {
                if (DEBUG)
                    Log.d(TAG, "Adding subject: " + line);

                electiveSubjects.subjects.add(new Subject(line));
            }
        }

        if (electiveSubjects != null) {
            section.electiveSubjects.add(electiveSubjects);
        }

        if (DEBUG)
            Log.d(TAG, "Section added: " + section.toString());

        return section;
    }

    /**
     * @param sectionName String
     * @param year        int
     * @return
     */
    private static int getSemesterWinterSubjectsSize(String sectionName, int year) {
        if (Section.SECTION_1_ISIT.equals(sectionName)) {
            switch (year) {
                case KEY_FIRST_YEAR:
                    return 6;
                case KEY_SECOND_YEAR:
                    return 6;
                case KEY_THIRD_YEAR:
                    return 5;
                case KEY_FOURTH_YEAR:
                    return 5;
            }
        } else if (Section.SECTION_2_M.equals(sectionName)) {
            switch (year) {
                case KEY_FIRST_YEAR:
                    return 7;
                case KEY_SECOND_YEAR:
                    return 6;
                case KEY_THIRD_YEAR:
                    return 5;
                case KEY_FOURTH_YEAR:
                    return 5;
            }
        } else if (Section.SECTION_3_MKIS.equals(sectionName)) {
            switch (year) {
                case KEY_FIRST_YEAR:
                    return 7;
                case KEY_SECOND_YEAR:
                    return 6;
                case KEY_THIRD_YEAR:
                    return 5;
                case KEY_FOURTH_YEAR:
                    return 6;
            }
        } else if (Section.SECTION_4_OM.equals(sectionName)) {
            switch (year) {
                case KEY_FIRST_YEAR:
                    return 7;
                case KEY_SECOND_YEAR:
                    return 6;
                case KEY_THIRD_YEAR:
                    return 5;
                case KEY_FOURTH_YEAR:
                    return 6;
            }
        }
        return 5;
    }

    /**
     * Creates Subject object form input String line.
     *
     * @param semester int
     * @param line     String
     * @return Subject object
     */
    private static Subject createSubjectObject(int semester, String line) {
        // get number
        String number = line.substring(0, line.indexOf('.'));

        // get subjectName
        int start = 0;
        {
            // finds all letters
            Pattern pattern = Pattern.compile("\\p{L}");
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                // finds first letter
                start = matcher.start();
            }
        }

        String subjectName = line.substring(start, line.indexOf('+') - 1);
        // removes spaces at the end and beginning
        subjectName = subjectName.replaceAll("\\s+$", "");
        if (DEBUG)
            Log.d(TAG, "createSchedule, subjectName: {" + subjectName + "}");

        int end = 0;
        {
            // finds all letters followed by space
            Pattern pattern = Pattern.compile("[\\p{L}[\\s]+]+");
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                // finds last space
                end = matcher.start();
            }
        }

        String semesterDescription = line.substring(line.indexOf('+') - 1, end);
        // removes spaces at the end and beginning
        semesterDescription = semesterDescription.replaceAll("\\s+$", "");

        String semesterDescription1 = (semester % 2 == 1) ? semesterDescription : "";
        String semesterDescription2 = (semester % 2 == 0) ? semesterDescription : "";

        if (semesterDescription.contains("\t")) {
            semesterDescription1 = semesterDescription.substring(0, semesterDescription.indexOf('\t'));
            semesterDescription2 = semesterDescription.substring(semesterDescription.indexOf('\t') + 1);
        }

        // get esp
        String esp = "";
        {
            // gets last digit
            Pattern pattern = Pattern.compile("[0-9]+$");
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                esp = matcher.group();
            }
        }

        return new Subject(Integer.parseInt(number), subjectName, semester, semesterDescription1, semesterDescription2, Integer.parseInt(esp));
    }

    /**
     * Loads file from assets.
     *
     * @param context  Context
     * @param fileName String
     * @return text as String
     */
    private static String loadContentFromAsset(Context context, String fileName) {
        String text = null;
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            text = new String(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return text;
    }

    protected static ArrayList<Term> loadMidtermSchedules(Context context) {
        ArrayList<Term> listOfSchedulesObject = new ArrayList<>();

        Term term = loadMidtermSchedules(context, MIDTERM_FIRST, "Prva kolokvijumska nedelja");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadMidtermSchedules(context, MIDTERM_SECOND, "Druga kolokvijumska nedelja");
        if (term != null) listOfSchedulesObject.add(term);

        return listOfSchedulesObject;
    }

    private static Term loadMidtermSchedules(Context context, String filePath, String description) {

        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "filePath: " + filePath);

        if (filePath == null) return null;

        description = CyrillicLatinConverter.latinToCyrillic(description);
        Term term = new Term(description);

        String contentLoaded = loadContentFromAsset(context, filePath);
        if (contentLoaded == null) return null;
        contentLoaded = CyrillicLatinConverter.latinToCyrillic(contentLoaded);

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        ArrayList<ScheduleExam> schedules = new ArrayList<>();
        SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE_TIME);

        for (String line : lines) {
            if (DEBUG)
                Log.d(TAG, "line: " + line);

            java.util.Date date = null;
            String subjectName = "";
            String hall = "";
            String note = "";

            if (line.contains("----")) {
                subjectName = line.substring(0, line.indexOf("\t"));
                // removes spaces at the end and beginning
                subjectName = subjectName.replaceAll("\\s+$", "");
                if (DEBUG)
                    Log.d(TAG, "subjectName: {" + subjectName + "}");

                // time
                String time = line.substring(line.indexOf(':') - 2, line.indexOf(':') + 3);
                if (DEBUG)
                    Log.d(TAG, "time: " + time);

                // date
                String startInput = line.substring(line.indexOf("\t"), line.indexOf("\t") + 20);
                if (DEBUG)
                    Log.d(TAG, "startInput: " + startInput);
                try {
                    date = parser.parse(startInput);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (date == null) continue;

                // get last hall
                int end = 0;
                {
                    // finds all 3 digits without space
                    Pattern pattern = Pattern.compile("[0-9][0-9][0-9]+");
                    Matcher matcher = pattern.matcher(line);
                    while (matcher.find()) {
                        // finds last hall
                        end = matcher.start();
                    }
                }
                if (DEBUG)
                    Log.d(TAG, "end: " + end);

                end += 3;
                if (end > line.indexOf("\t") + 21 && end < line.length()) {
                    hall = line.substring(line.indexOf("\t") + 21, end);

                    note = line.substring(end);
                    // removes spaces at the end and beginning
                    note = note.replaceAll("\\s+$", "");
                    if (note.contains("И")) {
                        // keep Roman numbers (I,II,III)
                        note = note.replace('И', 'I');
                    }
                    if (DEBUG)
                        Log.d(TAG, "note: " + note);
                } else {
                    hall = line.substring(line.indexOf("\t") + 21);
                }
                // removes spaces at the end and beginning
                hall = hall.replaceAll("\\s+$", "");
                if (DEBUG)
                    Log.d(TAG, "hall: {" + hall + "}");

                Date day = Date.getDate(date);
                if (DEBUG)
                    Log.d(TAG, "day: " + day);

                schedules.add(new ScheduleExam(day, subjectName, ScheduleExam.TYPE_M, note, time, hall));
            }
        }

        term.schedules = schedules;
        return term;
    }

    protected static ArrayList<Term> loadExamsSchedules(Context context) {
        ArrayList<Term> listOfSchedulesObject = new ArrayList<>();

        Term term = loadExamsSchedules(context, EXAMS_JAN, "Januarski ispitni rok");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_FEB, "Februarski ispitni rok");
        if (term != null) listOfSchedulesObject.add(term);

        /*term = loadExamsSchedulesDays(context, EXAMS_JUN, "Junski ispitni rok", new Date(2018, 6, 11));*/
        term = loadExamsSchedules(context, EXAMS_JUN, "Junski ispitni rok");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_JUL, "Julski ispitni rok");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_SEP, "Septembarski ispitni rok");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_OKT, "Oktobarski ispitni rok");
        if (term != null) listOfSchedulesObject.add(term);

        return listOfSchedulesObject;
    }

    private static Term loadExamsSchedules(Context context, String filePath, String description) {
        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "filePath: " + filePath);

        if (filePath == null) return null;

        description = CyrillicLatinConverter.latinToCyrillic(description);
        Term term = new Term(description);

        String contentLoaded = loadContentFromAsset(context, filePath);
        if (contentLoaded == null) return null;
        contentLoaded = CyrillicLatinConverter.latinToCyrillic(contentLoaded);

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        ArrayList<ScheduleExam> schedules = new ArrayList<>();


        for (String line : lines) {
            if (DEBUG)
                Log.d(TAG, "line: " + line);

            java.util.Date date = null;
            String subjectName = "";
            String hall = "";
            String note = "";

            if (line.contains("----")) {
                subjectName = line.substring(0, line.indexOf("\t"));
                // removes spaces at the end and beginning
                subjectName = subjectName.replaceAll("\\s+$", "");
                if (DEBUG)
                    Log.d(TAG, "subjectName: {" + subjectName + "}");

                // time
                String time = line.substring(line.indexOf(':') - 2, line.indexOf(':') + 3);
                if (DEBUG)
                    Log.d(TAG, "time: " + time);

                // date
                String startInput = line.substring(line.indexOf("-") - 2, line.indexOf("-") + 18);
                if (DEBUG)
                    Log.d(TAG, "startInput: " + startInput);
                try {
                    SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE_TIME_2);
                    date = parser.parse(startInput);
                } catch (ParseException e) {
                    e.printStackTrace();
                    try {
                        SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE_TIME);
                        date = parser.parse(startInput);
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                }
                if (date == null) continue;

                // get last hall
                int end = 0;
                {
                    // finds all 3 digits without space
                    Pattern pattern = Pattern.compile("[0-9][0-9][0-9]+");
                    Matcher matcher = pattern.matcher(line);
                    while (matcher.find()) {
                        // finds last hall
                        end = matcher.start();
                    }
                }
                if (DEBUG)
                    Log.d(TAG, "end: " + end);

                end += 3;
                if (end > line.indexOf("-") + 19 && end < line.length()) {
                    hall = line.substring(line.indexOf("-") + 19, end);

                    note = line.substring(end);
                    // removes spaces at the end and beginning
                    note = note.replaceAll("\\s+$", "");
                    if (note.contains("И")) {
                        // keep Roman numbers (I,II,III)
                        note = note.replace('И', 'I');
                    }
                    if (DEBUG)
                        Log.d(TAG, "note: " + note);
                } else {
                    hall = line.substring(line.indexOf("-") + 19);
                }
                // removes spaces at the end and beginning
                hall = hall.replaceAll("\\s+$", "");
                if (DEBUG)
                    Log.d(TAG, "hall: {" + hall + "}");


                Date day = Date.getDate(date);
                if (DEBUG)
                    Log.d(TAG, "day: " + day);

                String typeP = "\tП\t";
                String typeU = "\tУ\t";
                int type = 0;

                if (line.contains(typeP)) {
                    type = ScheduleExam.TYPE_P;
                } else if (line.contains(typeU)) {
                    type = ScheduleExam.TYPE_U;
                }

                schedules.add(new ScheduleExam(day, subjectName, type, note, time, hall));
            }
        }

        term.schedules = schedules;
        return term;
    }

    private static Term loadExamsSchedulesDays(Context context, String filePath, String description, Date startDate) {
        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "filePath: " + filePath);

        if (filePath == null) return null;

        description = CyrillicLatinConverter.latinToCyrillic(description);
        Term term = new Term(description);

        String contentLoaded = loadContentFromAsset(context, filePath);
        if (contentLoaded == null) return null;
        contentLoaded = CyrillicLatinConverter.latinToCyrillic(contentLoaded);

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        // adding 30 next dates
        HashMap<Integer, Date> map = new HashMap<>();
        for (int i = 1; i <= 30; i++) {
            map.put(i, startDate);
            startDate = startDate.nextDate();
        }

        ArrayList<ScheduleExam> schedules = new ArrayList<>();

        for (String line : lines) {
            if (DEBUG)
                Log.d(TAG, "line: " + line);

            String subjectName = "";
            String hall = "";
            String note = "";
            String dayString = "ДАН";

            if (line.contains(dayString)) {
                subjectName = line.substring(0, line.indexOf("\t"));
                // removes spaces at the end and beginning
                subjectName = subjectName.replaceAll("\\s+$", "");
                if (DEBUG)
                    Log.d(TAG, "subjectName: {" + subjectName + "}");

                // time
                String time = line.substring(line.indexOf(':') - 2, line.indexOf(':') + 3);
                if (DEBUG)
                    Log.d(TAG, "time: " + time);

                Date day = null;

                // finds last space
                int beginIndex = line.indexOf("ДАН") + 4;
                int endIndex = line.indexOf("ДАН") + 6;
                String substring = line.substring(beginIndex, endIndex);
                // removes spaces at the end and beginning
                substring = substring.replaceAll("\\s+$", "");
                int dayCounter = Integer.parseInt(substring);
                day = map.get(dayCounter);
                if (DEBUG)
                    Log.d(TAG, "day: " + day);

                if (day == null) continue;

                // get last hall
                int end = 0;
                {
                    // finds all 3 digits without space
                    Pattern pattern = Pattern.compile("[0-9][0-9][0-9]+");
                    Matcher matcher = pattern.matcher(line);
                    while (matcher.find()) {
                        // finds last hall
                        end = matcher.start();
                    }
                }
                if (DEBUG)
                    Log.d(TAG, "end: " + end);

                end += 3;
                if (end > line.indexOf(":") + 4 && end < line.length()) {
                    hall = line.substring(line.indexOf(":") + 4, end);

                    note = line.substring(end);
                    // removes spaces at the end and beginning
                    note = note.replaceAll("\\s+$", "");
                    if (note.contains("И")) {
                        // keep Roman numbers (I,II,III)
                        note = note.replace('И', 'I');
                    }
                    if (DEBUG)
                        Log.d(TAG, "note: " + note);
                } else {
                    hall = line.substring(line.indexOf(":") + 4);
                }
                // removes spaces at the end and beginning
                hall = hall.replaceAll("\\s+$", "");
                if (DEBUG)
                    Log.d(TAG, "hall: {" + hall + "}");

                String typeP = "\tП\t";
                String typeU = "\tУ\t";
                int type = 0;

                if (line.contains(typeP)) {
                    type = ScheduleExam.TYPE_P;
                } else if (line.contains(typeU)) {
                    type = ScheduleExam.TYPE_U;
                }

                schedules.add(new ScheduleExam(day, subjectName, type, note, time, hall));
            }
        }

        term.schedules = schedules;
        return term;
    }
}
