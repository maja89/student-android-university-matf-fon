package rs.ac.bg.student.utils;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.utils.Logger;

/**
 * Created by maja on 5/15/18.
 */

public class Utils {
    public static final String TAG = "Utils";

    private static final String URL_UPDATE = "http://10.10.0.129:3000/api/update/";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void loadAssets(final Context context) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                /*{
                    ArrayList<Term> terms = AssetLoader.loadExamsSchedules(context);
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    String json = gson.toJson(terms);
                    Logger.debugRequest(TAG, "term: " + json);
                    updateSection("term.json", json);
                }*/
                loadAll(context);
                return null;
            }
        }.execute();
    }

    private static void loadAll(Context context) {
        {
            CalendarYear calendarYear = AssetLoader.loadCalendarYear(context);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(calendarYear);
            Logger.debugRequest(TAG, "calendarYear: " + json);
            updateSection("calendar.json", json);
        }
        {
            Timetable timetable = AssetLoader.loadTimetableAll(context);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(timetable);
            Logger.debugRequest(TAG, "timetable: " + json);
            updateSection("timetable.json", json);
        }
        {
            ArrayList<Term> terms = AssetLoader.loadMidtermSchedules(context);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(terms);
            Logger.debugRequest(TAG, "midterm: " + json);
            updateSection("midterm.json", json);
        }
        {
            ArrayList<Term> terms = AssetLoader.loadExamsSchedules(context);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(terms);
            Logger.debugRequest(TAG, "term: " + json);
            updateSection("term.json", json);
        }
        {
            Section section = AssetLoader.loadSection(context, Section.SECTION_1_ISIT);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(section);
            Logger.debugRequest(TAG, "section_isit: " + json);
            updateSection("section_isit.json", json);
        }
        {
            Section section = AssetLoader.loadSection(context, Section.SECTION_2_M);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(section);
            Logger.debugRequest(TAG, "section_m: " + json);
            updateSection("section_m.json", json);
        }
        {
            Section section = AssetLoader.loadSection(context, Section.SECTION_3_MKIS);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(section);
            Logger.debugRequest(TAG, "section_mkis: " + json);
            updateSection("section_mkis.json", json);
        }
        {
            Section section = AssetLoader.loadSection(context, Section.SECTION_4_OM);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(section);
            Logger.debugRequest(TAG, "section_om: " + json);
            updateSection("section_om.json", json);
        }
    }

    private static void updateSection(String section, String json) {

        String requestUrl = URL_UPDATE + section;
        Logger.debugRequest(TAG, requestUrl);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(requestUrl)
                .post(body)
                .build();
        try {
            client.newCall(request).execute();
        } catch (IOException e) {
            Logger.debugRequest(TAG, "IOException" + e);
            e.printStackTrace();
        }
    }

    public static CalendarYear loadCalendarYear(Context context) {
        return null;
    }

    public static Timetable loadTimetable(Context context) {
        return null;
    }

    public static Section loadSection(Context context) {
        return null;
    }

    public static ArrayList<Term> loadMidterm(Context context) {
        return null;
    }

    public static ArrayList<Term> loadExams(Context context) {
        return null;
    }
}
