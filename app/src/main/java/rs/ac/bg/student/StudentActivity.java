package rs.ac.bg.student;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import rs.ac.bg.student.activities.BaseActivity;
import rs.ac.bg.student.activities.WelcomeActivity;
import rs.ac.bg.student.fragments.SearchFragment;
import rs.ac.bg.student.fragments.navigation.CalendarFragment;
import rs.ac.bg.student.fragments.navigation.NewsFragment;
import rs.ac.bg.student.fragments.navigation.ScheduleClassFragment;
import rs.ac.bg.student.fragments.navigation.SubjectsFragment;
import rs.ac.bg.student.fragments.navigation.NavigationFragment;
import rs.ac.bg.student.fragments.navigation.ScheduleExamsFragment;
import rs.ac.bg.student.fragments.navigation.ScheduleMidtermFragment;
import rs.ac.bg.student.fragments.navigation.SettingsFragment;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.AppUtils;
import rs.ac.bg.student.utils.Resources;

public class StudentActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchFragment.OnSearchListener {
    public static final String TAG = "StudentActivity";

    public static final String ACTION_SUBJECTS = "ACTION_SUBJECTS";

    private FloatingActionButton mAddButton;

    private SearchFragment mFragmentSearch;

    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;

    private AlertDialog mAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAddButton = (FloatingActionButton) findViewById(R.id.addButton);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddClicked(view);
            }
        });
        mAddButton.setVisibility(View.GONE);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                // navigation items not responsive, bugfix
                mNavigationView.bringToFront();
                mDrawerLayout.requestLayout();
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        // sets section name
        View headerLayout = mNavigationView.getHeaderView(0);
        ((TextView) headerLayout.findViewById(R.id.sectionName)).setText(Resources.getSectionNameResource(getBaseContext()));

        mFragmentSearch = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_search);
        mFragmentSearch.setOnSearchListener(this);
        hideFragment(mFragmentSearch);

        Intent intent = getIntent();
        if(ACTION_SUBJECTS.equals(intent.getAction())){
            setSubjectsItemSelected();
        } else {
            setCalendarItemSelected();
        }
    }

    private void setCalendarItemSelected() {
        // sets first fragment selected
        MenuItem firstItem = mNavigationView.getMenu().getItem(0);
        firstItem.setChecked(true);
        onNavigationItemSelected(firstItem);
    }

    private void setSubjectsItemSelected(){
        // sets subjects fragment selected
        MenuItem subjectsItem = mNavigationView.getMenu().getItem(5);
        subjectsItem.setChecked(true);
        onNavigationItemSelected(subjectsItem);
    }

    private void showAddButton() {
        mAddButton.setVisibility(View.VISIBLE);
    }

    private void hideAddButton() {
        mAddButton.setVisibility(View.GONE);
    }

    /**
     * Shows Search fragment on screen.
     */
    public void showSearchFragment(String tag, Subject subject) {
        showFragment(mFragmentSearch);
        mFragmentSearch.onShowFragment(tag, subject);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        hideAddButton();

        Fragment newFragment = null;
        String tag = "";

        if (id == R.id.nav_1_calendar) {
            newFragment = new CalendarFragment();
            tag = CalendarFragment.TAG;
        } else if (id == R.id.nav_2_news) {
            showAddButton();
            newFragment = new NewsFragment();
            tag = NewsFragment.TAG;
        } else if (id == R.id.nav_3_schedule_teaching) {
            int selectedSubjectsSize = AppUtils.getSelectedSubjects(getBaseContext()).size();
            if (selectedSubjectsSize > 0) {
                showAddButton();
            }
            newFragment = new ScheduleClassFragment();
            tag = ScheduleClassFragment.TAG;
        } else if (id == R.id.nav_4_schedule_midterm) {
            newFragment = new ScheduleMidtermFragment();
            tag = ScheduleMidtermFragment.TAG;
        } else if (id == R.id.nav_5_schedule_exams) {
            newFragment = new ScheduleExamsFragment();
            tag = ScheduleExamsFragment.TAG;
        } else if (id == R.id.nav_6_subjects) {
            newFragment = new SubjectsFragment();
            tag = SubjectsFragment.TAG;
        } else if (id == R.id.nav_7_settings) {
            newFragment = new SettingsFragment();
            tag = SettingsFragment.TAG;
        }

        if (newFragment != null) {
            // update the main content by replacing fragments
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, newFragment, tag);
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * PUBLIC VIEW METHOD SearchFragment
     */
    public void onBackArrowPressed(View view) {
        if (mFragmentSearch.isVisible()) {
            mFragmentSearch.onHideFragment();
            hideFragment(mFragmentSearch);
        }
    }

    /**
     * PUBLIC VIEW METHOD SettingsFragment
     */
    public void onDeleteAll(View view) {
        mAlertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_message)
                .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // deletes all
                        Preferences.clear(getBaseContext());

                        // cancels dialog
                        mAlertDialog.cancel();

                        // sends welcome activity intent
                        Intent intent = new Intent(getBaseContext(), WelcomeActivity.class);
                        intent.setAction(WelcomeActivity.ACTION_SHOW_SECTION);
                        startActivity(intent);

                        // finish this activity
                        finish();
                    }
                })
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // cancels dialog
                        mAlertDialog.cancel();
                    }
                })
                .create();
        mAlertDialog.show();
    }

    @Override
    public void onItemSelected(String tag, String subjectSelected) {
        if (mFragmentSearch.isVisible()) {
            mFragmentSearch.onHideFragment();
            hideFragment(mFragmentSearch);
        }

        if (!subjectSelected.isEmpty()) {
            NavigationFragment navigationFragment = NavigationFragment.getFragment(this, SubjectsFragment.TAG);
            if (navigationFragment != null) {
                if (navigationFragment instanceof SubjectsFragment) {
                    ((SubjectsFragment) navigationFragment).onItemSelected(subjectSelected);
                }
            }
        }
    }

    private void onAddClicked(View view) {
        NavigationFragment navigationFragment = NavigationFragment.getFragment(this, ScheduleClassFragment.TAG);
        if (navigationFragment != null && navigationFragment.isVisible()) {
            if (navigationFragment instanceof ScheduleClassFragment) {
                ((ScheduleClassFragment) navigationFragment).onAddClicked();
            }
        }

        navigationFragment = NavigationFragment.getFragment(this, NewsFragment.TAG);
        if (navigationFragment != null && navigationFragment.isVisible()) {
            if (navigationFragment instanceof NewsFragment) {
                ((NewsFragment) navigationFragment).onAddClicked();
            }
        }
    }
}
