package rs.ac.bg.student;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import rs.ac.bg.student.utils.Utils;


/**
 * Created by mcekic on 3/21/18.
 */

public class StudentApplication extends Application {
    public static final String TAG = "StudentApplication";

    public static final String FLAVOR_FON = "fon";
    public static final String FLAVOR_FON_TEST = "fonTest";

    public static final String FLAVOR_MATF = "matf";
    public static final String FLAVOR_MATF_TEST = "matfTest";

    @Override
    public void onCreate() {
        super.onCreate();
        // logs app version name and version code
        logPackageName();

        if (BuildConfig.FLAVOR.equals(FLAVOR_FON_TEST)) {
            Utils.loadAssets(getBaseContext());
        }
    }

    private void logPackageName() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            Log.i(TAG, "Current version name: " + info.versionName + " version code: " + info.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not find requested package");
        }
    }

    public static boolean isDebug() {
        return BuildConfig.DEBUG;
    }
}
