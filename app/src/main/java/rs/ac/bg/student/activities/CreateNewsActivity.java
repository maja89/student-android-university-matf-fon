package rs.ac.bg.student.activities;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import rs.ac.bg.student.R;
import rs.ac.bg.student.database.adapters.DatabaseAPIs;
import rs.ac.bg.student.model.NewsItem;
import rs.ac.bg.student.notifications.type.NewsNotification;
import rs.ac.bg.student.utils.AppUtils;

public class CreateNewsActivity extends AppCompatActivity implements View.OnFocusChangeListener {
    public static final String TAG = "CreateNewsActivity";

    private EditText mAuthorEditText, mTitleEditText, mMessageEditText;
    private Spinner mSubjectSpinner;

    private String mSubjectName, mAuthor, mTitle, mMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("");

        ActionBar bar = getSupportActionBar();
        bar.setHomeButtonEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayShowTitleEnabled(true);

        setSpinner();
        setUI();

        mSubjectName = mAuthor = mTitle = mMessage = "";
        mAuthorEditText.setText(mAuthor);
        mTitleEditText.setText(mTitle);
        mMessageEditText.setText(mMessage);
        checkValues();
    }

    private void setSpinner() {
        mSubjectSpinner = (Spinner) findViewById(R.id.spinner);

        List<String> selectedSubjects = AppUtils.getSelectedSubjects(getBaseContext());
        selectedSubjects.add(0, getResources().getString(R.string.to_all));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, selectedSubjects);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSubjectSpinner.setAdapter(adapter);
        mSubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = ((TextView) view).getText().toString();
                if (selected.equals(getResources().getString(R.string.to_all))) {
                    mSubjectName = "";
                } else {
                    mSubjectName = selected;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSubjectSpinner.setSelection(0);
    }

    private void setUI() {
        mAuthorEditText = (EditText) findViewById(R.id.news_author);
        mTitleEditText = (EditText) findViewById(R.id.news_title);
        mMessageEditText = (EditText) findViewById(R.id.news_message);
        setListeners();
    }

    private void setListeners() {
        mAuthorEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAuthor = editable.toString().trim();
                checkValues();
            }
        });
        mAuthorEditText.setOnFocusChangeListener(this);

        mTitleEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mTitle = editable.toString().trim();
                checkValues();
            }
        });
        mTitleEditText.setOnFocusChangeListener(this);

        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mMessage = editable.toString();
                checkValues();
            }
        });
        mMessageEditText.setOnFocusChangeListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        checkValues();
        mAuthorEditText.setText(mAuthor);
        mTitleEditText.setText(mTitle);
        mMessageEditText.setText(mMessage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_create_news, menu);
        menu.findItem(R.id.action_send).setEnabled(isSendEnabled());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_send:
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                onSendClicked();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onSendClicked() {
        long time = System.currentTimeMillis();
        NewsItem newsItem = new NewsItem(mSubjectName, mAuthor, mTitle, mMessage, time);

        //TODO: remove
        DatabaseAPIs.insertNewsItemToDB(getBaseContext(), newsItem);
        NewsNotification.createNewsNotification(getBaseContext(), newsItem);

        finish();
    }

    private void checkValues() {
        invalidateOptionsMenu();
    }

    private boolean isSendEnabled() {
        if (mAuthor == null || mAuthor.isEmpty()) {
            return false;
        }

        if (mTitle == null || mTitle.isEmpty()) {
            return false;
        }

        if (mMessage == null || mMessage.isEmpty()) {
            return false;
        }
        return true;
    }
}
