package rs.ac.bg.student.activities;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.MenuItem;
import android.widget.TextView;

import rs.ac.bg.student.R;
import rs.ac.bg.student.database.adapters.DatabaseAPIs;
import rs.ac.bg.student.model.NewsItem;
import rs.ac.bg.student.utils.TimeUtils;

public class NewsActivity extends AppCompatActivity {
    public static final String TAG = "NewsActivity";

    public static final String EXTRA_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("");

        ActionBar bar = getSupportActionBar();
        bar.setHomeButtonEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayShowTitleEnabled(true);

        Bundle extras = savedInstanceState == null ? getIntent().getExtras() : savedInstanceState;
        if (extras != null) {
            setUI(extras);
        } else {
            finish();
        }
    }

    private void setUI(Bundle extras) {
        long id = extras.getLong(EXTRA_ID);
        NewsItem item = DatabaseAPIs.getNewsItemFromDB(getBaseContext(), id);
        if (item == null) finish();

        TextView mSubject = (TextView) findViewById(R.id.news_subject);
        TextView mAuthor = (TextView) findViewById(R.id.news_author);
        TextView mTime = (TextView) findViewById(R.id.news_time);
        TextView mTitle = (TextView) findViewById(R.id.news_title);
        TextView mMessage = (TextView) findViewById(R.id.news_message);

        if (item.subjectName.isEmpty()) {
            mSubject.setText(getResources().getString(R.string.to_all));
        } else {
            mSubject.setText(item.subjectName);
        }
        mAuthor.setText(item.author);
        mTime.setText(TimeUtils.getTimeDetailString(item.time));
        mTitle.setText(item.title);
        mMessage.setText(item.message);
        Linkify.addLinks(mMessage, Linkify.WEB_URLS | Linkify.PHONE_NUMBERS | Linkify.EMAIL_ADDRESSES);
        mMessage.setMovementMethod(LinkMovementMethod.getInstance());

        // set read
        item.read = true;
        DatabaseAPIs.updateNewsItemFromDB(getBaseContext(), item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
