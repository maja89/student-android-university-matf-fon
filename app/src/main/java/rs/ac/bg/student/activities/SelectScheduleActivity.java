package rs.ac.bg.student.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ac.bg.student.R;
import rs.ac.bg.student.adapters.ScheduleAdapter;
import rs.ac.bg.student.adapters.data.ScheduleItem;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.network.Requests;
import rs.ac.bg.student.network.ResponseObject;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.AppUtils;

/**
 * Created by mcekic on 3/26/18.
 */

public class SelectScheduleActivity extends BaseActivity {
    public static final String TAG = "SelectScheduleActivity";

    /**
     * Reference to recycler view
     */
    private RecyclerView mRecyclerView;

    /**
     * Data Adapter
     */
    private ScheduleAdapter mAdapter;

    private RelativeLayout mLoadingLayout;
    private ProgressBar mProgressBar;
    private TextView mMessage;

    private ArrayList<ScheduleClass> mAllSchedules = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_schedule);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setHomeButtonEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayShowTitleEnabled(true);

        mLoadingLayout = (RelativeLayout) findViewById(R.id.layoutLoading);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mMessage = (TextView) findViewById(R.id.textView);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new ScheduleAdapter(getBaseContext(), ScheduleAdapter.TYPE_SCHEDULE_BY_SUBJECT);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                ScheduleItem item = mAdapter.getDataSet(itemPosition);
                if (item instanceof ScheduleClass) {
                    ScheduleClass schedule = (ScheduleClass) item;
                    schedule.selected = !schedule.selected;
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        TextView textView = (TextView) findViewById(R.id.screenMessage);
        textView.setVisibility(View.GONE);

        Timetable timetable = Preferences.loadTimetableAll(getBaseContext());
        if (timetable != null) {
            mAllSchedules = timetable.schedules;
            updateAdapter();
        } else {
            mMessage.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mLoadingLayout.setVisibility(View.VISIBLE);
            //TODO: remove getContext
            Requests.getTimetable(getBaseContext(), new Requests.RequestsCallback() {
                @Override
                public void onFailure() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                            mMessage.setVisibility(View.VISIBLE);
                        }
                    });
                }

                @Override
                public void onResponse(final ResponseObject response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoadingLayout.setVisibility(View.GONE);
                            if (response == null || response.result == null) return;

                            if (response.result instanceof Timetable) {
                                Timetable timetable1 = (Timetable) response.result;
                                mAllSchedules = timetable1.schedules;
                                updateAdapter();
                                Preferences.saveTimetableAll(getBaseContext(), timetable1);
                            }
                        }
                    });
                }
            });
        }
    }

    private void updateAdapter() {
        if (mAllSchedules == null) return;

        ArrayList<String> selectedSubjects = AppUtils.getSelectedSubjects(getBaseContext());

        ArrayList<ScheduleClass> classSchedule = new ArrayList<>();
        if (selectedSubjects.size() > 0) {
            for (ScheduleClass schedule : mAllSchedules) {
                for (String subjectName : selectedSubjects) {
                    if (AppUtils.areEqualSubjects(schedule.subjectName, subjectName)) {
                        classSchedule.add(schedule);
                    }
                }
            }
        }

        // set UI
        TextView textView = (TextView) findViewById(R.id.screenMessage);
        if (classSchedule.size() == 0) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

        mAdapter.updateClassSchedule(classSchedule);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        // saves modified data
        Timetable timetable = Preferences.loadTimetableAll(getBaseContext());
        if (timetable != null) {
            timetable.schedules = mAllSchedules;
            Preferences.saveTimetableAll(getBaseContext(), timetable);
        }
        Intent data = new Intent();
        setResult(RESULT_OK, data);
        super.finish();
    }
}
