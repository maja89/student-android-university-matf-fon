package rs.ac.bg.student.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by mcekic on 3/13/18.
 */

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected Context mContext;
    protected ArrayList<T> mDataSet;

    protected View.OnClickListener mOnItemClickListener;
    protected View.OnLongClickListener mOnItemLongClickListener;
    protected View.OnTouchListener mOnItemTouchListener;

    protected AdapterCallback<T> mAdapterCallback;

    public BaseAdapter(Context context) {
        mContext = context;
        mDataSet = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public T getItem(int position) {
        return mDataSet.get(position);
    }

    public void updateDataset(ArrayList<T> dataSet) {
        mDataSet = dataSet;
        notifyDataSetChanged();
    }

    public void add(T dataSet) {
        mDataSet.add(dataSet);
        notifyDataSetChanged();

        // fire callback when data is added
        if (mAdapterCallback != null) {
            mAdapterCallback.onDataSetAdded(dataSet);
        }
    }

    public void add(T dataSet, int position) {
        mDataSet.add(position, dataSet);
        // fire callback when data is added
        if (mAdapterCallback != null) {
            mAdapterCallback.onDataSetAdded(dataSet);
        }
    }

    public void clear() {
        mDataSet = new ArrayList<>();
        notifyDataSetChanged();
    }

    public T getDataSet(int position) {
        return mDataSet.get(position);
    }

    public void remove(final int position) {
        final T item = mDataSet.get(position);

        if (mDataSet.contains(item)) {
            // remove data and notify
            mDataSet.remove(position);
            notifyItemRemoved(position);

            // fire callback when data is removed
            if (mAdapterCallback != null) {
                mAdapterCallback.onDataSetRemoved(item);
            }
        }
    }

    public void setOnItemClickListener(View.OnClickListener clickListener) {
        mOnItemClickListener = clickListener;
    }

    public void setOnItemLongClickListener(View.OnLongClickListener clickListener) {
        mOnItemLongClickListener = clickListener;
    }

    public void setOnItemTouchListener(View.OnTouchListener touchListener) {
        mOnItemTouchListener = touchListener;
    }

    public void setAdapterCallback(AdapterCallback<T> callback) {
        mAdapterCallback = callback;
    }

    public interface AdapterCallback<T> {
        void onDataSetRemoved(T item);

        void onDataSetAdded(T item);

    }
}
