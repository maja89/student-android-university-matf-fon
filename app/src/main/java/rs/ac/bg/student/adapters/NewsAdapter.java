package rs.ac.bg.student.adapters;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rs.ac.bg.student.R;
import rs.ac.bg.student.model.NewsItem;
import rs.ac.bg.student.utils.TimeUtils;

public class NewsAdapter extends BaseAdapter<NewsItem, NewsAdapter.NewsViewHolder> {

    public NewsAdapter(Context context) {
        super(context);
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflates view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item, parent, false);

        // sets listeners
        if (mOnItemClickListener != null) {
            view.setOnClickListener(mOnItemClickListener);
        }
        if (mOnItemLongClickListener != null) {
            view.setOnLongClickListener(mOnItemLongClickListener);
        }
        if (mOnItemTouchListener != null) {
            view.setOnTouchListener(mOnItemTouchListener);
        }

        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        NewsItem item = mDataSet.get(position);
        if (item.subjectName.isEmpty()) {
            holder.mSubject.setText(mContext.getResources().getString(R.string.to_all));
        } else {
            holder.mSubject.setText(item.subjectName);
        }
        holder.mAuthor.setText(item.author);
        holder.mTime.setText(TimeUtils.getTimeString(item.time));
        holder.mTitle.setText(item.title);
        holder.mMessage.setText(item.message);

        holder.mSubject.setTypeface(null, item.read ? Typeface.NORMAL : Typeface.BOLD);
        holder.mAuthor.setTypeface(null, item.read ? Typeface.NORMAL : Typeface.BOLD);
        holder.mTime.setTypeface(null, item.read ? Typeface.NORMAL : Typeface.BOLD);
        holder.mTitle.setTypeface(null, item.read ? Typeface.NORMAL : Typeface.BOLD);
        holder.mMessage.setTypeface(null, item.read ? Typeface.NORMAL : Typeface.BOLD);
        // holder.mAuthor.setTextColor(ContextCompat.getColor(mContext, item.read ? R.color.black : R.color.black));
        // holder.mTitle.setTextColor(ContextCompat.getColor(mContext, item.read ? R.color.black : R.color.black));
    }

    /**
     * ViewHolder holding references to header view where we will populate our data
     */
    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        public TextView mSubject, mAuthor, mTime, mTitle, mMessage;

        public NewsViewHolder(View itemView) {
            super(itemView);
            mSubject = (TextView) itemView.findViewById(R.id.news_subject);
            mAuthor = (TextView) itemView.findViewById(R.id.news_author);
            mTime = (TextView) itemView.findViewById(R.id.news_time);
            mTitle = (TextView) itemView.findViewById(R.id.news_title);
            mMessage = (TextView) itemView.findViewById(R.id.news_message);
        }
    }
}
