package rs.ac.bg.student.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.adapters.data.ScheduleItem;
import rs.ac.bg.student.adapters.data.HeaderScheduleItem;
import rs.ac.bg.student.model.Date;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.ScheduleExam;
import rs.ac.bg.student.utils.Constants;
import rs.ac.bg.student.utils.Resources;

/**
 * Created by mcekic on 3/13/18.
 */

public class ScheduleAdapter extends BaseAdapter<ScheduleItem, ScheduleAdapter.BaseViewHolder> {
    public static final String TAG = "ScheduleAdapter";

    /**
     * ViewHolder types
     */
    private static final int INVALID = -1;
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_CLASS = 1;
    private static final int VIEW_TYPE_EXAM = 2;


    /**
     * Calendar: Teaching schedule and Exams schedule by date and time.
     * <p>
     * Exams schedule by date.
     */
    public static final String TYPE_SCHEDULE_BY_DATE = "ScheduleByDate";

    /**
     * Teaching schedule by day
     */
    public static final String TYPE_SCHEDULE_BY_DAY_OF_THE_WEEK = "ScheduleByDayOfTheWeek";

    /**
     * Teaching schedule by subject name
     */
    public static final String TYPE_SCHEDULE_BY_SUBJECT = "ScheduleBySubject";

    /**
     *
     */
    private String mType = "";

    /**
     * HeaderScheduleItem
     * <p>
     * ScheduleClass
     * TYPE_SCHEDULE_BY_DATE
     * TYPE_SCHEDULE_BY_DAY_OF_THE_WEEK
     * TYPE_SCHEDULE_BY_SUBJECT
     * <p>
     * ScheduleExam
     * TYPE_SCHEDULE_BY_DATE
     *
     * @param context Context
     * @param type    String, sorting items type
     */
    public ScheduleAdapter(Context context, String type) {
        super(context);
        mType = type;
    }

    @Override
    public int getItemViewType(int position) {
        ScheduleItem item = mDataSet.get(position);
        if (item instanceof HeaderScheduleItem) {
            return VIEW_TYPE_HEADER;
        } else if (item instanceof ScheduleClass) {
            return VIEW_TYPE_CLASS;
        } else if (item instanceof ScheduleExam) {
            return VIEW_TYPE_EXAM;
        }
        return INVALID;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        // inflates view
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_header_item, parent, false);
                break;
            case VIEW_TYPE_CLASS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_teaching_item, parent, false);
                break;
            case VIEW_TYPE_EXAM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_exam_item, parent, false);
                break;
            default:
                // VIEW_TYPE_HEADER
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_header_item, parent, false);
        }

        // sets listeners
        if (viewType != VIEW_TYPE_HEADER) {
            if (mOnItemClickListener != null) {
                view.setOnClickListener(mOnItemClickListener);
            }
            if (mOnItemLongClickListener != null) {
                view.setOnLongClickListener(mOnItemLongClickListener);
            }
            if (mOnItemTouchListener != null) {
                view.setOnTouchListener(mOnItemTouchListener);
            }
        }

        // creates ViewHolder
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new HeaderViewHolder(view);
            case VIEW_TYPE_CLASS:
                return new ClassViewHolder(view);
            case VIEW_TYPE_EXAM:
                return new ExamViewHolder(view);
            default:
                return new BaseViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        int viewType = holder.getItemViewType();

        ScheduleItem item = mDataSet.get(position);

        if (item instanceof HeaderScheduleItem && holder instanceof HeaderViewHolder) {
            final HeaderScheduleItem headerItem = (HeaderScheduleItem) item;
            HeaderViewHolder headerView = (HeaderViewHolder) holder;
            headerView.mTextView.setText(headerItem.text);

        } else if (item instanceof ScheduleClass && holder instanceof ClassViewHolder) {
            final ScheduleClass schedule = (ScheduleClass) item;
            ClassViewHolder itemView = (ClassViewHolder) holder;
            // subjectName
            if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
                itemView.mSubjectName.setText(schedule.subjectName);
            } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
                itemView.mSubjectName.setText(schedule.subjectName + "\n" + schedule.professor);
            }
            // subjectName selected background color
            if (schedule.selected) {
                itemView.mSubjectName.setBackgroundColor(mContext.getResources().getColor(ScheduleClass.TYPE_P == schedule.type ? R.color.table_yellow : R.color.table_green));
            } else {
                itemView.mSubjectName.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }
            // subjectName text color
            itemView.mSubjectName.setTextColor(mContext.getResources().getColor(schedule.selected ? R.color.white : R.color.black));

            // group
            itemView.mGroup.setText(schedule.group);

            // time
            String dayOfTheWeek = Resources.getDayOfTheWeekResource(mContext, schedule.dayOfWeek);
            itemView.mTime.setText(dayOfTheWeek + "\n" + schedule.time);

            // hall
            itemView.mHalls.setText(schedule.hall);

        } else if (item instanceof ScheduleExam && holder instanceof ExamViewHolder) {
            final ScheduleExam examItem = (ScheduleExam) item;
            ExamViewHolder itemView = (ExamViewHolder) holder;

            // subjectName
            itemView.mSubjectName.setText(examItem.subjectName);

            // backgroundColor
            int backgroundColor = INVALID;
            switch (examItem.type) {
                case ScheduleExam.TYPE_P:
                    backgroundColor = R.color.table_green;
                    break;
                case ScheduleExam.TYPE_U:
                    backgroundColor = R.color.table_yellow;
                    break;
                case ScheduleExam.TYPE_M:
                    backgroundColor = R.color.midterm_color;
                    break;
            }
            if (backgroundColor != INVALID) {
                itemView.mSubjectName.setBackgroundColor(mContext.getResources().getColor(backgroundColor));
            }

            // note
            if (examItem.note.isEmpty()) {
                itemView.mNote.setVisibility(View.GONE);
            } else {
                itemView.mNote.setVisibility(View.VISIBLE);
                itemView.mNote.setText(examItem.note);
            }

            // time
            itemView.mTime.setText(examItem.time);

            // hall
            itemView.mHalls.setText(examItem.hall);

        }
    }

    public static class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * ViewHolder holding references to header view where we will populate our data
     */
    private static class HeaderViewHolder extends BaseViewHolder {
        public TextView mTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.textView);
        }
    }

    /**
     * ViewHolder holding references to teaching schedule item view where we will populate our data
     */
    private static class ClassViewHolder extends BaseViewHolder {
        public TextView mSubjectName, mGroup, mTime, mHalls;

        public ClassViewHolder(View itemView) {
            super(itemView);
            mSubjectName = (TextView) itemView.findViewById(R.id.subjectName);
            mGroup = (TextView) itemView.findViewById(R.id.scheduleGroup);
            mTime = (TextView) itemView.findViewById(R.id.scheduleTime);
            mHalls = (TextView) itemView.findViewById(R.id.scheduleHall);
        }
    }

    /**
     * ViewHolder holding references to exam/midterm schedule item view where we will populate our data
     */
    private static class ExamViewHolder extends BaseViewHolder {
        public TextView mSubjectName, mNote, mTime, mHalls;

        public ExamViewHolder(View itemView) {
            super(itemView);
            mSubjectName = (TextView) itemView.findViewById(R.id.subjectName);
            mNote = (TextView) itemView.findViewById(R.id.scheduleNote);
            mTime = (TextView) itemView.findViewById(R.id.scheduleTime);
            mHalls = (TextView) itemView.findViewById(R.id.scheduleHall);
        }
    }

    /**
     * Sorts data based on mType.
     * Adds header view based on mType.
     *
     * @param dataSet ArrayList<ScheduleClass>
     */
    public void updateClassSchedule(ArrayList<ScheduleClass> dataSet) {
        ArrayList<ScheduleItem> dataSet1 = new ArrayList<>();
        switch (mType) {
            case TYPE_SCHEDULE_BY_DATE:
                Collections.sort(dataSet, new Comparator<ScheduleClass>() {
                    @Override
                    public int compare(final ScheduleClass schedule1, final ScheduleClass schedule2) {
                        return (Constants.getDayOfTheWeekInt(schedule1.dayOfWeek) + schedule1.time)
                                .compareTo(Constants.getDayOfTheWeekInt(schedule2.dayOfWeek) + schedule2.time);
                    }

                });
                dataSet1.addAll(dataSet);
                break;
            case TYPE_SCHEDULE_BY_DAY_OF_THE_WEEK:
                Collections.sort(dataSet, new Comparator<ScheduleClass>() {

                    @Override
                    public int compare(final ScheduleClass schedule1, final ScheduleClass schedule2) {
                        return (Constants.getDayOfTheWeekInt(schedule1.dayOfWeek) + schedule1.time)
                                .compareTo(Constants.getDayOfTheWeekInt(schedule2.dayOfWeek) + schedule2.time);
                    }

                });
                if (dataSet.size() > 0) {
                    String dayOfWeek = dataSet.get(0).dayOfWeek;
                    dataSet1.add(new HeaderScheduleItem(Resources.getDayOfTheWeekResource(mContext, dayOfWeek)));
                    for (ScheduleClass schedule : dataSet) {
                        if (!schedule.dayOfWeek.equals(dayOfWeek)) {
                            dayOfWeek = schedule.dayOfWeek;
                            dataSet1.add(new HeaderScheduleItem(Resources.getDayOfTheWeekResource(mContext, dayOfWeek)));
                        }
                        dataSet1.add(schedule);
                    }
                } else {
                    dataSet1.addAll(dataSet);
                }
                break;
            case TYPE_SCHEDULE_BY_SUBJECT:
                Collections.sort(dataSet, new Comparator<ScheduleClass>() {

                    @Override
                    public int compare(final ScheduleClass schedule1, final ScheduleClass schedule2) {
                        return (schedule1.subjectName + schedule1.type + getGroup(schedule1.group))
                                .compareTo(schedule2.subjectName + schedule2.type + getGroup(schedule2.group));
                    }

                });
                dataSet1.addAll(dataSet);
                break;
            default:
                dataSet1.addAll(dataSet);

        }
        updateDataset(dataSet1);
    }

    /**
     * Adds 0 in group for correct sorting.
     * Examples: A1 returns A01;  A10 returns A10;
     *
     * @param group String
     * @return
     */
    private String getGroup(String group) {
        if (group.length() == 2) {
            return group.charAt(0) + "0" + group.charAt(1);
        }
        return group;
    }

    /**
     * Sorts data based on mType.
     * Adds header view based on mType.
     *
     * @param dataSet ArrayList<ScheduleExam>
     * @param header  boolean
     */
    public void updateExamSchedule(ArrayList<ScheduleExam> dataSet, boolean header) {
        ArrayList<ScheduleItem> dataSet1 = new ArrayList<>();
        switch (mType) {
            case TYPE_SCHEDULE_BY_DATE:
                Collections.sort(dataSet, new Comparator<ScheduleExam>() {

                    @Override
                    public int compare(final ScheduleExam schedule1, final ScheduleExam schedule2) {
                        return (schedule1.date.year + getString(schedule1.date.month) + getString(schedule1.date.day) + schedule1.time)
                                .compareTo(schedule2.date.year + getString(schedule2.date.month) + getString(schedule2.date.day) + schedule2.time);
                    }

                });
                if (header && dataSet.size() > 0) {
                    Date date = dataSet.get(0).date;
                    dataSet1.add(new HeaderScheduleItem(date.getText(mContext)));
                    for (ScheduleExam schedule : dataSet) {
                        if (!schedule.date.equals(date)) {
                            date = schedule.date;
                            dataSet1.add(new HeaderScheduleItem(date.getText(mContext)));
                        }
                        dataSet1.add(schedule);
                    }
                } else {
                    dataSet1.addAll(dataSet);
                }
                break;
            default:
                dataSet1.addAll(dataSet);
        }
        updateDataset(dataSet1);
    }

    /**
     * Adds 0 in month/day for correct sorting.
     * Examples: 1 returns 01;
     *
     * @param number String
     * @return
     */
    private String getString(int number) {
        return Integer.toString(number / 10) + Integer.toString(number % 10);
    }
}
