package rs.ac.bg.student.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.adapters.data.HeaderSubjectItem;
import rs.ac.bg.student.adapters.data.SubjectItem;
import rs.ac.bg.student.model.AcademicYear;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by mcekic on 3/13/18.
 */

public class SubjectAdapter extends BaseAdapter<SubjectItem, SubjectAdapter.BaseViewHolder> {
    public static final String TAG = "SubjectAdapter";

    /**
     * ViewHolder types
     */
    private static final int INVALID = -1;
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_SUBJECT_UNSELECTED = 1;
    private static final int VIEW_TYPE_SUBJECT_SELECTED = 2;
    private static final int VIEW_TYPE_SUBJECT_PASSED = 3;

    public SubjectAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        SubjectItem item = mDataSet.get(position);
        if (item instanceof HeaderSubjectItem) {
            return VIEW_TYPE_HEADER;
        } else if (item instanceof Subject) {
            Subject subject = (Subject) item;
            if (subject.passed) {
                return VIEW_TYPE_SUBJECT_PASSED;
            } else if (subject.selected) {
                return VIEW_TYPE_SUBJECT_SELECTED;
            } else {
                return VIEW_TYPE_SUBJECT_UNSELECTED;
            }
        }
        return INVALID;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        // inflates view
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_header_item, parent, false);
                break;
            case VIEW_TYPE_SUBJECT_UNSELECTED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_item_unselected, parent, false);
                break;
            case VIEW_TYPE_SUBJECT_SELECTED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_item_selected, parent, false);
                break;
            case VIEW_TYPE_SUBJECT_PASSED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_item_passed, parent, false);
                break;
            default:
                // VIEW_TYPE_HEADER
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_header_item, parent, false);
        }

        // sets listeners
        if (viewType != VIEW_TYPE_HEADER) {
            if (mOnItemClickListener != null) {
                view.setOnClickListener(mOnItemClickListener);
            }
            if (mOnItemLongClickListener != null) {
                view.setOnLongClickListener(mOnItemLongClickListener);
            }
            if (mOnItemTouchListener != null) {
                view.setOnTouchListener(mOnItemTouchListener);
            }
        }

        // creates ViewHolder
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new HeaderViewHolder(view);
            case VIEW_TYPE_SUBJECT_UNSELECTED:
                return new SubjectUnelectedViewHolder(view);
            case VIEW_TYPE_SUBJECT_SELECTED:
                return new SubjectSelectedViewHolder(view);
            case VIEW_TYPE_SUBJECT_PASSED:
                return new SubjectPassedViewHolder(view);
            default:
                return new BaseViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        SubjectItem item = mDataSet.get(position);

        if (item instanceof HeaderSubjectItem && holder instanceof HeaderViewHolder) {
            final HeaderSubjectItem headerItem = (HeaderSubjectItem) item;
            HeaderViewHolder headerView = (HeaderViewHolder) holder;
            if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
                int year = headerItem.year;
                int semester = headerItem.year;
                headerView.mSemester1.setText(Integer.toString(semester));
                headerView.mSemester2.setText(Integer.toString(semester + 1));

                switch (year) {
                    case 1:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_1));
                        break;
                    case 2:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_2));
                        break;
                    case 3:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_3));
                        break;
                    case 4:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_4));
                        break;

                }
            } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
                int year = headerItem.year;
                int semester = headerItem.semester;

                String sectionName = Preferences.getSectionName(mContext);

                if (semester == 0 && year == 1) {
                    headerView.mSemester1.setText(mContext.getString(R.string.semester12_matf));
                } else if (semester == 0 && year == 2) {
                    headerView.mSemester1.setText(mContext.getString(R.string.semester34_matf));
                } else {
                    headerView.mSemester1.setText(semester + ". " + mContext.getString(R.string.semester_matf));
                }

                switch (semester) {
                    case 0:
                        headerView.mTitle.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                    case 3:
                        if (Section.SECTION_1_MATEMATIKA.equals(sectionName)) {
                            headerView.mTitle.setVisibility(View.GONE);
                        } else {
                            headerView.mTitle.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 5:
                    case 7:
                        headerView.mTitle.setVisibility(View.VISIBLE);
                        break;
                    default:
                        headerView.mTitle.setVisibility(View.GONE);
                }

                switch (year) {
                    case 1:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_1));
                        break;
                    case 2:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_2));
                        break;
                    case 3:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_3));
                        break;
                    case 4:
                        headerView.mTitle.setText(mContext.getString(R.string.table_title_4));
                        break;
                }
            }

        } else if (item instanceof Subject && holder instanceof SubjectViewHolder) {
            final Subject subject = (Subject) item;
            SubjectViewHolder itemView = (SubjectViewHolder) holder;

            // number
            itemView.mNumber.setText(Integer.toString(subject.number) + ".");

            // subjectName
            if ((subject.selected || subject.passed) && (subject.subjectName.contains(mContext.getResources().getString(R.string.or))
                    || subject.subjectName.contains(mContext.getString(R.string.elective_subject))
                    || subject.subjectName.contains(mContext.getString(R.string.elective_block)))) {
                itemView.mSubjectName.setText(subject.selectedSubject);
            } else {
                itemView.mSubjectName.setText(subject.subjectName);
            }

            if (holder instanceof SubjectUnelectedViewHolder) {
                SubjectUnelectedViewHolder itemViewUnselected = (SubjectUnelectedViewHolder) holder;
                itemViewUnselected.mSemesterWinter.setText(subject.semesterDescription1);
                itemViewUnselected.mSemesterSummer.setText(subject.semesterDescription2);
            }

            // esp
            itemView.mEsp.setText(Integer.toString(subject.esp));
        }
    }


    public static class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * ViewHolder holding references to header view where we will populate our data
     */
    private static class HeaderViewHolder extends BaseViewHolder {
        public TextView mTitle, mSemester1, mSemester2;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.headerTitle);
            mSemester1 = (TextView) itemView.findViewById(R.id.headerSemester1);
            mSemester2 = (TextView) itemView.findViewById(R.id.headerSemester2);
        }
    }

    /**
     * ViewHolder holding references to subject item view where we will populate our data
     */
    private static class SubjectViewHolder extends BaseViewHolder {
        public TextView mNumber;
        public TextView mSubjectName;
        public TextView mEsp;

        public SubjectViewHolder(View itemView) {
            super(itemView);

            mNumber = (TextView) itemView.findViewById(R.id.tableNumber);
            mSubjectName = (TextView) itemView.findViewById(R.id.tableSubjectName);
            mEsp = (TextView) itemView.findViewById(R.id.tableEsp);
        }
    }

    /**
     * ViewHolder holding references to subject item view where we will populate our data
     */
    private static class SubjectUnelectedViewHolder extends SubjectViewHolder {
        public TextView mSemesterWinter, mSemesterSummer;

        public SubjectUnelectedViewHolder(View itemView) {
            super(itemView);
            mSemesterWinter = (TextView) itemView.findViewById(R.id.semesterWinter);
            mSemesterSummer = (TextView) itemView.findViewById(R.id.semesterSummer);
        }
    }

    /**
     * ViewHolder holding references to subject item view where we will populate our data
     */
    private static class SubjectSelectedViewHolder extends SubjectViewHolder {

        public SubjectSelectedViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * ViewHolder holding references to subject item view where we will populate our data
     */
    private static class SubjectPassedViewHolder extends SubjectViewHolder {

        public SubjectPassedViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * @param dataSet ArrayList<AcademicYear>
     */
    public void updateSubjects(ArrayList<AcademicYear> dataSet) {
        ArrayList<SubjectItem> dataSet1 = new ArrayList<>();

        for (AcademicYear academicYear : dataSet) {
            dataSet1.add(new HeaderSubjectItem(academicYear.year, academicYear.semester));
            dataSet1.addAll(academicYear.subjects);
        }

        updateDataset(dataSet1);
    }
}
