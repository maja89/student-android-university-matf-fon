package rs.ac.bg.student.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rs.ac.bg.student.R;
import rs.ac.bg.student.model.Subject;

/**
 * Created by mcekic on 3/25/18.
 */

public class SubjectSearchSuggestionAdapter extends BaseAdapter<Subject, SubjectSearchSuggestionAdapter.ViewHolder> {
    public static final String TAG = "SubjectSearchSuggestionAdapter";

    public SubjectSearchSuggestionAdapter(Context context) {
        super(context);
    }

    @Override
    public SubjectSearchSuggestionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item_subject, parent, false);
        if (mOnItemClickListener != null) {
            v.setOnClickListener(mOnItemClickListener);
        }
        if (mOnItemLongClickListener != null) {
            v.setOnLongClickListener(mOnItemLongClickListener);
        }
        if (mOnItemTouchListener != null) {
            v.setOnTouchListener(mOnItemTouchListener);
        }
        return new SubjectSearchSuggestionAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SubjectSearchSuggestionAdapter.ViewHolder holder, int position) {
        final String subjectName = mDataSet.get(position).subjectName;
        holder.mSubjectName.setText(subjectName);
    }

    /**
     * ViewHolder holding references to view where we will populate our data
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mSubjectName;

        public ViewHolder(View itemView) {
            super(itemView);
            mSubjectName = (TextView) itemView.findViewById(R.id.subjectName);
        }
    }
}
