package rs.ac.bg.student.adapters.data;

/**
 * Created by maja on 4/22/18.
 */

public class HeaderScheduleItem extends ScheduleItem {

    public String text;

    public HeaderScheduleItem(String text) {
        this.text = text;
    }
}
