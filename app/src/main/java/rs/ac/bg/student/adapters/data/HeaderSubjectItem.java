package rs.ac.bg.student.adapters.data;

/**
 * Created by maja on 4/22/18.
 */

public class HeaderSubjectItem extends SubjectItem {

    public int year;
    public int semester;

    /**
     * @param year
     * @param semester
     */
    public HeaderSubjectItem(int year, int semester) {
        this.year = year;
        this.semester = semester;
    }
}
