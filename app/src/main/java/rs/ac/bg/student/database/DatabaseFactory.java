package rs.ac.bg.student.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseFactory {
    private static DatabaseHelper sDBHelper;

    /**
     * @param context Context
     * @return
     */
    public static SQLiteDatabase getDatabase(Context context) {
        if (sDBHelper == null) {
            sDBHelper = new DatabaseHelper(context);
        }
        return sDBHelper.getWritableDatabase();
    }

}
