package rs.ac.bg.student.database.adapters;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import rs.ac.bg.student.model.NewsItem;

public class DatabaseAPIs {
    public static final String TAG = "DatabaseAPIs";

    /**
     * @param context
     * @return
     */
    public static ArrayList<NewsItem> getNewsItemsFromDB(Context context) {
        Log.d(TAG, "getting all from db");
        return DatabaseItemAdapter.getAllNewsItem(context);
    }

    /**
     * @param context
     * @return
     */
    public static NewsItem getNewsItemFromDB(Context context, long id) {
        Log.d(TAG, "getting news from db with id: " + id);
        return DatabaseItemAdapter.getNewsItem(context, id);
    }

    /**
     * @param context
     * @param item
     */
    public static void insertNewsItemToDB(Context context, NewsItem item) {
        Log.d(TAG, "insert news to db: " + item.toString());
        boolean insert = DatabaseItemAdapter.insertNewsItem(context, item);
        Log.d(TAG, "insert: " + insert);
    }

    /**
     * @param context
     * @return
     */
    public static void updateNewsItemFromDB(Context context, NewsItem item) {
        Log.d(TAG, "updating news from db with id: " + item.toString());
        boolean updated = DatabaseItemAdapter.updateNewsItem(context, item);
        Log.d(TAG, "updated: " + updated);
    }


    /**
     * @param context
     * @param item
     * @return
     */
    public static void deleteNewsItemFromDB(Context context, NewsItem item) {
        Log.d(TAG, "deleting news from db: " + item.toString());
        boolean deleted = DatabaseItemAdapter.deleteItem(context, item);
        Log.d(TAG, "deleted: " + deleted);
    }

    /**
     * Deletes all functions from DB.
     *
     * @param context Context
     */
    public static void clearDB(Context context) {
        Log.d(TAG, "deleting all from db");
        DatabaseItemAdapter.deleteAll(context);
    }
}
