package rs.ac.bg.student.database.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

import rs.ac.bg.student.database.DatabaseFactory;
import rs.ac.bg.student.model.NewsItem;
import rs.ac.bg.student.utils.AppUtils;

public class DatabaseItemAdapter {
    public static final String TAG = "DatabaseAdapter";

    // Tables
    private static final String TABLE_NEWS = "table_news";

    // Columns
    private static final String COLUMN_ID = BaseColumns._ID;
    private static final String COLUMN_NEWS_SUBJECT_NAME = "news_subject_name";
    private static final String COLUMN_NEWS_AUTHOR = "news_author";
    private static final String COLUMN_NEWS_TITLE = "news_title";
    private static final String COLUMN_NEWS_MESSAGE = "news_message";
    private static final String COLUMN_NEWS_TIME = "news_time";
    private static final String COLUMN_NEWS_READ = "news_read";


    public static final String CREATE_TABLE_NEWS = "CREATE TABLE " + TABLE_NEWS + " ("
            + COLUMN_ID + " LONG PRIMARY KEY, "
            + COLUMN_NEWS_SUBJECT_NAME + " TEXT NOT NULL, "
            + COLUMN_NEWS_AUTHOR + " TEXT NOT NULL, "
            + COLUMN_NEWS_TITLE + " TEXT NOT NULL, "
            + COLUMN_NEWS_MESSAGE + " TEXT NOT NULL, "
            + COLUMN_NEWS_TIME + " LONG DEFAULT 0, "
            + COLUMN_NEWS_READ + " INTEGER DEFAULT 0"
            + ")";

    private static final String[] COLUMNS_TABLE_FUNCTIONS = new String[]{COLUMN_ID,
            COLUMN_NEWS_SUBJECT_NAME, COLUMN_NEWS_AUTHOR,
            COLUMN_NEWS_TITLE, COLUMN_NEWS_MESSAGE,
            COLUMN_NEWS_TIME, COLUMN_NEWS_READ};

    /**
     * @param context
     * @param item
     */
    static boolean insertNewsItem(Context context, NewsItem item) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, item.time);
        values.put(COLUMN_NEWS_SUBJECT_NAME, item.subjectName);
        values.put(COLUMN_NEWS_AUTHOR, item.author);
        values.put(COLUMN_NEWS_TITLE, item.title);
        values.put(COLUMN_NEWS_MESSAGE, item.message);
        values.put(COLUMN_NEWS_TIME, item.time);
        values.put(COLUMN_NEWS_READ, (item.read ? 1 : 0));
        return DatabaseFactory.getDatabase(context).insert(TABLE_NEWS, null, values) != -1;
    }

    /**
     * @param context
     * @param item
     */
    static boolean updateNewsItem(Context context, NewsItem item) {
        String whereClause = COLUMN_ID + "=?";
        String[] whereArgs = new String[]{Long.toString(item.time)};
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, item.time);
        values.put(COLUMN_NEWS_SUBJECT_NAME, item.subjectName);
        values.put(COLUMN_NEWS_AUTHOR, item.author);
        values.put(COLUMN_NEWS_TITLE, item.title);
        values.put(COLUMN_NEWS_MESSAGE, item.message);
        values.put(COLUMN_NEWS_TIME, item.time);
        values.put(COLUMN_NEWS_READ, (item.read ? 1 : 0));
        return DatabaseFactory.getDatabase(context).update(TABLE_NEWS, values, whereClause, whereArgs) > 0;
    }


    /**
     * @param context
     * @param items
     */
    static void insertAll(Context context, ArrayList<NewsItem> items) {
        for (NewsItem item : items) {
            insertNewsItem(context, item);
        }
    }

    /**
     * @param context
     * @param item
     * @return
     */
    static boolean deleteItem(Context context, NewsItem item) {
        String whereClause = COLUMN_ID + "=?";
        String[] whereArgs = new String[]{Long.toString(item.time)};
        return DatabaseFactory.getDatabase(context).delete(TABLE_NEWS, whereClause, whereArgs) > 0;
    }

    /**
     * @param context
     */
    static void deleteAll(Context context) {
        DatabaseFactory.getDatabase(context).delete(TABLE_NEWS, null, null);
    }

    /**
     * @param context Context
     * @return items ArrayList<NewsItem>
     */
    static ArrayList<NewsItem> getAllNewsItem(Context context) {
        String orderBy = COLUMN_ID + " DESC";

        String selection = "";
        ArrayList<String> selectedSubjects = AppUtils.getSelectedSubjects(context);
        if (selectedSubjects.size() > 0) {
            selection += COLUMN_NEWS_SUBJECT_NAME + " IN ('', ";
            selection += "'"+selectedSubjects.get(0) +"'";
            for (int i = 1; i < selectedSubjects.size(); i++) {
                selection += ", '" + selectedSubjects.get(i) + "'";
            }
            selection += ")";
        }

        Cursor c = DatabaseFactory.getDatabase(context).query(TABLE_NEWS, COLUMNS_TABLE_FUNCTIONS, selection, null, null, null, orderBy);
        ArrayList<NewsItem> items = new ArrayList<NewsItem>();
        try {
            if (c != null) {
                while (c.moveToNext()) {
                    String subjectName = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_SUBJECT_NAME));
                    String author = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_AUTHOR));
                    String title = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_TITLE));
                    String message = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_MESSAGE));
                    long time = c.getLong(c.getColumnIndexOrThrow(COLUMN_NEWS_TIME));
                    boolean read = c.getInt(c.getColumnIndexOrThrow(COLUMN_NEWS_READ)) != 0;
                    NewsItem item = new NewsItem(subjectName, author, title, message, time, read);
                    items.add(item);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception: " + e);
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return items;
    }

    /**
     * @param context Context
     * @return item NewsItem
     */
    static NewsItem getNewsItem(Context context, long id) {
        String selection = COLUMN_ID + "=" + id;

        Cursor c = DatabaseFactory.getDatabase(context).query(TABLE_NEWS, COLUMNS_TABLE_FUNCTIONS, selection, null, null, null, null);
        try {
            if (c != null) {
                while (c.moveToNext()) {

                    String subjectName = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_SUBJECT_NAME));
                    String author = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_AUTHOR));
                    String title = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_TITLE));
                    String message = c.getString(c.getColumnIndexOrThrow(COLUMN_NEWS_MESSAGE));
                    long time = c.getLong(c.getColumnIndexOrThrow(COLUMN_NEWS_TIME));
                    boolean read = c.getInt(c.getColumnIndexOrThrow(COLUMN_NEWS_READ)) != 0;
                    return new NewsItem(subjectName, author, title, message, time, read);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception: " + e);
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }
}
