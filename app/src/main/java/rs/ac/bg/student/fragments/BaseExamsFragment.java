package rs.ac.bg.student.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ac.bg.student.R;
import rs.ac.bg.student.animations.DepthPageTransformer;
import rs.ac.bg.student.fragments.navigation.NavigationFragment;
import rs.ac.bg.student.fragments.navigation.ScheduleExamsFragment;
import rs.ac.bg.student.fragments.navigation.ScheduleMidtermFragment;
import rs.ac.bg.student.model.Date;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.network.Requests;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.AppUtils;

/**
 * Created by maja on 4/24/18.
 */

public abstract class BaseExamsFragment extends NavigationFragment {
    public static final String TAG = "BaseExamsFragment";

    private TextView mTextView, mTitle;
    private ImageButton mPreviousButton, mForwardButton;
    private LinearLayout mHeaderView;
    private ViewPager mViewPager;
    private String mTag;
    private int mPosition;

    private ArrayList<Term> mTerms = null;

    private RelativeLayout mLoadingLayout;
    private ProgressBar mProgressBar;
    private TextView mMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this instanceof ScheduleMidtermFragment) {
            mTerms = Preferences.loadMidtermSchedules(getContext());
            mTag = ScheduleMidtermFragment.TAG;
        } else if (this instanceof ScheduleExamsFragment) {
            mTerms = Preferences.loadExamsSchedules(getContext());
            mTag = ScheduleExamsFragment.TAG;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_schedule_exams, container, false);

        // set title for this fragment
        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (toolbar != null) {
            if (this instanceof ScheduleMidtermFragment) {
                toolbar.setTitle(R.string.nav_4_schedule_midterm);
            } else if (this instanceof ScheduleExamsFragment) {
                toolbar.setTitle(R.string.nav_5_schedule_exams);
            }
        }

        mLoadingLayout = (RelativeLayout) rootView.findViewById(R.id.layoutLoading);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mMessage = (TextView) rootView.findViewById(R.id.textView);

        mTextView = (TextView) rootView.findViewById(R.id.screenMessage);
        mTextView.setVisibility(View.GONE);
        mHeaderView = (LinearLayout) rootView.findViewById(R.id.header);
        mHeaderView.setVisibility(View.GONE);
        mTitle = (TextView) rootView.findViewById(R.id.title);
        mPreviousButton = (ImageButton) rootView.findViewById(R.id.previousButton);
        mForwardButton = (ImageButton) rootView.findViewById(R.id.forwardButton);

        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPosition == 0) return;

                mPosition -= 1;
                setItem();
            }
        });
        mForwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPosition == mTerms.size() - 1) return;

                mPosition += 1;
                setItem();
            }
        });

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPosition = position;
                setItem();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setUI();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mTerms != null) {
            setUI();
        } else {
            mMessage.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mLoadingLayout.setVisibility(View.VISIBLE);
            if (this instanceof ScheduleMidtermFragment) {
                Requests.getMidterm(getContext(), new Requests.RequestTermCallback() {
                    @Override
                    public void onFailure() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgressBar.setVisibility(View.GONE);
                                mMessage.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onResponse(final ArrayList<Term> response) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mLoadingLayout.setVisibility(View.GONE);
                                if (response == null) return;
                                mTerms = (ArrayList<Term>) response;
                                setUI();
                                Preferences.saveMidtermSchedules(getContext(), mTerms);

                            }
                        });
                    }
                });
            } else if (this instanceof ScheduleExamsFragment) {
                Requests.getTerm(getContext(), new Requests.RequestTermCallback() {
                    @Override
                    public void onFailure() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgressBar.setVisibility(View.GONE);
                                mMessage.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onResponse(final ArrayList<Term> response) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mLoadingLayout.setVisibility(View.GONE);
                                if (response == null) return;
                                mTerms = (ArrayList<Term>) response;
                                setUI();
                                Preferences.saveExamsSchedules(getContext(), mTerms);
                            }
                        });
                    }
                });
            }
        }
    }

    private void setUI() {

        ArrayList<String> selectedSubjects = AppUtils.getSelectedSubjects(getContext());
        // set UI
        if (mTerms == null) {
            mHeaderView.setVisibility(View.GONE);
            mTextView.setVisibility(View.GONE);
        } else if (selectedSubjects == null || selectedSubjects.size() == 0) {
            mHeaderView.setVisibility(View.GONE);
            //show select subjects message
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.select_subjects_text);

        } else if (mTerms.size() == 0) {
            mHeaderView.setVisibility(View.GONE);
            //show empty schedule message
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.no_schedule_text);

        } else {
            mTextView.setVisibility(View.GONE);
            mHeaderView.setVisibility(View.VISIBLE);

            PagesAdapter adapter = new PagesAdapter(getChildFragmentManager(), mTerms.size());
            mViewPager.setAdapter(adapter);

            Date currentDate = AppUtils.getCurrentDate();
            // TODO: set start position based on currentDate
            mPosition = 0;
            setItem();
        }
    }

    private void setItem() {
        mPreviousButton.setVisibility(mPosition == 0 ? View.INVISIBLE : View.VISIBLE);
        mForwardButton.setVisibility(mPosition == mTerms.size() - 1 ? View.INVISIBLE : View.VISIBLE);

        mTitle.setText(mTerms.get(mPosition).description);
        mViewPager.setCurrentItem(mPosition);
    }

    public class PagesAdapter extends FragmentPagerAdapter {
        int pages;

        public PagesAdapter(FragmentManager fm, int pages) {
            super(fm);
            this.pages = pages;
        }

        @Override
        public Fragment getItem(int position) {
            ExamsFragment fragment = new ExamsFragment();
            Bundle arguments = new Bundle();
            arguments.putInt(ExamsFragment.KEY_POSITION, position);
            arguments.putString(ExamsFragment.KEY_TAG, mTag);
            fragment.setArguments(arguments);
            return fragment;
        }

        @Override
        public int getCount() {
            return this.pages;
        }
    }
}
