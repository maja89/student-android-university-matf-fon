package rs.ac.bg.student.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ac.bg.student.R;
import rs.ac.bg.student.adapters.ScheduleAdapter;
import rs.ac.bg.student.adapters.data.ScheduleItem;
import rs.ac.bg.student.fragments.navigation.ScheduleExamsFragment;
import rs.ac.bg.student.fragments.navigation.ScheduleMidtermFragment;
import rs.ac.bg.student.model.ScheduleExam;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.AppUtils;

/**
 * Created by maja on 4/24/18.
 */

public class ExamsFragment extends BaseFragment {
    public static final String TAG = "ExamsFragment";

    public static final String KEY_POSITION = "position";
    public static final String KEY_TAG = "tag";


    private TextView mTextView;

    /**
     * Reference to recycler view
     */
    private RecyclerView mRecyclerView;

    /**
     * Data adapter
     */
    private ScheduleAdapter mAdapter;

    private ArrayList<ScheduleExam> mExamSchedules;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments == null) return;

        int position = arguments.getInt(KEY_POSITION);
        String tag = arguments.getString(KEY_TAG);

        ArrayList<Term> terms = null;
        if (ScheduleMidtermFragment.TAG.equals(tag)) {
            terms = Preferences.loadMidtermSchedules(getContext());
        } else if (ScheduleExamsFragment.TAG.equals(tag)) {
            terms = Preferences.loadExamsSchedules(getContext());
        }

        if (terms != null && terms.size() > 0) {
            mExamSchedules = terms.get(position).schedules;
        } else {
            mExamSchedules = new ArrayList<>();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_exams, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mTextView = (TextView) rootView.findViewById(R.id.screenMessage);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new ScheduleAdapter(getContext(), ScheduleAdapter.TYPE_SCHEDULE_BY_DATE);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                ScheduleItem item = mAdapter.getDataSet(itemPosition);
                if (item instanceof ScheduleExam) {
                    ScheduleExam schedule = (ScheduleExam) item;
                }
            }
        });

        updateAdapter();
        return rootView;
    }

    private void updateAdapter() {
        ArrayList<String> selectedSubjects = AppUtils.getSelectedSubjects(getContext());

        ArrayList<ScheduleExam> examsSchedules = new ArrayList<>();
        if (selectedSubjects.size() > 0) {
            for (ScheduleExam schedule : mExamSchedules) {
                for (String subjectName : selectedSubjects) {
                    if (AppUtils.areEqualSubjects(schedule.subjectName, subjectName)) {
                        examsSchedules.add(schedule);
                    }
                }
            }
        }

        if (examsSchedules.size() == 0) {
            //show empty schedule message
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.no_schedule_text);
        } else {
            //show exams schedule
            mTextView.setVisibility(View.GONE);
        }

        mAdapter.updateExamSchedule(examsSchedules, true);
    }
}
