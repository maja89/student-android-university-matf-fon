package rs.ac.bg.student.fragments;

import android.app.Activity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.adapters.SubjectSearchSuggestionAdapter;
import rs.ac.bg.student.model.ElectiveSubjects;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by mcekic on 3/24/18.
 */

public class SearchFragment extends BaseFragment {
    public static final String TAG = "SearchFragment";

    /**
     * Use this listener to forward actions from fragment to activity.
     */
    private OnSearchListener mOnSearchListener;

    public interface OnSearchListener {
        void onItemSelected(String tag, String subjectSelected);
    }

    public void setOnSearchListener(OnSearchListener listener) {
        mOnSearchListener = listener;
    }

    private RecyclerView mRecyclerView;
    private SubjectSearchSuggestionAdapter mAdapter;
    private EditText mSearchView;
    private String mSearchQuery = "";

    private String mTag;
    private Subject mSubject = null;

    private ArrayList<Subject> mSubjects = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        mSearchView = (EditText) rootView.findViewById(R.id.searchView);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // clear button
        ImageView clearButton = (ImageView) rootView.findViewById(R.id.clearButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSearch();
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Initialize result list
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new SubjectSearchSuggestionAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                Subject subjectSelected = mAdapter.getDataSet(itemPosition);

                if (mOnSearchListener != null) {
                    mOnSearchListener.onItemSelected(mTag, subjectSelected.subjectName);
                }
            }
        });

        // TODO; setOnKeyListener
        /*mSearchView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (!isHidden()) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DEL) {
                            // search key
                            doSearch();
                            return true;
                        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                            // back key
                            if (mOnSearchListener != null) {
                                mOnSearchListener.onItemSelected(mSubject, "");
                            }
                            return true;
                        }
                    }
                }
                return false;
            }
        });*/

        mSearchView.setText(mSearchQuery);

        mSearchView.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                mSearchQuery = mSearchView.getText().toString();
                if (enoughToFilter()) {
                    doSearch();
                } else {
                    // When there's no input
                    mAdapter.updateDataset(mSubjects);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        getActivity().setDefaultKeyMode(Activity.DEFAULT_KEYS_SEARCH_LOCAL);
    }

    private void setSubjects() {
        if (mSubject == null) return;
        String subjectName = mSubject.subjectName;

        mSubjects.clear();

        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            if (subjectName.contains(getResources().getString(R.string.or))) {
                /**
                 *  subjectName example
                 *  Енглески језик струке 2 или Француски језик струке 2
                 */
                int offset = getString(R.string.or).length() + 1;
                mSubjects.add(new Subject(subjectName.substring(0, subjectName.indexOf(getString(R.string.or)))));
                mSubjects.add(new Subject(subjectName.substring(subjectName.indexOf(getString(R.string.or)) + offset, subjectName.length())));
            } else if (subjectName.contains(getResources().getString(R.string.elective_subject))) {
                /**
                 * subjectName example
                 * Изборни предмет ИСиТ-1
                 */
                Section section = Preferences.loadSection(getContext());
                if (section != null) {
                    /**
                     * listDescription examples
                     *  1. Листа изборних предмета ИСиТ-2, ИСиТ-3, ИСиТ-4 – 8 семестар
                     *  2. Листа изборних предмета са Математичког факултета које могу да слушају и полажу студенти Факултета организационих наука
                     */
                    for (ElectiveSubjects electives : section.electiveSubjects) {
                        String listName = subjectName.substring(getString(R.string.elective_subject).length() + 1, subjectName.length());
                        if (electives.listDescription.contains(listName)) {
                            // 1. Листа изборних предмета ИСиТ-2, ИСиТ-3, ИСиТ-4 – 8 семестар
                            mSubjects.addAll(electives.subjects);
                        } else if (!electives.listDescription.contains(listName.substring(0, listName.length() - 1))) {
                            // 2. Листа изборних предмета са Математичког факултета које могу да слушају и полажу студенти Факултета организационих наука
                            mSubjects.addAll(electives.subjects);
                        }
                    }

                }
            }
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            if (subjectName.contains(getResources().getString(R.string.elective_subject))) {
                Section section = Preferences.loadSection(getContext());
                if (section != null) {
                    for (ElectiveSubjects electives : section.electiveSubjects) {
                        String listName = subjectName.substring(getString(R.string.elective_subject).length() + 1, subjectName.length() - 1);
                        if (listName.isEmpty()) {
                            /**
                             * Изборни предмети (група К)
                             */
                            if (electives.listDescription.equals("Изборни предмети (група К)")
                                    || electives.listDescription.equals("Изборни предмети (група Р)")) {
                                mSubjects.addAll(electives.subjects);
                            }
                        } else {
                            /**
                             * Изборни предмети (група Р)
                             * Изборни предмети (група О)
                             * Изборни предмети (група М)
                             */
                            if (electives.listDescription.contains(listName)) {
                                mSubjects.addAll(electives.subjects);
                            }
                        }
                    }
                }
            } else if (subjectName.contains(getResources().getString(R.string.elective_block))) {
                Section section = Preferences.loadSection(getContext());
                if (section != null) {
                    for (ElectiveSubjects electives : section.electiveSubjects) {
                        String listName = subjectName.substring(getString(R.string.elective_block).length() + 1);
                        /**
                         * Изборни блок MM1
                         * Изборни блок MM2
                         * Изборни блок MM3
                         */
                        if (electives.listDescription.contains(listName)) {
                            mSubjects.addAll(electives.subjects);
                        }
                    }
                }
            }
        }
    }

    private void doSearch() {
        ArrayList<Subject> subjects = new ArrayList<>();
        for (Subject subject : mSubjects) {
            if (subject.subjectName.toLowerCase().contains(mSearchQuery)) {
                subjects.add(subject);
            }
        }
        mAdapter.updateDataset(subjects);
    }

    private boolean enoughToFilter() {
        return mSearchQuery.length() >= 1;
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible()) {
            showKeyboard();
        } else {
            hideKeyboard();
        }
    }

    private void clearSearch() {
        mSearchQuery = "";
        mSearchView.setText(mSearchQuery);
    }

    private void showKeyboard() {
        // shows keyboard
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity().getBaseContext());
        }
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        mSearchView.requestFocus();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity().getBaseContext());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void onShowFragment(String tag, Subject subject) {
        clearSearch();
        showKeyboard();

        mSubject = subject;
        mTag = tag;

        setSubjects();
        mAdapter.updateDataset(mSubjects);
    }

    public void onHideFragment() {
        hideKeyboard();
    }

}
