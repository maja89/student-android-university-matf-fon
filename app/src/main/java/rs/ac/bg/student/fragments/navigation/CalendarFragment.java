package rs.ac.bg.student.fragments.navigation;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import rs.ac.bg.calendar.listeners.OnDayClickListener;
import rs.ac.bg.calendar.model.EventDay;
import rs.ac.bg.student.R;
import rs.ac.bg.student.adapters.ScheduleAdapter;
import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Date;
import rs.ac.bg.student.model.Interval;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.ScheduleExam;
import rs.ac.bg.student.network.Requests;
import rs.ac.bg.student.network.ResponseObject;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.AppUtils;
import rs.ac.bg.student.utils.Constants;
import rs.ac.bg.student.utils.Resources;
import rs.ac.bg.calendar.MaterialCalendarView;

/**
 * Created by mcekic on 3/5/18.
 */

public class CalendarFragment extends NavigationFragment {
    public static final String TAG = "CalendarFragment";

    private MaterialCalendarView mCalendarView;
    private TextView mTitle;

    private CalendarYear mCalendarYear;

    private RelativeLayout mLoadingLayout;
    private ProgressBar mProgressBar;
    private TextView mMessage;

    private Calendar mMin = null, mMax = null, mCurrent = null;

    /**
     * Data Adapter
     */
    private ScheduleAdapter mAdapter;

    private List<EventDay> mEvents = new ArrayList<>();
    private HashMap<String, ArrayList<ScheduleClass>> sMapScheduleClasses = new HashMap<>();
    private HashMap<Double, ArrayList<ScheduleExam>> sMapScheduleExams = new HashMap<>();
    private HashMap<Double, ArrayList<ScheduleClass>> sMapScheduleClassEvents = new HashMap<>();
    private HashMap<Double, ArrayList<ScheduleExam>> sMapScheduleExamEvents = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCalendarYear = Preferences.loadCalendarYear(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        // set title for this fragment
        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (toolbar != null) {
            toolbar.setTitle(R.string.nav_1_calendar);
        }

        mTitle = (TextView) rootView.findViewById(R.id.title);

        mLoadingLayout = (RelativeLayout) rootView.findViewById(R.id.layoutLoading);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mMessage = (TextView) rootView.findViewById(R.id.textView);

        mCalendarView = (MaterialCalendarView) rootView.findViewById(R.id.calendarView);
        mCalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                onSelectedDay(eventDay.getCalendar());
            }
        });

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new ScheduleAdapter(getContext(), ScheduleAdapter.TYPE_SCHEDULE_BY_DATE);
        recyclerView.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mCalendarYear != null) {
            setCalendarYear();
        } else {
            mMessage.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mLoadingLayout.setVisibility(View.VISIBLE);
            //TODO: remove getContext
            Requests.getCalendar(getContext(), new Requests.RequestsCallback() {
                @Override
                public void onFailure() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                            mMessage.setVisibility(View.VISIBLE);
                        }
                    });
                }

                @Override
                public void onResponse(final ResponseObject response) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoadingLayout.setVisibility(View.GONE);
                            if (response == null || response.result == null) return;

                            if (response.result instanceof CalendarYear) {
                                mCalendarYear = (CalendarYear) response.result;
                                Preferences.saveCalendarYear(getContext(), mCalendarYear);
                                setCalendarYear();
                            }
                        }
                    });
                }
            });
        }
    }

    private void setCalendarYear() {
        // calendar min/max date
        setCalendarMinMaxDate();
        // calendar dates
        setCalendarDates();

        // sets current time
        mCalendarView.setDate(mCurrent);

        // set current day event
        onSelectedDay(mCurrent);

        // calendar events
        setEvents();
    }

    private void setEvents() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mMessage.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                mLoadingLayout.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                addAllEvents();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mProgressBar.setVisibility(View.GONE);
                mLoadingLayout.setVisibility(View.GONE);
                //sets events
                mCalendarView.setEvents(mEvents);
                // sets current time
                if (mCurrent != null) {
                    mCalendarView.setDate(mCurrent);
                    // set current day event
                    onSelectedDay(mCurrent);
                }
            }
        }.execute();
    }

    private void setCalendarMinMaxDate() {
        // sets min date
        Date startTime = mCalendarYear.workingWeeks.get(0).startDate;
        // min set date is disabled,
        // set previous day as min
        startTime = startTime.previousDate();
        mMin = Calendar.getInstance();
        mMin.set(startTime.year, startTime.month - 1, startTime.day);
        mCalendarView.setMinimumDate(mMin);

        // sets max date
        Date endTime = mCalendarYear.examWeeks.get(mCalendarYear.examWeeks.size() - 1).endDate;
        mMax = Calendar.getInstance();
        mMax.set(endTime.year, endTime.month - 1, endTime.day);
        mCalendarView.setMaximumDate(mMax);

        // sets current time
        mCurrent = Calendar.getInstance();
        if (mCurrent.getTimeInMillis() > mMax.getTimeInMillis()) {
            mCurrent = mMax;
        } else if (mCurrent.getTimeInMillis() < mMin.getTimeInMillis()) {
            mCurrent = mMin;
        }
    }

    private void setCalendarDates() {
        int background = getResources().getColor(R.color.colorAccent);
        int argb = Color.argb(Color.alpha(background), Color.red(background), Color.green(background), Color.blue(background));
        mCalendarView.getCalendarPageAdapter().addCalendarDates(mCalendarYear.getWorkingCalendarDates(), argb);

        background = getResources().getColor(R.color.midterm_color);
        argb = Color.argb(Color.alpha(background), Color.red(background), Color.green(background), Color.blue(background));
        mCalendarView.getCalendarPageAdapter().addCalendarDates(mCalendarYear.getMidtermCalendarDates(), argb);

        background = getResources().getColor(R.color.table_yellow);
        argb = Color.argb(Color.alpha(background), Color.red(background), Color.green(background), Color.blue(background));
        mCalendarView.getCalendarPageAdapter().addCalendarDates(mCalendarYear.getExamsCalendarDates(), argb);
    }

    private void setMapScheduleTeaching() {
        sMapScheduleClasses.put(ScheduleClass.DAY_MONDAY,
                AppUtils.getSelectedSchedulesByDayOfTheWeek(getContext(), ScheduleClass.DAY_MONDAY));
        sMapScheduleClasses.put(ScheduleClass.DAY_TUESDAY,
                AppUtils.getSelectedSchedulesByDayOfTheWeek(getContext(), ScheduleClass.DAY_TUESDAY));
        sMapScheduleClasses.put(ScheduleClass.DAY_WEDNESDAY,
                AppUtils.getSelectedSchedulesByDayOfTheWeek(getContext(), ScheduleClass.DAY_WEDNESDAY));
        sMapScheduleClasses.put(ScheduleClass.DAY_THURSDAY,
                AppUtils.getSelectedSchedulesByDayOfTheWeek(getContext(), ScheduleClass.DAY_THURSDAY));
        sMapScheduleClasses.put(ScheduleClass.DAY_FRIDAY,
                AppUtils.getSelectedSchedulesByDayOfTheWeek(getContext(), ScheduleClass.DAY_FRIDAY));
    }

    private void setMapScheduleExam() {
        ArrayList<ArrayList<ScheduleExam>> list = AppUtils.getMySchedulesByDate(getContext());
        for (ArrayList<ScheduleExam> selected : list) {
            Date date = selected.get(0).date;
            sMapScheduleExams.put(getKey(date), selected);
        }
    }

    private void addAllEvents() {
        setMapScheduleTeaching();
        setMapScheduleExam();

        sMapScheduleClassEvents.clear();
        sMapScheduleExamEvents.clear();
        mEvents.clear();

        /**
         * Adds all Teaching schedules
         */
        ArrayList<Date> days = mCalendarYear.getWorkingDates();
        for (Date date : days) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(date.year, date.month - 1, date.day);
            String dayOfWeekString = Constants.getDayOfTheWeekString(calendar.get(Calendar.DAY_OF_WEEK));
            Interval semesterInterval = AppUtils.getSemesterTimetableInterval(getContext());

            if (semesterInterval != null && (date.equals(semesterInterval.startDate) || date.after(semesterInterval.startDate))
                    && (date.equals(semesterInterval.endDate) || date.before(semesterInterval.endDate))) {

                ArrayList<ScheduleClass> scheduleClass = null;
                if ((dayOfWeekString.equals(ScheduleClass.DAY_SATURDAY) || dayOfWeekString.equals(ScheduleClass.DAY_SUNDAY))) {
                    String dayString = AppUtils.getWorkingDay(getContext(), date);
                    if (dayString != null) {
                        scheduleClass = sMapScheduleClasses.get(dayString);
                    }
                } else {
                    scheduleClass = sMapScheduleClasses.get(dayOfWeekString);
                }

                if (scheduleClass != null && scheduleClass.size() > 0) {
                    // add event
                    mEvents.add(new EventDay(calendar, R.drawable.ic_schedule_white_24dp));

                    // add to map
                    sMapScheduleClassEvents.put(getKey(date), scheduleClass);
                }
            }
        }

        /**
         * Add all Exam schedules
         */
        for (double key : sMapScheduleExams.keySet()) {
            // add event
            Date date = getDate(key);
            Calendar calendar = Calendar.getInstance();
            calendar.set(date.year, date.month - 1, date.day);
            mEvents.add(new EventDay(calendar, R.drawable.ic_event_note_white_24dp));

            // add to map
            ArrayList<ScheduleExam> scheduleExams = sMapScheduleExams.get(key);
            sMapScheduleExamEvents.put(key, scheduleExams);
        }
    }

    /**
     * When day is selected.
     * Sets title.
     *
     * @param calendar
     */
    private void onSelectedDay(Calendar calendar) {

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        // sets title
        String title = String.format(getResources().getString(R.string.calendar_title),
                Resources.getDayOfTheWeekResource(getContext(), dayOfWeek),
                dayOfMonth, Resources.getMonthResource(getContext(), month), year);
        mTitle.setText(title);

        Date date = new Date(year, month + 1, dayOfMonth);
        setEventOnSelectedDay(date);
    }

    /**
     * Sets list for the day.
     * <p>
     *
     * @param date Date
     */
    private void setEventOnSelectedDay(Date date) {
        mAdapter.clear();

        ArrayList<ScheduleClass> scheduleClass = sMapScheduleClassEvents.get(getKey(date));
        if (scheduleClass != null) {
            mAdapter.updateClassSchedule(scheduleClass);
        }

        ArrayList<ScheduleExam> scheduleExams = sMapScheduleExamEvents.get(getKey(date));
        if (scheduleExams != null) {
            mAdapter.updateExamSchedule(scheduleExams, false);
        }
    }

    /**
     * @param date
     * @return
     */
    private double getKey(Date date) {
        return ((date.year * Math.pow(10, 2)) + date.month) * Math.pow(10, 2) + date.day;
    }

    /**
     * @param key
     * @return
     */
    private Date getDate(double key) {
        int year = (int) (key / Math.pow(10, 4));
        int month = (int) ((int) (key % Math.pow(10, 4)) / Math.pow(10, 2));
        int day = (int) (key % Math.pow(10, 2));
        return new Date(year, month, day);
    }
}
