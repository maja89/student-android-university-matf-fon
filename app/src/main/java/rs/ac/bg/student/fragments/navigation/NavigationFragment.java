package rs.ac.bg.student.fragments.navigation;

import androidx.fragment.app.FragmentManager;

import rs.ac.bg.student.StudentActivity;
import rs.ac.bg.student.fragments.BaseFragment;

/**
 * Created by mcekic on 3/25/18.
 */

public abstract class NavigationFragment extends BaseFragment {
    public static final String TAG = "NavigationFragment";

    public static NavigationFragment getFragment(StudentActivity activity, String tag) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        return (NavigationFragment) fragmentManager.findFragmentByTag(tag);
    }
}
