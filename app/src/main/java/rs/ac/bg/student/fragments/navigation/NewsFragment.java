package rs.ac.bg.student.fragments.navigation;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ac.bg.student.R;
import rs.ac.bg.student.activities.CreateNewsActivity;
import rs.ac.bg.student.activities.NewsActivity;
import rs.ac.bg.student.adapters.NewsAdapter;
import rs.ac.bg.student.database.adapters.DatabaseAPIs;
import rs.ac.bg.student.model.NewsItem;

/**
 * Created by mcekic on 3/5/18.
 */

public class NewsFragment extends NavigationFragment {
    public static final String TAG = "NewsFragment";

    public static final int REQUEST_MESSAGE = 21;
    public static final int REQUEST_CREATE = 22;

    private TextView mTextView;

    /**
     * Data Adapter
     */
    private NewsAdapter mAdapter;

    private RecyclerView mRecyclerView;

    private ArrayList<NewsItem> mNewsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNewsList = DatabaseAPIs.getNewsItemsFromDB(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);

        // set title for this fragment
        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (toolbar != null) {
            toolbar.setTitle(R.string.nav_2_news);
        }

        mTextView = (TextView) rootView.findViewById(R.id.screenMessage);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new NewsAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                NewsItem item = mAdapter.getItem(itemPosition);
                if (item == null) return;

                item.read = true;
                Intent intent = new Intent(getActivity(), NewsActivity.class);
                intent.putExtra(NewsActivity.EXTRA_ID, item.time);
                startActivityForResult(intent, REQUEST_MESSAGE);
            }
        });

        setUI();

        return rootView;
    }

    private void setUI() {
        if (mNewsList == null || mNewsList.size() == 0) {
            //show select schedule message
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.no_news_text);

        } else {
            //show schedule
            mTextView.setVisibility(View.GONE);
        }

        // update adapter
        mAdapter.updateDataset(mNewsList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUI();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_MESSAGE == requestCode) {
            mAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     *
     */
    public void onAddClicked() {
        Intent intent = new Intent(getActivity(), CreateNewsActivity.class);
        startActivity(intent);
    }
}
