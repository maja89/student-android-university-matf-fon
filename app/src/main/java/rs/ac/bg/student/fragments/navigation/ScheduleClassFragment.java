package rs.ac.bg.student.fragments.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import rs.ac.bg.student.R;
import rs.ac.bg.student.activities.SelectScheduleActivity;
import rs.ac.bg.student.adapters.ScheduleAdapter;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.AppUtils;

/**
 * Created by mcekic on 3/5/18.
 */

public class ScheduleClassFragment extends NavigationFragment {
    public static final String TAG = "ScheduleClassFragment";

    public static final int REQUEST_SCHEDULE = 31;

    private HashMap<String, Boolean> mMapSelectedSubjects = new HashMap<>();

    private TextView mTextView;

    /**
     * Data Adapter
     */
    private ScheduleAdapter mAdapter;

    private ArrayList<ScheduleClass> mSchedules;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Timetable timetable = Preferences.loadTimetableSelected(getContext());
        if (timetable != null && timetable.schedules != null) {
            mSchedules = timetable.schedules;
        }
        mMapSelectedSubjects = new HashMap<>();
        ArrayList<String> selectedSubjects = AppUtils.getSelectedSubjects(getContext());
        if (selectedSubjects.size() != 0) {
            for (String subjectName : selectedSubjects) {
                mMapSelectedSubjects.put(subjectName, true);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_schedule_class, container, false);

        // set title for this fragment
        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (toolbar != null) {
            toolbar.setTitle(R.string.nav_3_schedule_class);
        }

        mTextView = (TextView) rootView.findViewById(R.id.screenMessage);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new ScheduleAdapter(getContext(), ScheduleAdapter.TYPE_SCHEDULE_BY_DAY_OF_THE_WEEK);
        recyclerView.setAdapter(mAdapter);

        setUI();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        update();
    }

    /**
     * Updates schedule.
     */
    private void update() {
        Timetable timetable = Preferences.loadTimetableAll(getContext());
        if (timetable == null) return;

        mSchedules = new ArrayList<>();

        if (mMapSelectedSubjects.size() == 0) {
            // no schedule, update ui
            setUI();
            // reset selected
            for (ScheduleClass schedule : timetable.schedules) {
                if (schedule.selected) {
                    schedule.selected = false;
                }
            }
            // update preferences
            Preferences.saveTimetableAll(getContext(), timetable);
        } else {
            // there are selected subjects
            for (ScheduleClass schedule : timetable.schedules) {
                if (schedule == null) continue;
                if (schedule.selected) {
                    if (mMapSelectedSubjects.containsKey(schedule.subjectName)) {
                        // add selected schedule for subject
                        mSchedules.add(schedule);
                    } else {
                        // subject not selected reset value
                        schedule.selected = false;
                    }
                }
            }
            // update ui with schedule
            setUI();
            // update preferences
            Preferences.saveTimetableAll(getContext(), timetable);
        }
    }

    private void setUI() {
        if (mMapSelectedSubjects.size() == 0) {
            if (mSchedules != null && mSchedules.size() > 0) {
                mTextView.setVisibility(View.GONE);
            } else {
                //show select subjects message
                mTextView.setVisibility(View.VISIBLE);
                mTextView.setText(R.string.select_subjects_text);
            }
        } else if (mSchedules == null || mSchedules.size() == 0) {
            //show select schedule message
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.select_schedule_text);

        } else {
            //show schedule
            mTextView.setVisibility(View.GONE);
        }

        // update adapter
        if (mSchedules != null) {
            mAdapter.updateClassSchedule(mSchedules);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (REQUEST_SCHEDULE == requestCode && Activity.RESULT_OK == resultCode) {
            Timetable timetable = Preferences.loadTimetableAll(getContext());
            if (timetable == null) return;

            mSchedules = new ArrayList<>();

            for (ScheduleClass schedule : timetable.schedules) {
                if (schedule.selected) {
                    mSchedules.add(schedule);
                }
            }

            setUI();

            Preferences.saveTimetableSelected(getContext(), new Timetable(timetable.interval, mSchedules));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     *
     */
    public void onAddClicked() {
        Intent intent = new Intent(getActivity(), SelectScheduleActivity.class);
        startActivityForResult(intent, REQUEST_SCHEDULE);
    }
}
