package rs.ac.bg.student.fragments.navigation;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rs.ac.bg.student.R;

/**
 * Created by mcekic on 3/5/18.
 */

public class SettingsFragment extends NavigationFragment {
    public static final String TAG = "SettingsFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        // set application versionName
        TextView appVersionText = (TextView) rootView.findViewById(R.id.appVersionText);
        appVersionText.setText(getVersionName());

        // set title for this fragment
        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (toolbar != null) {
            toolbar.setTitle(R.string.nav_7_settings);
        }

        return rootView;
    }

    /**
     * @return application versionName
     */
    private String getVersionName() {
        try {
            PackageInfo info = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not find requested package");
        }

        return "";
    }
}
