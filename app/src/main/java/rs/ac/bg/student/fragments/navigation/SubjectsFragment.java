package rs.ac.bg.student.fragments.navigation;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentActivity;
import rs.ac.bg.student.adapters.SubjectAdapter;
import rs.ac.bg.student.adapters.data.SubjectItem;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.network.Requests;
import rs.ac.bg.student.network.ResponseObject;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by mcekic on 3/5/18.
 */

public class SubjectsFragment extends NavigationFragment {
    public static final String TAG = "SubjectsFragment";

    private static final int INVALID_POSITION = -1;

    /**
     * Reference to recycler view
     */
    private RecyclerView mRecyclerView;

    /**
     * Data Adapter
     */
    private SubjectAdapter mAdapter;

    private Section mSection;

    private RelativeLayout mLoadingLayout;
    private ProgressBar mProgressBar;
    private TextView mMessage;

    private int mSelectedItemPosition = INVALID_POSITION;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSection = Preferences.loadSection(getContext());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_subjects, container, false);

        // set title for this fragment
        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (toolbar != null) {
            toolbar.setTitle(R.string.nav_6_subjects);
        }

        mLoadingLayout = (RelativeLayout) rootView.findViewById(R.id.layoutLoading);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mMessage = (TextView) rootView.findViewById(R.id.textView);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new SubjectAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                SubjectItem item = mAdapter.getDataSet(itemPosition);
                if (item instanceof Subject) {
                    Subject subject = (Subject) item;

                    // last two items should not be selected
                    String sectionName = Preferences.getSectionName(getContext());
                    if (!subject.isValidSubject(sectionName)) return;

                    if (subject.passed) return;

                    if (!subject.selected && (subject.subjectName.contains(getResources().getString(R.string.or))
                            || subject.subjectName.contains(getResources().getString(R.string.elective_subject))
                            || subject.subjectName.contains(getResources().getString(R.string.elective_block)))) {
                        // show fragment search
                        StudentActivity activity = (StudentActivity) getActivity();
                        if (activity != null) {
                            mSelectedItemPosition = itemPosition;
                            activity.showSearchFragment(SubjectsFragment.TAG, subject);
                        }
                    } else {
                        subject.selected = !subject.selected;
                        /**
                         * bug in notifyItemChanged ()
                         * removes view item
                         */
                        mAdapter.notifyDataSetChanged();
                        save();
                    }
                }
            }
        });

        mAdapter.setOnItemLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                SubjectItem item = mAdapter.getDataSet(itemPosition);
                if (item instanceof Subject) {
                    Subject subject = (Subject) item;
                    if (subject.passed) {
                        subject.passed = false;
                        mAdapter.notifyDataSetChanged();
                        save();
                        return true;
                    } else if (subject.selected) {
                        subject.passed = true;
                        subject.selected = false;
                        mAdapter.notifyDataSetChanged();
                        save();
                        return true;
                    }
                }
                return false;
            }
        });

        updateAdapter();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mSection != null) {
            updateAdapter();
        } else {
            mMessage.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mLoadingLayout.setVisibility(View.VISIBLE);
            Requests.getSection(getContext(), new Requests.RequestsCallback() {
                @Override
                public void onFailure() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                            mMessage.setVisibility(View.VISIBLE);
                        }
                    });
                }

                @Override
                public void onResponse(final ResponseObject response) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoadingLayout.setVisibility(View.GONE);
                            if (response == null || response.result == null) return;

                            if (response.result instanceof Section) {
                                mSection = (Section) response.result;
                                updateAdapter();
                                Preferences.saveSection(getContext(), mSection);
                            }
                        }
                    });
                }
            });
        }
    }

    private void updateAdapter() {
        if (mSection == null || mSection.academicYears == null) return;

        mAdapter.updateSubjects(mSection.academicYears);
    }

    /**
     * @param subjectSelected String
     */
    public void onItemSelected(String subjectSelected) {

        if (mSelectedItemPosition == INVALID_POSITION) return;

        SubjectItem item = mAdapter.getDataSet(mSelectedItemPosition);
        if (item instanceof Subject) {
            Subject subject = (Subject) item;
            if (!subject.selected) {
                subject.selected = true;
                subject.selectedSubject = subjectSelected;
                mAdapter.notifyDataSetChanged();
                save();
            }
        }
    }

    private void save() {
        Preferences.saveSection(getContext(), mSection);
    }
}
