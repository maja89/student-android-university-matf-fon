package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mcekic on 3/21/18.
 */

public class AcademicYear implements Parcelable {

    /**
     * Fon
     */
    private static final String YEAR = "year";

    /**
     * Matf
     */
    private static final String SEMESTER = "semester";

    private static final String SUBJECTS = "subjects";

    @Expose
    @SerializedName(YEAR)
    public int year;

    @Expose
    @SerializedName(SEMESTER)
    public int semester;

    @Expose
    @SerializedName(SUBJECTS)
    public ArrayList<Subject> subjects;

    /**
     * Fon
     *
     * @param year
     */
    public AcademicYear(int year) {
        this.year = year;
        this.subjects = new ArrayList<>();
    }

    /**
     * Matf
     *
     * @param year
     * @param semester
     */
    public AcademicYear(int year, int semester) {
        this.year = year;
        this.semester = semester;
        this.subjects = new ArrayList<>();
    }

    public AcademicYear(int year, int semester, ArrayList<Subject> subjects) {
        this.year = year;
        this.semester = semester;
        this.subjects = subjects;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.year);
        dest.writeInt(this.semester);
        dest.writeSerializable(this.subjects);
    }

    protected AcademicYear(Parcel in) {
        this.year = in.readInt();
        this.semester = in.readInt();
        this.subjects = (ArrayList<Subject>) in.readSerializable();
    }

    @Override
    public String toString() {
        return "AcademicYear{" +
                "year=" + year +
                "semester=" + semester +
                ", subjects=" + subjects +
                '}';
    }

    public static final Creator<AcademicYear> CREATOR = new Creator<AcademicYear>() {
        @Override
        public AcademicYear createFromParcel(Parcel source) {
            return new AcademicYear(source);
        }

        @Override
        public AcademicYear[] newArray(int size) {
            return new AcademicYear[size];
        }
    };
}
