package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mcekic on 4/2/18.
 */

public class CalendarYear implements Parcelable {

    private static final String WORKING_WEEKEND_DAYS = "working_weekend_days";

    private static final String WORKING_WEEKS = "working_weeks";
    private static final String MIDTERM_WEEKS = "midterm_weeks";
    private static final String EXAM_WEEKS = "exam_weeks";

    @Expose
    @SerializedName(WORKING_WEEKEND_DAYS)
    public ArrayList<WorkingWeekendDay> workingWeekendDays;

    @Expose
    @SerializedName(WORKING_WEEKS)
    public ArrayList<Interval> workingWeeks;

    @Expose
    @SerializedName(MIDTERM_WEEKS)
    public ArrayList<Interval> midtermWeeks;

    @Expose
    @SerializedName(EXAM_WEEKS)
    public ArrayList<Interval> examWeeks;

    public CalendarYear() {
        this.workingWeekendDays = new ArrayList<>();
        this.workingWeeks = new ArrayList<>();
        this.midtermWeeks = new ArrayList<>();
        this.examWeeks = new ArrayList<>();
    }

    public ArrayList<Date> getWorkingDates() {
        ArrayList<Date> dates = new ArrayList<>();
        for (Interval interval : workingWeeks) {
            dates.addAll(interval.getDates());
        }
        return dates;
    }

    public ArrayList<rs.ac.bg.calendar.model.Date> getWorkingCalendarDates() {
        ArrayList<rs.ac.bg.calendar.model.Date> dates = new ArrayList<>();
        for (Interval interval : workingWeeks) {
            dates.addAll(interval.getCalendarDates());
        }
        return dates;
    }

    public ArrayList<rs.ac.bg.calendar.model.Date> getMidtermCalendarDates() {
        ArrayList<rs.ac.bg.calendar.model.Date> dates = new ArrayList<>();
        for (Interval interval : midtermWeeks) {
            dates.addAll(interval.getCalendarDates());
        }
        return dates;
    }

    public ArrayList<rs.ac.bg.calendar.model.Date> getExamsCalendarDates() {
        ArrayList<rs.ac.bg.calendar.model.Date> dates = new ArrayList<>();
        for (Interval interval : examWeeks) {
            dates.addAll(interval.getCalendarDates());
        }
        return dates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.workingWeekendDays);
        dest.writeTypedList(this.workingWeeks);
        dest.writeTypedList(this.midtermWeeks);
        dest.writeTypedList(this.examWeeks);
    }

    protected CalendarYear(Parcel in) {
        this.workingWeekendDays = in.createTypedArrayList(WorkingWeekendDay.CREATOR);
        this.workingWeeks = in.createTypedArrayList(Interval.CREATOR);
        this.midtermWeeks = in.createTypedArrayList(Interval.CREATOR);
        this.examWeeks = in.createTypedArrayList(Interval.CREATOR);
    }

    @Override
    public String toString() {
        return "CalendarYear{" +
                "workingWeekendDays=" + workingWeekendDays +
                ", workingWeeks=" + workingWeeks +
                ", midtermWeeks=" + midtermWeeks +
                ", examWeeks=" + examWeeks +
                '}';
    }

    public static final Creator<CalendarYear> CREATOR = new Creator<CalendarYear>() {
        @Override
        public CalendarYear createFromParcel(Parcel source) {
            return new CalendarYear(source);
        }

        @Override
        public CalendarYear[] newArray(int size) {
            return new CalendarYear[size];
        }
    };
}
