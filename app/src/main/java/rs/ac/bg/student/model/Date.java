package rs.ac.bg.student.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.HashMap;

import rs.ac.bg.student.R;
import rs.ac.bg.student.utils.Resources;

/**
 * Created by mcekic on 4/15/18.
 */

public class Date implements Parcelable {
    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String DAY = "day";

    /**
     *
     */
    public final static int JANUARY = 1;
    public final static int FEBRUARY = 2;
    public final static int MARCH = 3;
    public final static int APRIL = 4;
    public final static int MAY = 5;
    public final static int JUNE = 6;
    public final static int JULY = 7;
    public final static int AUGUST = 8;
    public final static int SEPTEMBER = 9;
    public final static int OCTOBER = 10;
    public final static int NOVEMBER = 11;
    public final static int DECEMBER = 12;

    private static HashMap<Integer, Integer> sMaxDaysOfMonth = new HashMap<>();

    static {
        sMaxDaysOfMonth.put(JANUARY, 31);
        sMaxDaysOfMonth.put(FEBRUARY, 28);
        sMaxDaysOfMonth.put(MARCH, 31);
        sMaxDaysOfMonth.put(APRIL, 30);
        sMaxDaysOfMonth.put(MAY, 31);
        sMaxDaysOfMonth.put(JUNE, 30);
        sMaxDaysOfMonth.put(JULY, 31);
        sMaxDaysOfMonth.put(AUGUST, 31);
        sMaxDaysOfMonth.put(SEPTEMBER, 30);
        sMaxDaysOfMonth.put(OCTOBER, 31);
        sMaxDaysOfMonth.put(NOVEMBER, 30);
        sMaxDaysOfMonth.put(DECEMBER, 31);
    }

    @Expose
    @SerializedName(YEAR)
    public int year;

    @Expose
    @SerializedName(MONTH)
    public int month;
    @Expose
    @SerializedName(DAY)
    public int day;

    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    /**
     * @param calendar
     * @return
     */
    public static Date getDate(Calendar calendar) {
        return new Date(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * @param day
     * @return
     */
    public static Date getDate(java.util.Date day) {
        return new Date(day.getYear() + 1900, day.getMonth() + 1, day.getDate());
    }

    /**
     * @param day
     * @return
     */
    public static Date getDate(rs.ac.bg.calendar.model.Date day) {
        return new Date(day.year, day.month, day.day);
    }

    /**
     * @param date
     * @return
     */
    public boolean before(Date date) {
        if (year < date.year) return true;

        if (year == date.year) {
            if (month < date.month) return true;

            if (month == date.month) {
                if (day < date.day) return true;
            }
        }
        return false;
    }

    public boolean after(Date date) {
        if (year > date.year) return true;

        if (year == date.year) {
            if (month > date.month) return true;

            if (month == date.month) {
                if (day > date.day) return true;
            }
        }
        return false;
    }

    /**
     * @param context Context
     * @return
     */
    public String getText(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return String.format(context.getResources().getString(R.string.calendar_title),
                Resources.getDayOfTheWeekResource(context, dayOfWeek),
                day, Resources.getMonthResource(context, month - 1), year);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Date) {
            Date date = (Date) obj;
            return date.year == year && date.month == month && date.day == day;
        }
        return false;
    }

    public Date previousDate() {
        Date nextDate = clone();

        if (nextDate.day == 1) {
            if (nextDate.month == JANUARY) {
                nextDate.year--;
                nextDate.month = DECEMBER;
            } else {
                nextDate.month--;
            }
            nextDate.day = getMaxDaysOfMonth(nextDate.year, nextDate.month);
        } else {
            nextDate.day--;
        }
        return nextDate;
    }

    /**
     * @return
     */
    public Date nextDate() {
        Date nextDate = clone();

        if (nextDate.day == getMaxDaysOfMonth(nextDate.year, nextDate.month)) {
            if (nextDate.month == DECEMBER) {
                nextDate.year++;
                nextDate.month = JANUARY;
            } else {
                nextDate.month++;
            }
            nextDate.day = 1;
        } else {
            nextDate.day++;
        }
        return nextDate;
    }

    /**
     * @param year  int
     * @param month int
     * @return
     */
    private static int getMaxDaysOfMonth(int year, int month) {
        int maxDaysOfMonth = sMaxDaysOfMonth.get(month);
        if (month == FEBRUARY && isLeapYear(year)) {
            maxDaysOfMonth++;
        }
        return maxDaysOfMonth;
    }

    /**
     * @param year int
     * @return
     */
    private static boolean isLeapYear(int year) {
        return year % 4 == 0;
    }

    @Override
    protected Date clone() {
        return new Date(year, month, day);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.year);
        dest.writeInt(this.month);
        dest.writeInt(this.day);
    }

    protected Date(Parcel in) {
        this.year = in.readInt();
        this.month = in.readInt();
        this.day = in.readInt();
    }

    @Override
    public String toString() {
        return "Date{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }

    public static final Creator<Date> CREATOR = new Creator<Date>() {
        @Override
        public Date createFromParcel(Parcel source) {
            return new Date(source);
        }

        @Override
        public Date[] newArray(int size) {
            return new Date[size];
        }
    };
}
