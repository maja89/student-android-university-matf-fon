package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mcekic on 3/21/18.
 */

public class ElectiveSubjects implements Parcelable {

    private static final String LIST_DESCRIPTION = "list_description";
    private static final String SUBJECTS = "subjects";

    @Expose
    @SerializedName(LIST_DESCRIPTION)
    public String listDescription;

    @Expose
    @SerializedName(SUBJECTS)
    public ArrayList<Subject> subjects;

    public ElectiveSubjects(String listDescription) {
        this.listDescription = listDescription;
        this.subjects = new ArrayList<>();
    }

    public ElectiveSubjects(String listDescription, ArrayList<Subject> subjects) {
        this.listDescription = listDescription;
        this.subjects = subjects;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.listDescription);
        dest.writeSerializable(this.subjects);
    }

    protected ElectiveSubjects(Parcel in) {
        this.listDescription = in.readString();
        this.subjects = (ArrayList<Subject>) in.readSerializable();
    }

    @Override
    public String toString() {
        return "ElectiveSubjects{" +
                "listDescription='" + listDescription + '\'' +
                ", subjects=" + subjects +
                '}';
    }

    public static final Creator<ElectiveSubjects> CREATOR = new Creator<ElectiveSubjects>() {
        @Override
        public ElectiveSubjects createFromParcel(Parcel source) {
            return new ElectiveSubjects(source);
        }

        @Override
        public ElectiveSubjects[] newArray(int size) {
            return new ElectiveSubjects[size];
        }
    };
}
