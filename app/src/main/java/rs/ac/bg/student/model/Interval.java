package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mcekic on 4/2/18.
 */

public class Interval implements Parcelable {
    public static final String TAG = "Interval";

    private static final String START_DATE = "start_date";
    private static final String END_DATE = "end_date";

    @Expose
    @SerializedName(START_DATE)
    public Date startDate;

    @Expose
    @SerializedName(END_DATE)
    public Date endDate;

    public Interval(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    protected ArrayList<Date> getDates() {
        ArrayList<Date> dates = new ArrayList<>();

        if (endDate.before(startDate)) {
            return dates;
        }

        Date start = new Date(startDate.year, startDate.month, startDate.day);
        dates.add(start);

        if (startDate.equals(endDate)) {
            return dates;
        }

        Date nextDate = startDate.nextDate();
        while (!endDate.equals(nextDate)) {
            dates.add(new Date(nextDate.year, nextDate.month, nextDate.day));
            nextDate = nextDate.nextDate();
        }

        Date end = new Date(endDate.year, endDate.month, endDate.day);
        dates.add(end);
        return dates;
    }

    /**
     * @return
     */
    protected ArrayList<rs.ac.bg.calendar.model.Date> getCalendarDates() {
        ArrayList<rs.ac.bg.calendar.model.Date> dates = new ArrayList<>();

        if (endDate.before(startDate)) {
            return dates;
        }

        rs.ac.bg.calendar.model.Date start = new rs.ac.bg.calendar.model.Date(startDate.year, startDate.month, startDate.day);
        dates.add(start);

        if (startDate.equals(endDate)) {
            return dates;
        }

        Date nextDate = startDate.nextDate();
        while (!endDate.equals(nextDate)) {
            dates.add(new rs.ac.bg.calendar.model.Date(nextDate.year, nextDate.month, nextDate.day));
            nextDate = nextDate.nextDate();
        }

        rs.ac.bg.calendar.model.Date end = new rs.ac.bg.calendar.model.Date(endDate.year, endDate.month, endDate.day);
        dates.add(end);
        return dates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.startDate, flags);
        dest.writeParcelable(this.endDate, flags);
    }

    protected Interval(Parcel in) {
        this.startDate = in.readParcelable(Date.class.getClassLoader());
        this.endDate = in.readParcelable(Date.class.getClassLoader());
    }

    @Override
    public String toString() {
        return "Interval{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }

    public static final Creator<Interval> CREATOR = new Creator<Interval>() {
        @Override
        public Interval createFromParcel(Parcel source) {
            return new Interval(source);
        }

        @Override
        public Interval[] newArray(int size) {
            return new Interval[size];
        }
    };
}
