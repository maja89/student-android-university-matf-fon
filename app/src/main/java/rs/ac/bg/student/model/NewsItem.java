package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsItem implements Parcelable {

    private static final String SUBJECT_NAME = "subject_name";
    private static final String AUTHOR = "author";
    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String TIME = "time";
    private static final String READ = "read";

    @Expose
    @SerializedName(SUBJECT_NAME)
    public String subjectName;

    @Expose
    @SerializedName(AUTHOR)
    public String author;

    @Expose
    @SerializedName(TITLE)
    public String title;

    @Expose
    @SerializedName(MESSAGE)
    public String message;

    @Expose
    @SerializedName(TIME)
    public long time;

    @Expose
    @SerializedName(READ)
    public boolean read;

    /**
     * @param subjectName String
     * @param author      String
     * @param title       String
     * @param message     String
     * @param time        long
     */
    public NewsItem(String subjectName, String author, String title, String message, long time) {
        this.subjectName = subjectName;
        this.author = author;
        this.title = title;
        this.message = message;
        this.time = time;
        this.read = false;
    }

    /**
     * @param subjectName String
     * @param author      String
     * @param title       String
     * @param message     String
     * @param time        long
     * @param read        boolean
     */
    public NewsItem(String subjectName, String author, String title, String message, long time, boolean read) {
        this.subjectName = subjectName;
        this.author = author;
        this.title = title;
        this.message = message;
        this.time = time;
        this.read = read;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.subjectName);
        dest.writeString(this.author);
        dest.writeString(this.title);
        dest.writeString(this.message);
        dest.writeLong(this.time);
        dest.writeByte(this.read ? (byte) 1 : (byte) 0);
    }

    protected NewsItem(Parcel in) {
        this.subjectName = in.readString();
        this.author = in.readString();
        this.title = in.readString();
        this.message = in.readString();
        this.time = in.readLong();
        this.read = in.readByte() != 0;
    }

    @Override
    public String toString() {
        return "NewsItem{" +
                "subjectName='" + subjectName + '\'' +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", time=" + time  + '\'' +
                ", read=" + read +
                '}';
    }

    public static final Parcelable.Creator<NewsItem> CREATOR = new Parcelable.Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel source) {
            return new NewsItem(source);
        }

        @Override
        public NewsItem[] newArray(int size) {
            return new NewsItem[size];
        }
    };
}
