package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import rs.ac.bg.student.adapters.data.ScheduleItem;

/**
 * Created by mcekic on 3/26/18.
 */

public class ScheduleClass extends ScheduleItem implements Parcelable {

    /**
     *
     */
    public static final String DAY_MONDAY = "monday";
    public static final String DAY_TUESDAY = "tuesday";
    public static final String DAY_WEDNESDAY = "wednesday";
    public static final String DAY_THURSDAY = "thursday";
    public static final String DAY_FRIDAY = "friday";
    public static final String DAY_SATURDAY = "saturday";
    public static final String DAY_SUNDAY = "sunday";

    /**
     * Fon
     */
    public static final String TIME_8 = "08:15-10:00";
    public static final String TIME_10 = "10:15-12:00";
    public static final String TIME_12 = "12:15-14:00";
    public static final String TIME_14 = "14:15-16:00";
    public static final String TIME_16 = "16:15-18:00";
    public static final String TIME_18 = "18:15-20:00";
    public static final String TIME_20 = "20:15-22:00";

    public static final int TYPE_P = 0;
    public static final int TYPE_V = 1;

    private static final String DAY_OF_WEEK = "day_of_week";
    private static final String SUBJECT_NAME = "subject_name";
    private static final String TYPE = "type";
    private static final String GROUP = "group";
    private static final String TIME = "time";
    private static final String HALL = "hall";
    private static final String PROFESSOR = "professor";
    private static final String SELECTED = "selected";

    @Expose
    @SerializedName(DAY_OF_WEEK)
    public String dayOfWeek;

    @Expose
    @SerializedName(SUBJECT_NAME)
    public String subjectName;

    @Expose
    @SerializedName(TYPE)
    public int type;

    @Expose
    @SerializedName(GROUP)
    public String group;

    @Expose
    @SerializedName(TIME)
    public String time;

    @Expose
    @SerializedName(HALL)
    public String hall;

    @Expose
    @SerializedName(PROFESSOR)
    public String professor;

    @Expose
    @SerializedName(SELECTED)
    public boolean selected;

    /**
     * Fon
     *
     * @param dayOfWeek
     * @param subjectName
     * @param type
     * @param group
     * @param time
     * @param hall
     */
    public ScheduleClass(String dayOfWeek, String subjectName, int type, String group, String time, String hall) {
        this.dayOfWeek = dayOfWeek;
        this.subjectName = subjectName;
        this.type = type;
        this.group = group;
        this.time = time;
        this.hall = hall;
    }

    /**
     * Matf
     *
     * @param dayOfWeek
     * @param subjectName
     * @param type
     * @param group
     * @param time
     * @param hall
     * @param professor
     */
    public ScheduleClass(String dayOfWeek, String subjectName, int type, String group, String time, String hall, String professor) {
        this.dayOfWeek = dayOfWeek;
        this.subjectName = subjectName;
        this.type = type;
        this.group = group;
        this.time = time;
        this.hall = hall;
        this.professor = professor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.dayOfWeek);
        dest.writeString(this.subjectName);
        dest.writeInt(this.type);
        dest.writeString(this.group);
        dest.writeString(this.time);
        dest.writeString(this.hall);
        dest.writeString(this.professor);
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
    }

    protected ScheduleClass(Parcel in) {
        this.dayOfWeek = in.readString();
        this.subjectName = in.readString();
        this.type = in.readInt();
        this.group = in.readString();
        this.time = in.readString();
        this.hall = in.readString();
        this.professor = in.readString();
        this.selected = in.readByte() != 0;
    }

    @Override
    public String toString() {
        return "ScheduleClass{" +
                "dayOfWeek='" + dayOfWeek + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", type=" + type +
                ", group='" + group + '\'' +
                ", time='" + time + '\'' +
                ", hall='" + hall + '\'' +
                ", professor='" + professor + '\'' +
                ", selected=" + selected +
                '}';
    }

    public static final Parcelable.Creator<ScheduleClass> CREATOR = new Parcelable.Creator<ScheduleClass>() {
        @Override
        public ScheduleClass createFromParcel(Parcel source) {
            return new ScheduleClass(source);
        }

        @Override
        public ScheduleClass[] newArray(int size) {
            return new ScheduleClass[size];
        }
    };
}
