package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import rs.ac.bg.student.adapters.data.ScheduleItem;

/**
 * Created by mcekic on 3/26/18.
 */

public class ScheduleExam extends ScheduleItem implements Parcelable {

    public static final int TYPE_P = 0;
    public static final int TYPE_U = 1;
    public static final int TYPE_M = 2;

    private static final String DATE = "date";
    private static final String SUBJECT_NAME = "subject_name";
    private static final String TYPE = "type";
    private static final String NOTE = "note";
    private static final String TIME = "time";
    private static final String HALL = "hall";
    private static final String SELECTED = "selected";

    @Expose
    @SerializedName(DATE)
    public Date date;

    @Expose
    @SerializedName(SUBJECT_NAME)
    public String subjectName;

    @Expose
    @SerializedName(TYPE)
    public int type;

    @Expose
    @SerializedName(NOTE)
    public String note;

    @Expose
    @SerializedName(TIME)
    public String time;

    @Expose
    @SerializedName(HALL)
    public String hall;

    @Expose
    @SerializedName(SELECTED)
    public boolean selected;


    public ScheduleExam(Date date, String subjectName, int type, String note, String time, String hall) {
        this.date = date;
        this.subjectName = subjectName;
        this.type = type;
        this.note = note;
        this.time = time;
        this.hall = hall;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.date, flags);
        dest.writeString(this.subjectName);
        dest.writeInt(this.type);
        dest.writeString(this.note);
        dest.writeString(this.time);
        dest.writeString(this.hall);
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
    }

    protected ScheduleExam(Parcel in) {
        this.date = in.readParcelable(Date.class.getClassLoader());
        this.subjectName = in.readString();
        this.type = in.readInt();
        this.note = in.readString();
        this.time = in.readString();
        this.hall = in.readString();
        this.selected = in.readByte() != 0;
    }

    @Override
    public String toString() {
        return "ScheduleExam{" +
                "date=" + date +
                ", subjectName='" + subjectName + '\'' +
                ", type=" + type +
                ", note='" + note + '\'' +
                ", time='" + time + '\'' +
                ", hall='" + hall + '\'' +
                ", selected=" + selected +
                '}';
    }

    public static final Creator<ScheduleExam> CREATOR = new Creator<ScheduleExam>() {
        @Override
        public ScheduleExam createFromParcel(Parcel source) {
            return new ScheduleExam(source);
        }

        @Override
        public ScheduleExam[] newArray(int size) {
            return new ScheduleExam[size];
        }
    };
}
