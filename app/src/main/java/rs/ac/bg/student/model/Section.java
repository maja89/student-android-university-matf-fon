package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mcekic on 3/21/18.
 */

public class Section implements Parcelable {

    /**
     * Fon
     */
    public static final String SECTION_1_ISIT = "ISIT";
    public static final String SECTION_2_M = "M";
    public static final String SECTION_3_MKIS = "MKIS";
    public static final String SECTION_4_OM = "OM";

    /**
     * Matf
     */
    public static final String SECTION_1_MATEMATIKA = "MATEMATIKA";
    public static final String SECTION_2_INFORMATIKA = "INFORMATIKA";
    public static final String SECTION_3_AIA = "AIA";


    public static final String SUBSECTION_11_ML = "ML";
    public static final String SUBSECTION_12_MM = "MM";
    public static final String SUBSECTION_13_MR = "MR";
    public static final String SUBSECTION_14_MP = "MP";
    public static final String SUBSECTION_15_MS = "MS";
    public static final String SUBSECTION_16_MA = "MA";

    public static final String SUBSECTION_31_AF = "AF";
    public static final String SUBSECTION_32_AI = "AI";

    private static final String SECTION_NAME = "section_name";
    private static final String ACADEMIC_YEARS = "academic_years";
    private static final String ELECTIVE_SUBJECTS = "elective_subjects";

    @Expose
    @SerializedName(SECTION_NAME)
    public String sectionName;

    @Expose
    @SerializedName(ACADEMIC_YEARS)
    public ArrayList<AcademicYear> academicYears;

    @Expose
    @SerializedName(ELECTIVE_SUBJECTS)
    public ArrayList<ElectiveSubjects> electiveSubjects;

    public Section(String sectionName) {
        this.sectionName = sectionName;
        this.academicYears = new ArrayList<>();
        this.electiveSubjects = new ArrayList<>();
    }

    public Section(String sectionName, ArrayList<AcademicYear> academicYears, ArrayList<ElectiveSubjects> electiveSubjects) {
        this.sectionName = sectionName;
        this.academicYears = academicYears;
        this.electiveSubjects = electiveSubjects;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sectionName);
        dest.writeSerializable(this.academicYears);
        dest.writeSerializable(this.electiveSubjects);
    }

    protected Section(Parcel in) {
        this.sectionName = in.readString();
        this.academicYears = (ArrayList<AcademicYear>) in.readSerializable();
        this.electiveSubjects = (ArrayList<ElectiveSubjects>) in.readSerializable();
    }

    @Override
    public String toString() {
        return "Section{" +
                "sectionName='" + sectionName + '\'' +
                ", academicYears=" + academicYears +
                ", electiveSubjects=" + electiveSubjects +
                '}';
    }

    public static final Parcelable.Creator<Section> CREATOR = new Parcelable.Creator<Section>() {
        @Override
        public Section createFromParcel(Parcel source) {
            return new Section(source);
        }

        @Override
        public Section[] newArray(int size) {
            return new Section[size];
        }
    };
}
