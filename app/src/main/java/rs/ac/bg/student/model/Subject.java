package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.adapters.data.SubjectItem;

/**
 * Created by mcekic on 3/13/18.
 */

public class Subject extends SubjectItem implements Parcelable {

    private static final String NUMBER = "number";
    private static final String SUBJECT_NAME = "subject_name";
    private static final String SEMESTER = "semester";
    private static final String SEMESTER_DESCRIPTION_1 = "semester_description_1";
    private static final String SEMESTER_DESCRIPTION_2 = "semester_description_2";
    private static final String ESP = "esp";
    private static final String SELECTED = "selected";
    private static final String SELECTED_SUBJECT = "selected_subject";
    private static final String PASSED = "passed";

    @Expose
    @SerializedName(NUMBER)
    public int number;

    @Expose
    @SerializedName(SUBJECT_NAME)
    public String subjectName;

    @Expose
    @SerializedName(SEMESTER)
    public int semester;

    @Expose
    @SerializedName(SEMESTER_DESCRIPTION_1)
    public String semesterDescription1;

    @Expose
    @SerializedName(SEMESTER_DESCRIPTION_2)
    public String semesterDescription2;

    @Expose
    @SerializedName(ESP)
    public int esp;

    @Expose
    @SerializedName(SELECTED)
    public boolean selected;

    @Expose
    @SerializedName(SELECTED_SUBJECT)
    public String selectedSubject;

    @Expose
    @SerializedName(PASSED)
    public boolean passed;

    public Subject(String subjectName) {
        this.subjectName = subjectName;
    }

    public Subject(int number, String subjectName, int semester, String semesterDescription1, String semesterDescription2, int esp) {
        this.number = number;
        this.subjectName = subjectName;
        this.semester = semester;
        this.semesterDescription1 = semesterDescription1;
        this.semesterDescription2 = semesterDescription2;
        this.esp = esp;
    }

    /**
     * @param sectionName String
     * @return
     */
    public boolean isValidSubject(String sectionName) {

        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            switch (sectionName) {
                case Section.SECTION_1_ISIT:
                case Section.SECTION_2_M:
                case Section.SECTION_3_MKIS:
                    if (number == 44 || number == 45) return false;
                    break;
                case Section.SECTION_4_OM:
                    if (number == 43 || number == 44) return false;
                    break;
            }
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            switch (sectionName) {
                case Section.SECTION_1_MATEMATIKA:
                case Section.SECTION_2_INFORMATIKA:
                case Section.SECTION_3_AIA:
                    return true;
            }
        }
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.number);
        dest.writeString(this.subjectName);
        dest.writeInt(this.semester);
        dest.writeString(this.semesterDescription1);
        dest.writeString(this.semesterDescription2);
        dest.writeInt(this.esp);
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.selectedSubject);
        dest.writeByte(this.passed ? (byte) 1 : (byte) 0);
    }

    protected Subject(Parcel in) {
        this.number = in.readInt();
        this.subjectName = in.readString();
        this.semester = in.readInt();
        this.semesterDescription1 = in.readString();
        this.semesterDescription2 = in.readString();
        this.esp = in.readInt();
        this.selected = in.readByte() != 0;
        this.selectedSubject = in.readString();
        this.passed = in.readByte() != 0;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "number=" + number +
                ", subjectName='" + subjectName + '\'' +
                ", semester=" + semester +
                ", semesterDescription1='" + semesterDescription1 + '\'' +
                ", semesterDescription2='" + semesterDescription2 + '\'' +
                ", esp=" + esp +
                ", selected=" + selected +
                ", selectedSubject='" + selectedSubject + '\'' +
                ", passed=" + passed +
                '}';
    }

    public static final Creator<Subject> CREATOR = new Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel source) {
            return new Subject(source);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };
}
