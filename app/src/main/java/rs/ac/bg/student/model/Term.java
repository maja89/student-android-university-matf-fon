package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by maja on 4/24/18.
 */

public class Term implements Parcelable {

    private static final String DESCRIPTION = "description";
    private static final String SCHEDULES = "schedules";

    @Expose
    @SerializedName(DESCRIPTION)
    public String description;

    @Expose
    @SerializedName(SCHEDULES)
    public ArrayList<ScheduleExam> schedules;

    public Term(String description) {
        this.description = description;
    }

    public Term(String description, ArrayList<ScheduleExam> schedules) {
        this.description = description;
        this.schedules = schedules;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeTypedList(this.schedules);
    }

    protected Term(Parcel in) {
        this.description = in.readString();
        this.schedules = in.createTypedArrayList(ScheduleExam.CREATOR);
    }

    @Override
    public String toString() {
        return "Term{" +
                "description='" + description + '\'' +
                ", schedules=" + schedules +
                '}';
    }

    public static final Parcelable.Creator<Term> CREATOR = new Parcelable.Creator<Term>() {
        @Override
        public Term createFromParcel(Parcel source) {
            return new Term(source);
        }

        @Override
        public Term[] newArray(int size) {
            return new Term[size];
        }
    };
}
