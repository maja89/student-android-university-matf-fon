package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by maja on 4/30/18.
 */

public class Timetable implements Parcelable {
    private static final String INTERVAL = "interval";
    private static final String SCHEDULES = "schedules";

    @Expose
    @SerializedName(INTERVAL)
    public Interval interval;

    @Expose
    @SerializedName(SCHEDULES)
    public ArrayList<ScheduleClass> schedules;

    public Timetable(Interval interval, ArrayList<ScheduleClass> schedules) {
        this.interval = interval;
        this.schedules = schedules;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.interval, flags);
        dest.writeTypedList(this.schedules);
    }

    protected Timetable(Parcel in) {
        this.interval = in.readParcelable(Interval.class.getClassLoader());
        this.schedules = in.createTypedArrayList(ScheduleClass.CREATOR);
    }

    @Override
    public String toString() {
        return "Timetable{" +
                "interval=" + interval +
                ", schedules=" + schedules +
                '}';
    }

    public static final Parcelable.Creator<Timetable> CREATOR = new Parcelable.Creator<Timetable>() {
        @Override
        public Timetable createFromParcel(Parcel source) {
            return new Timetable(source);
        }

        @Override
        public Timetable[] newArray(int size) {
            return new Timetable[size];
        }
    };
}
