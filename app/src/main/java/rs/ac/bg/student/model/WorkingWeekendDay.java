package rs.ac.bg.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maja on 5/5/18.
 */

public class WorkingWeekendDay implements Parcelable {

    private static final String DATE = "date";
    private static final String DAY = "day";

    @Expose
    @SerializedName(DATE)
    public Date date;

    @Expose
    @SerializedName(DAY)
    public String day;

    public WorkingWeekendDay(Date date, String day) {
        this.date = date;
        this.day = day;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.date, flags);
        dest.writeString(this.day);
    }

    protected WorkingWeekendDay(Parcel in) {
        this.date = in.readParcelable(Date.class.getClassLoader());
        this.day = in.readString();
    }

    @Override
    public String toString() {
        return "WorkingWeekendDay{" +
                "date=" + date +
                ", day='" + day + '\'' +
                '}';
    }

    public static final Creator<WorkingWeekendDay> CREATOR = new Creator<WorkingWeekendDay>() {
        @Override
        public WorkingWeekendDay createFromParcel(Parcel source) {
            return new WorkingWeekendDay(source);
        }

        @Override
        public WorkingWeekendDay[] newArray(int size) {
            return new WorkingWeekendDay[size];
        }
    };
}
