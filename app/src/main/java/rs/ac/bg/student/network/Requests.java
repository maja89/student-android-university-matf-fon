package rs.ac.bg.student.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.preferences.Preferences;
import rs.ac.bg.student.utils.Logger;
import rs.ac.bg.student.utils.Utils;

/**
 * Created by mcekic on 5/10/18.
 */

public class Requests {
    public static final String TAG = "Requests";

    //TODO: setYear
    private static String sYear = "/2017-2018";

    public interface RequestsCallback {
        void onFailure();

        void onResponse(ResponseObject response);
    }

    public interface RequestTermCallback {
        void onFailure();

        void onResponse(ArrayList<Term> term);
    }

    /**
     * @param callback RequestsCallback
     */
    public static void getCalendar(final Context context, final RequestsCallback callback) {
        //TODO:remove
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            ResponseObject<CalendarYear> responseObject = new ResponseObject<>();
            responseObject.result = Utils.loadCalendarYear(context);
            callback.onResponse(responseObject);
            return;
        }

        String request = URLs.getCalendarUrl() + sYear;
        Logger.debugRequest(TAG, request);

        httpRequest(request, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Logger.debug(TAG, e.toString());
                callback.onFailure();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Logger.debugResponse(TAG, response.toString());
                if (response.isSuccessful()) {
                    String responseString = response.body().string();
                    try {
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        CalendarYear result = gson.fromJson(responseString, new TypeToken<CalendarYear>() {
                        }.getType());
                        ResponseObject<CalendarYear> responseObject = new ResponseObject<>();
                        if (result != null) {
                            responseObject.result = result;
                            callback.onResponse(responseObject);
                        } else {
                            callback.onFailure();
                        }
                    } catch (Exception e) {
                        callback.onFailure();
                    }
                } else {
                    callback.onFailure();
                }
            }
        });
    }

    /**
     * @param callback RequestsCallback
     */
    public static void getTimetable(final Context context, final RequestsCallback callback) {
        //TODO:remove
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            ResponseObject<Timetable> responseObject = new ResponseObject<>();
            responseObject.result = Utils.loadTimetable(context);
            callback.onResponse(responseObject);
            return;
        }

        String request = URLs.getTimetableUrl() + sYear;
        Logger.debugRequest(TAG, request);

        httpRequest(request, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Logger.debug(TAG, e.toString());
                callback.onFailure();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Logger.debugResponse(TAG, response.toString());
                if (response.isSuccessful()) {
                    String responseString = response.body().string();
                    try {
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        Timetable result = gson.fromJson(responseString, new TypeToken<Timetable>() {
                        }.getType());
                        ResponseObject<Timetable> responseObject = new ResponseObject<>();
                        if (result != null) {
                            responseObject.result = result;
                            callback.onResponse(responseObject);
                        } else {
                            callback.onFailure();
                        }
                    } catch (Exception e) {
                        callback.onFailure();
                    }
                } else {
                    callback.onFailure();
                }
            }
        });
    }

    /**
     * @param callback RequestsCallback
     */
    public static void getMidterm(final Context context, final RequestTermCallback callback) {
        //TODO:remove
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            ArrayList<Term> result = Utils.loadMidterm(context);
            callback.onResponse(result);
            return;
        }

        String request = URLs.getMidtermUrl() + sYear;
        Logger.debugRequest(TAG, request);

        httpRequest(request, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Logger.debug(TAG, e.toString());
                callback.onFailure();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Logger.debugResponse(TAG, response.toString());
                if (response.isSuccessful()) {
                    String responseString = response.body().string();
                    try {
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        ArrayList<Term> result = gson.fromJson(responseString, new TypeToken<ArrayList<Term>>() {
                        }.getType());
                        if (result != null) {
                            callback.onResponse(result);
                        } else {
                            callback.onFailure();
                        }
                    } catch (Exception e) {
                        callback.onFailure();
                    }
                } else {
                    callback.onFailure();
                }
            }
        });
    }

    /**
     * @param callback RequestsCallback
     */
    public static void getTerm(final Context context, final RequestTermCallback callback) {
        //TODO:remove
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            ArrayList<Term> result = Utils.loadExams(context);
            callback.onResponse(result);
            return;
        }

        String request = URLs.getTermUrl() + sYear;
        Logger.debugRequest(TAG, request);

        httpRequest(request, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Logger.debug(TAG, e.toString());
                callback.onFailure();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Logger.debugResponse(TAG, response.toString());
                if (response.isSuccessful()) {
                    String responseString = response.body().string();
                    try {
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        ArrayList<Term> result = gson.fromJson(responseString, new TypeToken<ArrayList<Term>>() {
                        }.getType());
                        if (result != null) {
                            callback.onResponse(result);
                        } else {
                            callback.onFailure();
                        }
                    } catch (Exception e) {
                        callback.onFailure();
                    }
                } else {
                    callback.onFailure();
                }
            }
        });
    }

    /**
     * @param context Context
     */
    public static void getSection(final Context context, final RequestsCallback callback) {
        //TODO:remove
        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            ResponseObject<Section> responseObject = new ResponseObject<>();
            responseObject.result = Utils.loadSection(context);
            callback.onResponse(responseObject);
            return;
        }

        String request = getSectionUrl(context);
        if (request.isEmpty()) {
            callback.onFailure();
            return;
        }
        Logger.debugRequest(TAG, request);

        httpRequest(request, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Logger.debug(TAG, e.toString());
                callback.onFailure();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Logger.debugResponse(TAG, response.toString());
                if (response.isSuccessful()) {
                    String responseString = response.body().string();
                    try {
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        Section result = gson.fromJson(responseString, new TypeToken<Section>() {
                        }.getType());
                        ResponseObject<Section> responseObject = new ResponseObject<>();
                        if (result != null) {
                            responseObject.result = result;
                            callback.onResponse(responseObject);
                        } else {
                            callback.onFailure();
                        }
                    } catch (Exception e) {
                        callback.onFailure();
                    }
                } else {
                    callback.onFailure();
                }
            }
        });
    }

    /**
     * @param context Context
     * @return
     */
    private static String getSectionUrl(Context context) {
        String sectionName = Preferences.getSectionName(context);

        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            if (Section.SECTION_1_ISIT.equals(sectionName)) {
                return URLs.getSectionISITUrl();
            } else if (Section.SECTION_2_M.equals(sectionName)) {
                return URLs.getSectionMUrl();
            } else if (Section.SECTION_3_MKIS.equals(sectionName)) {
                return URLs.getSectionMIKSUrl();
            } else if (Section.SECTION_4_OM.equals(sectionName)) {
                return URLs.getSectionOMUrl();
            }
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            if (Section.SECTION_1_MATEMATIKA.equals(sectionName)) {
                return URLs.getSectionMatematikaUrl();
            } else if (Section.SECTION_2_INFORMATIKA.equals(sectionName)) {
                return URLs.getSectionInformatikaUrl();
            } else if (Section.SECTION_3_AIA.equals(sectionName)) {
                return URLs.getSectionAstronomijaIAstrofizikaUrl();
            }
        }
        return "";
    }

    private static void httpRequest(String url, Callback callback) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .build();
        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(callback);
    }
}
