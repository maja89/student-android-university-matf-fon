package rs.ac.bg.student.network;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maja on 5/11/18.
 */

public class ResponseObject<T> implements Parcelable {

    private static final String STATUS = "status";

    private static final String RESULT = "result";

    private static final String ERROR_CODE = "code";

    private static final String MESSAGE = "message";

    @Expose
    @SerializedName(STATUS)
    public String status;

    @Expose
    @SerializedName(RESULT)
    public T result;

    @Expose
    @SerializedName(ERROR_CODE)
    public int errorCode;

    @Expose
    @SerializedName(MESSAGE)
    public String message;

    public ResponseObject() {
    }

    public ResponseObject(Object result, String status, int errorCode, String message) {
        this.status = status;
        this.result = (T) result;
        this.errorCode = errorCode;
        this.message = message;
    }

    public ResponseObject(Object result) {
        this.result = (T) result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeParcelable((Parcelable) this.result, 0);
        dest.writeInt(this.errorCode);
    }

    private ResponseObject(Parcel in, Class<T> t) {
        this.status = in.readString();
        this.result = (T) in.readParcelable(t.getClassLoader());
        this.errorCode = in.readInt();
        this.message = in.readString();
    }

    public static final Parcelable.Creator<ResponseObject> CREATOR =
            new Parcelable.Creator<ResponseObject>() {
                public ResponseObject createFromParcel(Parcel source) {
                    return new ResponseObject(source);
                }

                public ResponseObject[] newArray(int size) {
                    return new ResponseObject[size];
                }
            };
}
