package rs.ac.bg.student.network;

/**
 * Created by mcekic on 3/7/18.
 */

public class URLs {

    private static final String BASE_URL = "https://student-fon.herokuapp.com/api";

    private static final String URL_CALENDAR = "/calendar";
    private static final String URL_TIMETABLE = "/timetable";
    private static final String URL_MIDTERM = "/midterm";
    private static final String URL_TERM = "/term";
    private static final String URL_SECTION = "/section";

    /**
     * Fon
     */
    private static final String URL_ISIT = "/isit";
    private static final String URL_M = "/m";
    private static final String URL_MKIS = "/mkis";
    private static final String URL_OM = "/om";

    /**
     * Matf
     */
    private static final String URL_INFORMATIKA = "/i";
    private static final String URL_MATEMATIKA = "/m";
    private static final String URL_ASTRONOMIJA_I_ASTROFIZIKA = "/a";

    /**
     * @return
     */
    static String getBaseUrl() {
        return BASE_URL;
    }

    /**
     * @return
     */
    static String getCalendarUrl() {
        return getBaseUrl() + URL_CALENDAR;
    }

    /**
     * @return
     */
    static String getTimetableUrl() {
        return getBaseUrl() + URL_TIMETABLE;
    }

    /**
     * @return
     */
    static String getMidtermUrl() {
        return getBaseUrl() + URL_MIDTERM;
    }

    /**
     * @return
     */
    static String getTermUrl() {
        return getBaseUrl() + URL_TERM;
    }

    /**
     * Fon
     */

    /**
     * @return
     */
    static String getSectionISITUrl() {
        return getBaseUrl() + URL_SECTION + URL_ISIT;
    }

    /**
     * @return
     */
    static String getSectionMUrl() {
        return getBaseUrl() + URL_SECTION + URL_M;
    }

    /**
     * @return
     */
    static String getSectionMIKSUrl() {
        return getBaseUrl() + URL_SECTION + URL_MKIS;
    }

    /**
     * @return
     */
    static String getSectionOMUrl() {
        return getBaseUrl() + URL_SECTION + URL_OM;
    }

    /**
     * Matf
     */

    /**
     * @return
     */
    static String getSectionInformatikaUrl() {
        return getBaseUrl() + URL_SECTION + URL_INFORMATIKA;
    }

    /**
     * @return
     */
    static String getSectionMatematikaUrl() {
        return getBaseUrl() + URL_SECTION + URL_MATEMATIKA;
    }

    /**
     * @return
     */
    static String getSectionAstronomijaIAstrofizikaUrl() {
        return getBaseUrl() + URL_SECTION + URL_ASTRONOMIJA_I_ASTROFIZIKA;
    }

}
