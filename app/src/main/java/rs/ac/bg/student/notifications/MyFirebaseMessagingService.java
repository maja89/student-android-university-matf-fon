package rs.ac.bg.student.notifications;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by mcekic on 4/11/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String TAG = "MyFirebaseMessagingService";

    private static final boolean DEBUG = false;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        if (DEBUG)
            Log.d(TAG, "Refreshed token: " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (DEBUG)
            Log.d(TAG, "onMessageReceived, remoteMessage: " + remoteMessage);

        Map<String, String> remoteData = remoteMessage.getData();

        if (remoteData == null) {
            Log.w(TAG, "onMessageReceived, remoteData: " + remoteData);
            return;
        }

        if (remoteMessage.getNotification() == null) {
            Log.w(TAG, "onMessageReceived, notification: " + remoteMessage.getNotification());
            return;
        }


        String type = remoteData.get(NotificationItem.TYPE);
        if (type != null && NotificationItem.TYPE_UPDATE.equals(type)) {
            //TODO: send update

        } else {
            // create notification
            NotificationItem notificationItem = new NotificationItem();
            notificationItem.title = remoteMessage.getNotification().getTitle();
            notificationItem.message = remoteMessage.getNotification().getBody();
            notificationItem.type = type;
            notificationItem.time = System.currentTimeMillis();
            MyNotificationManager.createNotification(getBaseContext(), notificationItem);
        }
    }
}
