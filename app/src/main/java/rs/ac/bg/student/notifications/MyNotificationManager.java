package rs.ac.bg.student.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentActivity;

/**
 * Created by mcekic on 4/10/18.
 */

public class MyNotificationManager {
    public static final String TAG = "MyNotificationManager";

    public static final String CHANNEL_NAME = "Notifications";
    public static final String CHANNEL_ID_SOUND = "sound_based_notification_channel";
    public static final String CHANNEL_ID_SILENT = "silent_based_notification_channel";

    public static final int NOTIFICATION_ID = 1;

    public static void createNotification(Context context, NotificationItem notification) {
        setSingleNotificationView(context, notification.title, notification.message, notification.message, notification.time);
    }

    private static void setSingleNotificationView(Context context, String title, String smallDescription, String bigDescription, long notificationId) {

        // sets pending intent for witch activity should notification open onClick
        Intent intent = new Intent(context, StudentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel(context, false, false, false);
        }

        // crates notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(smallDescription)
                .setDefaults(Notification.DEFAULT_ALL)
                /*
                 * Sets the big view "big text" style
                 */
                .setStyle(new NotificationCompat.BigTextStyle().bigText(bigDescription))
                .setSubText(title)
                .setAutoCancel(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder.setColor(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));
        } else {
            builder.setColor(context.getResources().getColor(R.color.colorPrimary));
        }

        builder.setContentIntent(contentIntent);
        builder.setTicker(bigDescription);
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

        // sends notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) notificationId, builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String createNotificationChannel(Context context, boolean vibrate, boolean playSound, boolean flashLights) {

        String channelId = (playSound) ? CHANNEL_ID_SOUND : CHANNEL_ID_SILENT;
        int importance = (playSound) ? NotificationManager.IMPORTANCE_HIGH : NotificationManager.IMPORTANCE_LOW;

        // create android channel
        NotificationChannel androidChannel = new NotificationChannel(channelId, CHANNEL_NAME, importance);
        // Sets whether notifications posted to this channel should display notification lights
        androidChannel.enableLights(flashLights);
        // Sets whether notification posted to this channel should vibrate.
        androidChannel.enableVibration(vibrate);
        // Sets the notification light color for notifications posted to this channel
        androidChannel.setLightColor(context.getResources().getColor(R.color.colorAccent));
        // Sets whether notifications posted to this channel appear on the lockscreen or not
        androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(androidChannel);

        return channelId;
    }
}
