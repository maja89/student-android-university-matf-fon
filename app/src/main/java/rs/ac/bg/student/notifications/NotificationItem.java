package rs.ac.bg.student.notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mcekic on 4/10/18.
 */

public class NotificationItem implements Parcelable {
    public static final String TYPE_UPDATE = "Update";

    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String TITLE = "title";
    public static final String MESSAGE = "message";
    public static final String TIME = "date_created";

    @Expose
    @SerializedName(ID)
    public String id;

    @Expose
    @SerializedName(TYPE)
    public String type;

    @Expose
    @SerializedName(TITLE)
    public String title;

    @Expose
    @SerializedName(MESSAGE)
    public String message;

    @Expose
    @SerializedName(TIME)
    public long time;

    public NotificationItem() {
    }

    public NotificationItem(String id, String type, String title, String message, long time) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.message = message;
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeString(this.message);
        dest.writeLong(this.time);
    }

    protected NotificationItem(Parcel in) {
        this.id = in.readString();
        this.type = in.readString();
        this.title = in.readString();
        this.message = in.readString();
        this.time = in.readLong();
    }

    @Override
    public String toString() {
        return "NotificationItem{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", time=" + time +
                '}';
    }

    public static final Creator<NotificationItem> CREATOR = new Creator<NotificationItem>() {
        @Override
        public NotificationItem createFromParcel(Parcel source) {
            return new NotificationItem(source);
        }

        @Override
        public NotificationItem[] newArray(int size) {
            return new NotificationItem[size];
        }
    };
}
