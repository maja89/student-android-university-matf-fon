package rs.ac.bg.student.notifications.type;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import rs.ac.bg.student.R;
import rs.ac.bg.student.activities.NewsActivity;
import rs.ac.bg.student.model.NewsItem;
import rs.ac.bg.student.notifications.MyNotificationManager;

public class NewsNotification {

    public static void createNewsNotification(Context context, NewsItem item) {
        setSingleNotificationView(context, item.author + " @ " + item.title, item.title, item.message, item.time);
    }

    private static void setSingleNotificationView(Context context, String title, String smallDescription, String bigDescription, long notificationId) {


        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = MyNotificationManager.createNotificationChannel(context, false, false, false);
        }

        // crates notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(smallDescription)
                .setDefaults(Notification.DEFAULT_ALL)
                /*
                 * Sets the big view "big text" style
                 */
                .setStyle(new NotificationCompat.BigTextStyle().bigText(bigDescription))
                .setSubText(title)
                .setAutoCancel(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder.setColor(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));
        } else {
            builder.setColor(context.getResources().getColor(R.color.colorPrimary));
        }

        builder.setContentIntent(getContentPendingIntent(context, notificationId));
        builder.setTicker(bigDescription);
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

        // sends notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) notificationId, builder.build());
    }

    /**
     * Sets pending intent for witch activity should notification open onClick.
     *
     * @param context
     * @return
     */
    private static PendingIntent getContentPendingIntent(Context context, long id) {
        Intent intent = new Intent(context, NewsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.putExtra(NewsActivity.EXTRA_ID, id);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
