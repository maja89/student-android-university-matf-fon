package rs.ac.bg.student.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.model.Section;

/**
 * Created by mcekic on 3/24/18.
 */

public class Preferences {
    public static final String TAG = "Preferences";

    public static final String KEY_SECTION_NAME = "key_section_name";
    public static final String KEY_SUBSECTION_NAME = "key_subsection_name";
    public static final String KEY_SECTION_OBJECT = "key_section_object";

    public static final String KEY_TIMETABLE_ALL_OBJECT = "key_timetable_all_object";
    public static final String KEY_TIMETABLE_SELECTED_OBJECT = "key_timetable_selected_object";

    public static final String KEY_MIDTERM_SCHEDULES_OBJECT = "key_midterm_schedules_object";
    public static final String KEY_EXAMS_SCHEDULES_OBJECT = "key_exams_schedules_object";

    public static final String KEY_CALENDAR_YEAR_OBJECT = "key_calendar_year_object";

    public static void clear(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().commit();
    }

    public static String getSectionName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_SECTION_NAME, "");
    }

    public static void saveSectionName(Context context, String sectionName) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefsEditor.putString(KEY_SECTION_NAME, sectionName);
        prefsEditor.commit();
    }

    public static String getSubsectionName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_SUBSECTION_NAME, "");
    }

    public static void saveSubsectionName(Context context, String subsectionName) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefsEditor.putString(KEY_SUBSECTION_NAME, subsectionName);
        prefsEditor.commit();
    }

    /**
     * Loads section from preferences.
     *
     * @param context Context
     * @return
     */
    public static Section loadSection(Context context) {
        String sectionString = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_SECTION_OBJECT, null);
        if (sectionString == null) return null;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(sectionString, new TypeToken<Section>() {
        }.getType());
    }

    /**
     * Saves section to preferences.
     *
     * @param context Context
     */
    public static void saveSection(Context context, Section section) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(section);

        prefsEditor.putString(KEY_SECTION_OBJECT, json);
        prefsEditor.commit();
    }

    /**
     * Loads schedules from preferences.
     *
     * @param context Context
     * @return
     */
    public static Timetable loadTimetableAll(Context context) {
        String timetableString = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_TIMETABLE_ALL_OBJECT, null);
        if (timetableString == null) return null;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(timetableString, new TypeToken<Timetable>() {
        }.getType());
    }

    /**
     * Saves schedules to preferences.
     *
     * @param context Context
     */
    public static void saveTimetableAll(Context context, Timetable timetable) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(timetable);

        prefsEditor.putString(KEY_TIMETABLE_ALL_OBJECT, json);
        prefsEditor.commit();
    }

    /**
     * Loads schedules from preferences.
     *
     * @param context Context
     * @return
     */
    public static Timetable loadTimetableSelected(Context context) {
        String timetableString = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_TIMETABLE_SELECTED_OBJECT, null);
        if (timetableString == null) return null;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(timetableString, new TypeToken<Timetable>() {
        }.getType());
    }

    /**
     * Saves schedules to preferences.
     *
     * @param context Context
     */
    public static void saveTimetableSelected(Context context, Timetable timetable) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(timetable);

        prefsEditor.putString(KEY_TIMETABLE_SELECTED_OBJECT, json);
        prefsEditor.commit();
    }

    /**
     * Loads schedules from preferences.
     *
     * @param context Context
     * @return
     */
    public static ArrayList<Term> loadMidtermSchedules(Context context) {
        String schedulesString = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_MIDTERM_SCHEDULES_OBJECT, null);
        if (schedulesString == null) return null;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(schedulesString, new TypeToken<ArrayList<Term>>() {
        }.getType());
    }

    /**
     * Saves schedules to preferences.
     *
     * @param context Context
     */
    public static void saveMidtermSchedules(Context context, ArrayList<Term> schedules) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(schedules);

        prefsEditor.putString(KEY_MIDTERM_SCHEDULES_OBJECT, json);
        prefsEditor.commit();
    }

    /**
     * Loads schedules from preferences.
     *
     * @param context Context
     * @return
     */
    public static ArrayList<Term> loadExamsSchedules(Context context) {
        String schedulesString = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_EXAMS_SCHEDULES_OBJECT, null);
        if (schedulesString == null) return null;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(schedulesString, new TypeToken<ArrayList<Term>>() {
        }.getType());
    }

    /**
     * Saves schedules to preferences.
     *
     * @param context Context
     */
    public static void saveExamsSchedules(Context context, ArrayList<Term> schedules) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(schedules);

        prefsEditor.putString(KEY_EXAMS_SCHEDULES_OBJECT, json);
        prefsEditor.commit();
    }

    /**
     * Loads schedules from preferences.
     *
     * @param context Context
     * @return
     */
    public static CalendarYear loadCalendarYear(Context context) {
        String calendarString = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_CALENDAR_YEAR_OBJECT, null);
        if (calendarString == null) return null;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(calendarString, new TypeToken<CalendarYear>() {
        }.getType());
    }

    /**
     * Saves schedules to preferences.
     *
     * @param context Context
     */
    public static void saveCalendarYear(Context context, CalendarYear calendarYear) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(calendarYear);

        prefsEditor.putString(KEY_CALENDAR_YEAR_OBJECT, json);
        prefsEditor.commit();
    }
}
