package rs.ac.bg.student.utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import rs.ac.bg.student.R;
import rs.ac.bg.student.model.AcademicYear;
import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Date;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.Interval;
import rs.ac.bg.student.model.ScheduleExam;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.WorkingWeekendDay;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by mcekic on 3/22/18.
 */

public class AppUtils {
    public static final String TAG = "AppUtils";

    /**
     * @param context Context
     * @return
     */
    public static Interval getSemesterTimetableInterval(Context context) {
        Timetable timetable = Preferences.loadTimetableAll(context);
        if (timetable == null) return null;
        return timetable.interval;
    }

    /**
     * @param context Context
     * @return
     */
    public static ArrayList<String> getSelectedSubjects(Context context) {
        ArrayList<String> selectedSubjects = new ArrayList<>();
        Section section = Preferences.loadSection(context);
        if (section == null || section.academicYears == null) return selectedSubjects;

        for (AcademicYear academicYear : section.academicYears) {
            if (academicYear == null) continue;
            if (academicYear.subjects == null) continue;

            for (Subject subject : academicYear.subjects) {
                if (subject.selected) {
                    if (subject.subjectName.contains(context.getResources().getString(R.string.or))
                            || subject.subjectName.contains(context.getString(R.string.elective_subject))
                            || subject.subjectName.contains(context.getString(R.string.elective_block))) {
                        selectedSubjects.add(subject.selectedSubject);
                    } else {
                        selectedSubjects.add(subject.subjectName);
                    }

                }
            }
        }
        return selectedSubjects;
    }

    /**
     * @param context Context
     * @return
     */
    public static ArrayList<ScheduleClass> getSelectedSchedulesByDayOfTheWeek(Context context, String day) {
        Timetable timetable = Preferences.loadTimetableSelected(context);
        if (timetable == null || timetable.schedules == null) return null;

        ArrayList<ScheduleClass> selectedSchedules = timetable.schedules;
        ArrayList<ScheduleClass> schedules = new ArrayList<>();
        for (ScheduleClass scheduleClass : selectedSchedules) {
            if (scheduleClass.dayOfWeek.equals(day)) {
                schedules.add(scheduleClass);
            }
        }

        return schedules;
    }

    /**
     * @return
     */
    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return Date.getDate(calendar);
    }

    /**
     * @param context Context
     * @param terms   ArrayList<Term>
     * @return
     */
    private static ArrayList<ScheduleExam> getSelectedSchedules(Context context, ArrayList<Term> terms) {
        ArrayList<String> selectedSubjects = AppUtils.getSelectedSubjects(context);
        ArrayList<ScheduleExam> selected = new ArrayList<>();

        if (selectedSubjects.size() == 0 || terms == null) return selected;

        for (Term term : terms) {
            if (term.schedules == null) continue;

            for (ScheduleExam schedule : term.schedules) {
                for (String subjectName : selectedSubjects) {
                    if (AppUtils.areEqualSubjects(schedule.subjectName, subjectName)) {
                        selected.add(schedule);
                    }
                }
            }

        }

        // sort by date
        Collections.sort(selected, new Comparator<ScheduleExam>() {

            @Override
            public int compare(final ScheduleExam schedule1, final ScheduleExam schedule2) {
                return (schedule1.date.year + getString(schedule1.date.month) + getString(schedule1.date.day) + schedule1.time)
                        .compareTo(schedule2.date.year + getString(schedule2.date.month) + getString(schedule2.date.day) + schedule2.time);
            }

        });
        return selected;
    }

    /**
     * Adds 0 in month/day for correct sorting.
     * Examples: 1 returns 01;
     *
     * @param number String
     * @return
     */
    private static String getString(int number) {
        return Integer.toString(number / 10) + Integer.toString(number % 10);
    }

    /**
     * @param context Context
     * @return
     */
    public static ArrayList<ArrayList<ScheduleExam>> getMySchedulesByDate(Context context) {
        ArrayList<ArrayList<ScheduleExam>> mySchedulesByDate = new ArrayList<>();

        ArrayList<ScheduleExam> sortedMidtermSchedules = AppUtils.getSelectedSchedules(context, Preferences.loadMidtermSchedules(context));
        for (int i = 0; i < sortedMidtermSchedules.size(); i++) {
            ArrayList<ScheduleExam> selectedForDate = new ArrayList<>();
            ScheduleExam first = sortedMidtermSchedules.get(i);
            selectedForDate.add(first);
            for (int j = i + 1; j < sortedMidtermSchedules.size(); j++) {
                ScheduleExam next = sortedMidtermSchedules.get(j);
                if (first.date.equals(next.date)) {
                    selectedForDate.add(next);
                } else {
                    i = j - 1;
                    break;
                }
            }
            mySchedulesByDate.add(selectedForDate);
        }
        ArrayList<ScheduleExam> sortedExamsSchedules = AppUtils.getSelectedSchedules(context, Preferences.loadExamsSchedules(context));
        for (int i = 0; i < sortedExamsSchedules.size(); i++) {
            ArrayList<ScheduleExam> selectedForDate = new ArrayList<>();
            ScheduleExam first = sortedExamsSchedules.get(i);
            selectedForDate.add(first);
            for (int j = i + 1; j < sortedExamsSchedules.size(); j++) {
                ScheduleExam next = sortedExamsSchedules.get(j);
                if (first.date.equals(next.date)) {
                    selectedForDate.add(next);
                } else {
                    i = j - 1;
                    break;
                }
            }
            mySchedulesByDate.add(selectedForDate);
        }

        return mySchedulesByDate;
    }

    /**
     * description example:  *Субота	* Настава за Уторак
     *
     * @param context Context
     * @param date    Date
     * @return
     */
    public static String getWorkingDay(Context context, Date date) {
        CalendarYear calendarYear = Preferences.loadCalendarYear(context);
        for (WorkingWeekendDay workingWeekendDay : calendarYear.workingWeekendDays) {
            if (date.equals(workingWeekendDay.date)) {
                return workingWeekendDay.day;
            }
        }
        return null;
    }

    /**
     * @param subjectName1 String
     * @param subjectName2 String
     * @return
     */
    public static boolean areEqualSubjects(String subjectName1, String subjectName2) {

        if (subjectName1.equals(subjectName2)) return true;

        // Изборни - Мобилно рачунарство / Мобилно рачунарство
        if (subjectName1.contains("Изборни - ")) {
            subjectName1 = subjectName1.substring("Изборни - ".length());
        }
        if (subjectName2.contains("Изборни - ")) {
            subjectName2 = subjectName2.substring("Изборни - ".length());
        }

        if (subjectName1.equals(subjectName2)) return true;

        // Пројектовање софтвера (за студенте који слушају C#)
        if (subjectName1.contains(subjectName2 + " (")) return true;
        if (subjectName2.contains(subjectName1 + " (")) return true;

        // Архитектура рачунара и опер. системи / Архитектура рачунара и оперативни системи
        if ("Архитектура рачунара и опер. системи".equals(subjectName1)) {
            subjectName1 = "Архитектура рачунара и оперативни системи";
        }
        if ("Архитектура рачунара и опер. системи".equals(subjectName2)) {
            subjectName2 = "Архитектура рачунара и оперативни системи";
        }

        // Физичко пројектовање ИС у изабраном софт. окруж. / Физички пројекат ИС у изабраном софтверском окружењу (пројекат)
        if ("Физичко пројектовање ИС у изабраном софт. окруж.".equals(subjectName1)) {
            subjectName1 = "Физички пројекат ИС у изабраном софтверском окружењу (пројекат)";
        }
        if ("Физичко пројектовање ИС у изабраном софт. окруж.".equals(subjectName2)) {
            subjectName2 = "Физички пројекат ИС у изабраном софтверском окружењу (пројекат)";
        }

        if (subjectName1.equals(subjectName2)) return true;

        return false;
    }
}
