package rs.ac.bg.student.utils;

import java.util.Calendar;

import rs.ac.bg.student.model.ScheduleClass;

/**
 * Created by mcekic on 3/25/18.
 */

public class Constants {
    /**
     *
     */
    private final static int MONDAY = 1;
    private final static int TUESDAY = 2;
    private final static int WEDNESDAY = 3;
    private final static int THURSDAY = 4;
    private final static int FRIDAY = 5;
    private final static int SATURDAY = 6;
    private final static int SUNDAY = 7;

    /**
     * @param dayOfWeek String
     * @return
     */
    public static int getDayOfTheWeekInt(String dayOfWeek) {

        if (dayOfWeek.equals(ScheduleClass.DAY_MONDAY)) {
            return MONDAY;
        } else if (dayOfWeek.equals(ScheduleClass.DAY_TUESDAY)) {
            return TUESDAY;
        } else if (dayOfWeek.equals(ScheduleClass.DAY_WEDNESDAY)) {
            return WEDNESDAY;
        } else if (dayOfWeek.equals(ScheduleClass.DAY_THURSDAY)) {
            return THURSDAY;
        } else if (dayOfWeek.equals(ScheduleClass.DAY_FRIDAY)) {
            return FRIDAY;
        } else if (dayOfWeek.equals(ScheduleClass.DAY_SATURDAY)) {
            return SATURDAY;
        } else if (dayOfWeek.equals(ScheduleClass.DAY_SUNDAY)) {
            return SUNDAY;
        }

        return 0;
    }

    /**
     * @param dayOfWeek String
     * @return
     */
    public static String getDayOfTheWeekString(int dayOfWeek) {
        if (dayOfWeek == Calendar.MONDAY) {
            return ScheduleClass.DAY_MONDAY;
        } else if (dayOfWeek == Calendar.TUESDAY) {
            return ScheduleClass.DAY_TUESDAY;
        } else if (dayOfWeek == Calendar.WEDNESDAY) {
            return ScheduleClass.DAY_WEDNESDAY;
        } else if (dayOfWeek == Calendar.THURSDAY) {
            return ScheduleClass.DAY_THURSDAY;
        } else if (dayOfWeek == Calendar.FRIDAY) {
            return ScheduleClass.DAY_FRIDAY;
        } else if (dayOfWeek == Calendar.SATURDAY) {
            return ScheduleClass.DAY_SATURDAY;
        } else if (dayOfWeek == Calendar.SUNDAY) {
            return ScheduleClass.DAY_SUNDAY;
        }
        return "";
    }
}
