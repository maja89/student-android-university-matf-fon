package rs.ac.bg.student.utils;

import android.util.Log;

import rs.ac.bg.student.StudentApplication;

/**
 * Created by mcekic on 5/10/18.
 */

public class Logger {

    public static final boolean DEBUG = StudentApplication.isDebug();

    public static void debugRequest(String tag, String message) {
        if (DEBUG) {
            Log.d(tag, "Request:\n" + message);
        }
    }

    public static void debugRequest(String tag, String message, String json) {
        if (DEBUG) {
            Log.d(tag, "Request:\n" + message + "\nJSON: " + json);
        }
    }

    public static void debugResponse(String tag, String message) {
        if (DEBUG) {
            Log.d(tag, "ResponseObject = \n" + message);
        }
    }

    public static void debug(String tag, String message) {
        if (DEBUG) {
            Log.d(tag, message);
        }
    }

    public static void debug(String tag, String status, int errorCode, String message) {
        if (DEBUG) {
            Log.d(tag, "status: " + status);
            Log.d(tag, "error code: " + errorCode);
            Log.d(tag, "message: " + message);
        }
    }
}
