package rs.ac.bg.student.utils;

import android.content.Context;

import java.util.Calendar;

import rs.ac.bg.student.BuildConfig;
import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentApplication;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by maja on 4/22/18.
 */

public class Resources {

    /**
     * @param context   Context
     * @param dayOfWeek String
     * @return
     */
    public static String getDayOfTheWeekResource(Context context, int dayOfWeek) {
        if (dayOfWeek == Calendar.MONDAY) {
            return context.getResources().getString(R.string.day_monday);
        } else if (dayOfWeek == Calendar.TUESDAY) {
            return context.getResources().getString(R.string.day_tuesday);
        } else if (dayOfWeek == Calendar.WEDNESDAY) {
            return context.getResources().getString(R.string.day_wenesday);
        } else if (dayOfWeek == Calendar.THURSDAY) {
            return context.getResources().getString(R.string.day_thursday);
        } else if (dayOfWeek == Calendar.FRIDAY) {
            return context.getResources().getString(R.string.day_friday);
        } else if (dayOfWeek == Calendar.SATURDAY) {
            return context.getResources().getString(R.string.day_saturday);
        } else if (dayOfWeek == Calendar.SUNDAY) {
            return context.getResources().getString(R.string.day_sunday);
        }
        return "";
    }

    /**
     * @param context   Context
     * @param dayOfWeek String
     * @return
     */
    public static String getDayOfTheWeekResource(Context context, String dayOfWeek) {
        if (dayOfWeek.equals(ScheduleClass.DAY_MONDAY)) {
            return context.getResources().getString(R.string.day_monday);
        } else if (dayOfWeek.equals(ScheduleClass.DAY_TUESDAY)) {
            return context.getResources().getString(R.string.day_tuesday);
        } else if (dayOfWeek.equals(ScheduleClass.DAY_WEDNESDAY)) {
            return context.getResources().getString(R.string.day_wenesday);
        } else if (dayOfWeek.equals(ScheduleClass.DAY_THURSDAY)) {
            return context.getResources().getString(R.string.day_thursday);
        } else if (dayOfWeek.equals(ScheduleClass.DAY_FRIDAY)) {
            return context.getResources().getString(R.string.day_friday);
        } else if (dayOfWeek.equals(ScheduleClass.DAY_SATURDAY)) {
            return context.getResources().getString(R.string.day_saturday);
        } else if (dayOfWeek.equals(ScheduleClass.DAY_SUNDAY)) {
            return context.getResources().getString(R.string.day_sunday);
        }
        return "";
    }

    /**
     * Converts int month to String month.
     *
     * @param context Context
     * @param month   month
     * @return
     */
    public static String getMonthResource(Context context, int month) {
        switch (month) {
            case Calendar.JANUARY:
                return context.getResources().getString(R.string.month_january);
            case Calendar.FEBRUARY:
                return context.getResources().getString(R.string.month_february);
            case Calendar.MARCH:
                return context.getResources().getString(R.string.month_march);
            case Calendar.APRIL:
                return context.getResources().getString(R.string.month_april);
            case Calendar.MAY:
                return context.getResources().getString(R.string.month_may);
            case Calendar.JUNE:
                return context.getResources().getString(R.string.month_june);
            case Calendar.JULY:
                return context.getResources().getString(R.string.month_july);
            case Calendar.AUGUST:
                return context.getResources().getString(R.string.month_august);
            case Calendar.SEPTEMBER:
                return context.getResources().getString(R.string.month_september);
            case Calendar.OCTOBER:
                return context.getResources().getString(R.string.month_october);
            case Calendar.NOVEMBER:
                return context.getResources().getString(R.string.month_november);
            case Calendar.DECEMBER:
                return context.getResources().getString(R.string.month_december);
        }
        return "";
    }

    /**
     * @param context Context
     * @return
     */
    public static String getSectionNameResource(Context context) {
        String sectionName = Preferences.getSectionName(context);
        String subsectionName = Preferences.getSubsectionName(context);

        if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_FON)) {
            if (Section.SECTION_1_ISIT.equals(sectionName)) {
                return context.getResources().getString(R.string.section_1);
            } else if (Section.SECTION_2_M.equals(sectionName)) {
                return context.getResources().getString(R.string.section_2);
            } else if (Section.SECTION_3_MKIS.equals(sectionName)) {
                return context.getResources().getString(R.string.section_3);
            } else if (Section.SECTION_4_OM.equals(sectionName)) {
                return context.getResources().getString(R.string.section_4);
            }
        } else if (BuildConfig.FLAVOR.equals(StudentApplication.FLAVOR_MATF)) {
            if (Section.SECTION_1_MATEMATIKA.equals(sectionName)) {

                switch (subsectionName) {
                    case Section.SUBSECTION_11_ML:
                        return context.getResources().getString(R.string.subsection_m1);
                    case Section.SUBSECTION_12_MM:
                        return context.getResources().getString(R.string.subsection_m2);
                    case Section.SUBSECTION_13_MR:
                        return context.getResources().getString(R.string.subsection_m3);
                    case Section.SUBSECTION_14_MP:
                        return context.getResources().getString(R.string.subsection_m4);
                    case Section.SUBSECTION_15_MS:
                        return context.getResources().getString(R.string.subsection_m5);
                    case Section.SUBSECTION_16_MA:
                        return context.getResources().getString(R.string.subsection_m6);
                }

                return context.getResources().getString(R.string.section_1);
            } else if (Section.SECTION_2_INFORMATIKA.equals(sectionName)) {
                return context.getResources().getString(R.string.section_2);
            } else if (Section.SECTION_3_AIA.equals(sectionName)) {

                switch (subsectionName) {
                    case Section.SUBSECTION_31_AF:
                        return context.getResources().getString(R.string.subsection_a1);
                    case Section.SUBSECTION_32_AI:
                        return context.getResources().getString(R.string.subsection_a2);
                }

                return context.getResources().getString(R.string.section_3);
            }
        }
        return "";
    }
}
