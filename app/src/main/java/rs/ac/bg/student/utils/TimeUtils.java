package rs.ac.bg.student.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

    private static final String PATTERN_DATE = "d/M/yyyy";
    private static final String PATTERN_HOUR = "HH:mm";
    private static final String PATTERN_TIME = "HH:mm d/M/yyyy";

    public static String getTimeString(long time) {
        long currentTime = System.currentTimeMillis();

        Date currentDate = new Date(currentTime);
        Date date = new Date(time);

        DateFormat formatter = new SimpleDateFormat(PATTERN_DATE);
        String dateFormatted = formatter.format(date);
        String currentDateFormatted = formatter.format(currentDate);

        if (!currentDateFormatted.equals(dateFormatted)) {
            return dateFormatted;
        }

        DateFormat formatterHour = new SimpleDateFormat(PATTERN_HOUR);
        return formatterHour.format(date);
    }

    public static String getTimeDetailString(long time) {
        Date date = new Date(time);
        DateFormat formatterHour = new SimpleDateFormat(PATTERN_TIME);
        return formatterHour.format(date);
    }
}
