package rs.ac.bg.student.utils;

import android.content.Context;

import java.util.ArrayList;

import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by maja on 5/15/18.
 */

public class Utils {

    public static void loadAssets(Context context) {
        //TODO: loadAssets
    }

    public static CalendarYear loadCalendarYear(Context context) {
        return AssetLoader.loadCalendarYear(context);
    }

    public static Timetable loadTimetable(Context context) {
        String sectionName = Preferences.getSectionName(context);
        return AssetLoader.loadTimetableAll(context, sectionName);
    }

    public static Section loadSection(Context context) {
        String sectionName = Preferences.getSectionName(context);
        String subsection = Preferences.getSubsectionName(context);
        return AssetLoader.loadSection(context, sectionName, subsection);
    }

    public static ArrayList<Term> loadMidterm(Context context) {
        return AssetLoader.loadMidtermSchedules(context);
    }

    public static ArrayList<Term> loadExams(Context context) {
        return AssetLoader.loadExamsSchedules(context);
    }
}
