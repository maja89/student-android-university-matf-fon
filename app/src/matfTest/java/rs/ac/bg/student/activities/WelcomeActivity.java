package rs.ac.bg.student.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;


import rs.ac.bg.student.R;
import rs.ac.bg.student.StudentActivity;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.preferences.Preferences;

/**
 * Created by maja on 4/21/18.
 */

public class WelcomeActivity extends BaseActivity {
    public static final String TAG = "WelcomeActivity";

    public static final String ACTION_SHOW_SECTION = "ACTION_SHOW_SECTION";

    /**
     * Screen delay
     */
    private static final int DELAY = 1000; // 1 second

    private LinearLayout mSectionLayout, mSubsectionMLayout, mSubsectionALayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mSectionLayout = (LinearLayout) findViewById(R.id.layoutSection);
        mSubsectionMLayout = (LinearLayout) findViewById(R.id.layoutSubsectionM);
        mSubsectionALayout = (LinearLayout) findViewById(R.id.layoutSubsectionA);

        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                if (ACTION_SHOW_SECTION.equals(action)) {
                    mSectionLayout.setVisibility(View.VISIBLE);
                    return;
                }
            }
        }

        String sectionName = Preferences.getSectionName(getBaseContext());
        if (sectionName.isEmpty()) {
            mSectionLayout.setVisibility(View.VISIBLE);
        } else {
            showStudent();
        }
    }

    public void onSelectedSection1(View view) {
        mSectionLayout.setVisibility(View.GONE);
        mSubsectionMLayout.setVisibility(View.VISIBLE);
    }

    public void onSelectedSection2(View view) {
        saveSection(Section.SECTION_2_INFORMATIKA);
    }

    public void onSelectedSection3(View view) {
        mSectionLayout.setVisibility(View.GONE);
        mSubsectionALayout.setVisibility(View.VISIBLE);
    }

    public void onSelectedSubsectionA1(View view) {
        saveSectionAndSubsection(Section.SECTION_3_AIA, Section.SUBSECTION_31_AF);
    }

    public void onSelectedSubsectionA2(View view) {
        saveSectionAndSubsection(Section.SECTION_3_AIA, Section.SUBSECTION_32_AI);
    }

    public void onSelectedSubsectionM1(View view) {
        saveSectionAndSubsection(Section.SECTION_1_MATEMATIKA, Section.SUBSECTION_11_ML);
    }

    public void onSelectedSubsectionM2(View view) {
        saveSectionAndSubsection(Section.SECTION_1_MATEMATIKA, Section.SUBSECTION_12_MM);
    }

    public void onSelectedSubsectionM3(View view) {
        saveSectionAndSubsection(Section.SECTION_1_MATEMATIKA, Section.SUBSECTION_13_MR);
    }

    public void onSelectedSubsectionM4(View view) {
        saveSectionAndSubsection(Section.SECTION_1_MATEMATIKA, Section.SUBSECTION_14_MP);
    }

    public void onSelectedSubsectionM5(View view) {
        saveSectionAndSubsection(Section.SECTION_1_MATEMATIKA, Section.SUBSECTION_15_MS);
    }

    public void onSelectedSubsectionM6(View view) {
        saveSectionAndSubsection(Section.SECTION_1_MATEMATIKA, Section.SUBSECTION_16_MA);
    }

    private void saveSection(String sectionName) {
        saveSectionAndSubsection(sectionName, "");
    }

    private void saveSectionAndSubsection(String sectionName, String subsectionName) {
        Preferences.saveSectionName(getBaseContext(), sectionName);
        if (!subsectionName.isEmpty()) {
            Preferences.saveSubsectionName(getBaseContext(), subsectionName);
        }
        mSectionLayout.setVisibility(View.GONE);
        showStudent();
    }

    private void showStudent() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getBaseContext(), StudentActivity.class);
                startActivity(intent);

                finish();
            }
        }, DELAY);
    }

}
