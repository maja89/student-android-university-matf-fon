package rs.ac.bg.student.utils;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rs.ac.bg.student.R;
import rs.ac.bg.student.model.AcademicYear;
import rs.ac.bg.student.model.CalendarYear;
import rs.ac.bg.student.model.Date;
import rs.ac.bg.student.model.ElectiveSubjects;
import rs.ac.bg.student.model.ScheduleClass;
import rs.ac.bg.student.model.Term;
import rs.ac.bg.student.model.ScheduleExam;
import rs.ac.bg.student.model.Timetable;
import rs.ac.bg.student.model.Subject;
import rs.ac.bg.student.model.Interval;
import rs.ac.bg.student.model.Section;
import rs.ac.bg.student.model.WorkingWeekendDay;

/**
 * Created by mcekic on 3/21/18.
 */

public class AssetLoader {
    public static final String TAG = "AssetLoader";

    private static boolean DEBUG = false;

    private static final String CALENDAR = "data/kalendar/kalendar.txt";

    private static final String PATTERN_DATE = "d.M.yyyy.";

    /**
     * Matf
     */
    private static final String TIME_8 = "08:00";
    private static final String TIME_9 = "09:00";
    private static final String TIME_10 = "10:00";
    private static final String TIME_11 = "11:00";
    private static final String TIME_12 = "12:00";
    private static final String TIME_13 = "13:00";
    private static final String TIME_14 = "14:00";
    private static final String TIME_15 = "15:00";
    private static final String TIME_16 = "16:00";
    private static final String TIME_17 = "17:00";
    private static final String TIME_18 = "18:00";
    private static final String TIME_19 = "19:00";
    private static final String TIME_20 = "20:00";
    private static final String TIME_21 = "21:00";
    private static final String TIME_22 = "22:00";

    private static final String SUBJECTS_MATEMATIKA_ML = "data/predmeti/predmeti_matematika_ml.txt";
    private static final String SUBJECTS_MATEMATIKA_MM = "data/predmeti/predmeti_matematika_mm.txt";
    private static final String SUBJECTS_MATEMATIKA_MR = "data/predmeti/predmeti_matematika_mr.txt";
    private static final String SUBJECTS_MATEMATIKA_MP = "data/predmeti/predmeti_matematika_mp.txt";
    private static final String SUBJECTS_MATEMATIKA_MS = "data/predmeti/predmeti_matematika_ms.txt";
    private static final String SUBJECTS_MATEMATIKA_MA = "data/predmeti/predmeti_matematika_ma.txt";

    private static final String SUBJECTS_INFORMATIKA = "data/predmeti/predmeti_informatika.txt";

    private static final String SUBJECTS_MATEMATIKA_AF = "data/predmeti/predmeti_matematika_af.txt";
    private static final String SUBJECTS_MATEMATIKA_AI = "data/predmeti/predmeti_matematika_ai.txt";

    /**
     * First year Matematika
     */
    private static final String SCHEDULE_1O1A = "data/raspored/matematika/raspored_1O1A_zim_2019.txt";
    private static final String SCHEDULE_1O1B = "data/raspored/matematika/raspored_1O1B_zim_2019.txt";
    private static final String SCHEDULE_1O2A = "data/raspored/matematika/raspored_1O2A_zim_2019.txt";
    private static final String SCHEDULE_1O2B = "data/raspored/matematika/raspored_1O2B_zim_2019.txt";
    private static final String SCHEDULE_1O3A = "data/raspored/matematika/raspored_1O3A_zim_2019.txt";
    private static final String SCHEDULE_1O3B = "data/raspored/matematika/raspored_1O3B_zim_2019.txt";
    private static final String SCHEDULE_1O4A = "data/raspored/matematika/raspored_1O4A_zim_2019.txt";
    private static final String SCHEDULE_1O4B = "data/raspored/matematika/raspored_1O4B_zim_2019.txt";

    /**
     * Second year Matematika
     */
    private static final String SCHEDULE_2MNVA = "data/raspored/matematika/raspored_2MNVA_zim_2019.txt";
    private static final String SCHEDULE_2MNVB = "data/raspored/matematika/raspored_2MNVB_zim_2019.txt";
    private static final String SCHEDULE_2LR1A = "data/raspored/matematika/raspored_2LR1A_zim_2019.txt";
    private static final String SCHEDULE_2LR1B = "data/raspored/matematika/raspored_2LR1B_zim_2019.txt";
    private static final String SCHEDULE_2LR2A = "data/raspored/matematika/raspored_2LR2A_zim_2019.txt";
    private static final String SCHEDULE_2LR2B = "data/raspored/matematika/raspored_2LR2B_zim_2019.txt";

    /**
     * Third year Matematika
     */
    private static final String SCHEDULE_3L = "data/raspored/matematika/raspored_3L_zim_2019.txt";
    private static final String SCHEDULE_3M = "data/raspored/matematika/raspored_3M_zim_2019.txt";
    private static final String SCHEDULE_3N = "data/raspored/matematika/raspored_3N_zim_2019.txt";
    private static final String SCHEDULE_3R = "data/raspored/matematika/raspored_3R_zim_2019.txt";
    private static final String SCHEDULE_3V = "data/raspored/matematika/raspored_3V_zim_2019.txt";

    /**
     * Fourth year Matematika
     */
    private static final String SCHEDULE_4L = "data/raspored/matematika/raspored_4L_zim_2019.txt";
    private static final String SCHEDULE_4M = "data/raspored/matematika/raspored_4M_zim_2019.txt";
    private static final String SCHEDULE_4N = "data/raspored/matematika/raspored_4N_zim_2019.txt";
    private static final String SCHEDULE_4R = "data/raspored/matematika/raspored_4R_zim_2019.txt";
    private static final String SCHEDULE_4V = "data/raspored/matematika/raspored_4V_zim_2019.txt";

    /**
     * First year Informatika
     */
    private static final String SCHEDULE_1I1A = "data/raspored/informatika/raspored_1I1A_zim_2019.txt";
    private static final String SCHEDULE_1I1B = "data/raspored/informatika/raspored_1I1B_zim_2019.txt";
    private static final String SCHEDULE_1I2A = "data/raspored/informatika/raspored_1I2A_zim_2019.txt";
    private static final String SCHEDULE_1I2B = "data/raspored/informatika/raspored_1I2B_zim_2019.txt";
    private static final String SCHEDULE_1I3A = "data/raspored/informatika/raspored_1I3A_zim_2019.txt";
    private static final String SCHEDULE_1I3B = "data/raspored/informatika/raspored_1I3B_zim_2019.txt";

    /**
     * Second year Informatika
     */
    private static final String SCHEDULE_2I1A = "data/raspored/informatika/raspored_2I1A_zim_2019.txt";
    private static final String SCHEDULE_2I1B = "data/raspored/informatika/raspored_2I1B_zim_2019.txt";
    private static final String SCHEDULE_2I2A = "data/raspored/informatika/raspored_2I2A_zim_2019.txt";
    private static final String SCHEDULE_2I2B = "data/raspored/informatika/raspored_2I2B_zim_2019.txt";

    /**
     * Third year Informatika
     */
    private static final String SCHEDULE_3I1 = "data/raspored/informatika/raspored_3I1_zim_2019.txt";
    private static final String SCHEDULE_3I2 = "data/raspored/informatika/raspored_3I2_zim_2019.txt";

    /**
     * Fourth year Informatika
     */
    private static final String SCHEDULE_4I = "data/raspored/informatika/raspored_4I_zim_2019.txt";

    /**
     * First year Matematika
     */
    private static final String GROUP_1O1A = "1О1А";
    private static final String GROUP_1O1B = "1О1Б";
    private static final String GROUP_1O2A = "1О2А";
    private static final String GROUP_1O2B = "1О2Б";
    private static final String GROUP_1O3A = "1О3А";
    private static final String GROUP_1O3B = "1О3Б";
    private static final String GROUP_1O4A = "1О4А";
    private static final String GROUP_1O4B = "1О4Б";

    /**
     * Second year Matematika
     */
    private static final String GROUP_2MNVA = "2МНВА";
    private static final String GROUP_2MNVB = "2МНВБ";
    private static final String GROUP_2LR1A = "2ЛР1А";
    private static final String GROUP_2LR1B = "2ЛР1Б";
    private static final String GROUP_2LR2A = "2ЛР2А";
    private static final String GROUP_2LR2B = "2ЛР2Б";

    /**
     * Third year Matematika
     */
    private static final String GROUP_3L = "3Л";
    private static final String GROUP_3M = "3М";
    private static final String GROUP_3N = "3Н";
    private static final String GROUP_3R = "3Р";
    private static final String GROUP_3V = "3В";

    /**
     * Fourth year Matematika
     */
    private static final String GROUP_4L = "4Л";
    private static final String GROUP_4M = "4М";
    private static final String GROUP_4N = "4Н";
    private static final String GROUP_4R = "4Р";
    private static final String GROUP_4V = "4В";

    /**
     * First year Informatika
     */
    private static final String GROUP_1I1A = "1И1А";
    private static final String GROUP_1I1B = "1И1Б";
    private static final String GROUP_1I2A = "1И2А";
    private static final String GROUP_1I2B = "1И2Б";
    private static final String GROUP_1I3A = "1И3А";
    private static final String GROUP_1I3B = "1И3Б";

    /**
     * Second year Informatika
     */
    private static final String GROUP_2I1A = "2И1А";
    private static final String GROUP_2I1B = "2И1Б";
    private static final String GROUP_2I2A = "2И2А";
    private static final String GROUP_2I2B = "2И2Б";

    /**
     * Third year Informatika
     */
    private static final String GROUP_3I1 = "3И1";
    private static final String GROUP_3I2 = "3И2";

    /**
     * Fourth year Informatika
     */
    private static final String GROUP_4I = "4И";

    private static final String EXAMS_JAN_1 = "data/ispiti/2019_2020/januar_1.txt";
    private static final String EXAMS_JAN_2 = "data/ispiti/2019_2020/januar_2.txt";
    private static final String EXAMS_JUN_1 = "data/ispiti/2019_2020/jun_1.txt";
    private static final String EXAMS_JUN_2 = "data/ispiti/2019_2020/jun_2.txt";
    private static final String EXAMS_SEP_1 = "data/ispiti/2019_2020/septembar_1.txt";
    private static final String EXAMS_SEP_2 = "data/ispiti/2019_2020/septembar_2.txt";

    private static final String MIDTERM_FIRST = "data/kolokvijumi/2019_2020/kolokvijumi_zimski.txt";
    private static final String MIDTERM_SECOND = "data/kolokvijumi/2019_2020/kolokvijumi_letnji.txt";

    protected static CalendarYear loadCalendarYear(Context context) {
        CalendarYear calendarYear = new CalendarYear();
        String contentLoaded = loadContentFromAsset(context, CALENDAR);

        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE);

        for (String line : lines) {
            if (DEBUG)
                Log.d(TAG, "line: " + line);

            java.util.Date dateStart = null;
            java.util.Date dateEnd = null;

            try {
                if (containsWeekdays(context, line) || line.contains(context.getResources().getString(R.string.midterm))) {
                    if (line.contains("-")) {
                        // start date
                        String startInput = line.substring(0, line.indexOf('-')) + line.substring(line.indexOf("\t") - 5, line.indexOf("\t"));
                        if (DEBUG)
                            Log.d(TAG, "startInput: " + startInput);
                        dateStart = parser.parse(startInput);

                        // end date
                        String endInput = line.substring(line.indexOf('-') + 1, line.indexOf("\t"));
                        if (DEBUG)
                            Log.d(TAG, "endInput: " + endInput);
                        dateEnd = parser.parse(endInput);
                    } else {
                        // date
                        String input = line.substring(0, line.indexOf("\t"));
                        if (DEBUG)
                            Log.d(TAG, "input: " + input);
                        dateStart = parser.parse(input);
                        dateEnd = dateStart;
                    }
                } else if (line.contains(context.getResources().getString(R.string.examination_period))) {
                    // start date
                    int start = line.indexOf(": ");
                    String startInput = line.substring(start + 2, start + 13);
                    if (DEBUG)
                        Log.d(TAG, "startInput: " + startInput);
                    dateStart = parser.parse(startInput);

                    // end date
                    start = line.indexOf("до ");
                    String endInput = line.substring(start + 3, start + 14);
                    if (DEBUG)
                        Log.d(TAG, "endInput: " + endInput);
                    dateEnd = parser.parse(endInput);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (dateStart == null || dateEnd == null) continue;

            Date date1 = Date.getDate(dateStart);
            Date date2 = Date.getDate(dateEnd);

            Interval interval = new Interval(date1, date2);
            if (containsWeekdays(context, line)) {
                if (DEBUG)
                    Log.d(TAG, "workingWeeks, interval added: " + interval.toString());

                if (line.contains("*")) {
                    String description = CyrillicLatinConverter.latinToCyrillic(line.substring(line.indexOf('*')));
                    WorkingWeekendDay workingWeekendDay = new WorkingWeekendDay(interval.endDate, getWorkingDay(context, description));
                    if (DEBUG)
                        Log.d(TAG, "workingWeekendDay added: " + workingWeekendDay.toString());
                    calendarYear.workingWeekendDays.add(workingWeekendDay);
                }
                calendarYear.workingWeeks.add(interval);
            } else if (line.contains(context.getResources().getString(R.string.midterm))) {
                if (DEBUG)
                    Log.d(TAG, "midtermWeeks, interval added: " + interval.toString());

                calendarYear.midtermWeeks.add(interval);
            } else if (line.contains(context.getResources().getString(R.string.examination_period))) {
                if (DEBUG)
                    Log.d(TAG, "examWeeks, interval added: " + interval.toString());

                calendarYear.examWeeks.add(interval);
            }
        }
        return calendarYear;
    }

    /**
     * @param context Context
     * @param input   String
     * @return
     */
    private static boolean containsWeekdays(Context context, String input) {
        return input.contains(context.getResources().getString(R.string.day_monday))
                || input.contains(context.getResources().getString(R.string.day_tuesday))
                || input.contains(context.getResources().getString(R.string.day_wenesday))
                || input.contains(context.getResources().getString(R.string.day_thursday))
                || input.contains(context.getResources().getString(R.string.day_friday))
                || input.contains(context.getResources().getString(R.string.day_saturday))
                || input.contains(context.getResources().getString(R.string.day_sunday));
    }

    /**
     * description example:  *Субота	* Настава за Уторак
     *
     * @param context     Context
     * @param description String
     * @return
     */
    private static String getWorkingDay(Context context, String description) {
        // description contains Субота
        if (description.contains(Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_SATURDAY))
                || description.contains(Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_SATURDAY))) {

            String day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_MONDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_MONDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_TUESDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_TUESDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_WEDNESDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_WEDNESDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_THURSDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_THURSDAY;
            }

            day = Resources.getDayOfTheWeekResource(context, ScheduleClass.DAY_FRIDAY);
            if (description.contains(day.substring(0, 3))) {
                return ScheduleClass.DAY_FRIDAY;
            }
        }
        return null;
    }

    protected static Timetable loadTimetableAll(Context context, String sectionName) {

        ArrayList<ScheduleClass> schedules = new ArrayList<>();
        if (Section.SECTION_1_MATEMATIKA.equals(sectionName)) {
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O1A, GROUP_1O1A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O1B, GROUP_1O1B));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O2A, GROUP_1O2A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O2B, GROUP_1O2B));
            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O3A, GROUP_1O3A));
            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O3B, GROUP_1O3B));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O4A, GROUP_1O4A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1O4B, GROUP_1O4B));

            schedules.addAll(loadTimetableAll(context, SCHEDULE_2MNVA, GROUP_2MNVA));
            schedules.addAll(loadTimetableAll(context, SCHEDULE_2MNVB, GROUP_2MNVB));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2LR1A, GROUP_2LR1A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2LR1B, GROUP_2LR1B));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2LR2A, GROUP_2LR2A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2LR2B, GROUP_2LR2B));

//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3L, GROUP_3L));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3M, GROUP_3M));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3N, GROUP_3N));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3R, GROUP_3R));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3V, GROUP_3V));
//
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_4L, GROUP_4L));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_4M, GROUP_4M));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_4N, GROUP_4N));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_4R, GROUP_4R));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_4V, GROUP_4V));

        } else if (Section.SECTION_2_INFORMATIKA.equals(sectionName)) {

//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1I1A, GROUP_1I1A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1I1B, GROUP_1I1B));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1I2A, GROUP_1I2A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1I2B, GROUP_1I2B));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1I3A, GROUP_1I3A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_1I3B, GROUP_1I3B));
//
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2I1A, GROUP_2I1A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2I1B, GROUP_2I1B));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2I2A, GROUP_2I2A));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_2I2B, GROUP_2I2B));
//
//
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3I1, GROUP_3I1));
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_3I2, GROUP_3I2));
//
//            schedules.addAll(loadTimetableAll(context, SCHEDULE_4I, GROUP_4I));
        }


        //TODO:
        Interval interval = new Interval(new Date(2020, 10, 5), new Date(2021, 1, 15));
        Timetable timetable = new Timetable(interval, schedules);
        return timetable;
    }

    /**
     * Loads schedule for given sectionName.
     *
     * @param context Context
     * @return
     */
    private static ArrayList<ScheduleClass> loadTimetableAll(Context context, String filePath, String group) {

        DEBUG = true;

        if (DEBUG)
            Log.d(TAG, "filePath: " + filePath);

        if (filePath == null) return null;

        String contentLoaded = loadContentFromAsset(context, filePath);

        if (contentLoaded == null) return null;

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");
        ArrayList<ScheduleClass> schedules = new ArrayList<>();

        String dayOfTheWeek = "";

        String time = "";
        String subject = "";
        String professor = "";
        String hall = "";

        for (String line : lines) {
            if (line.contains(context.getResources().getString(R.string.day_monday))) {
                dayOfTheWeek = ScheduleClass.DAY_MONDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_tuesday))) {
                dayOfTheWeek = ScheduleClass.DAY_TUESDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_wenesday))) {
                dayOfTheWeek = ScheduleClass.DAY_WEDNESDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_thursday))) {
                dayOfTheWeek = ScheduleClass.DAY_THURSDAY;
            } else if (line.contains(context.getResources().getString(R.string.day_friday))) {
                dayOfTheWeek = ScheduleClass.DAY_FRIDAY;
            } else if (line.contains(":") && line.contains("-") && time.isEmpty()) {
                time = line;
                if (DEBUG) {
                    Log.d(TAG, "schedule time: " + time);
                }
            } else if (!time.isEmpty() && subject.isEmpty()) {
                subject = line;
                if (DEBUG) {
                    Log.d(TAG, "schedule subject: " + subject);
                }
            } else if (!subject.isEmpty() && professor.isEmpty()) {
                professor = line;
                if (DEBUG) {
                    Log.d(TAG, "schedule professor: " + professor);
                }
            } else if (!professor.isEmpty() && hall.isEmpty()) {
                hall = line;
                if (DEBUG) {
                    Log.d(TAG, "schedule hall: " + hall);
                }
                if (DEBUG)
                    Log.d(TAG, "schedule dayOfTheWeek: " + dayOfTheWeek);

                ScheduleClass schedule = createSchedule(group, dayOfTheWeek, time, subject, professor, hall);
                time = subject = professor = hall = "";
                if (schedule != null) {
                    if (DEBUG)
                        Log.d(TAG, "schedule added: " + schedule);

                    schedules.add(schedule);
                }
            }
        }

        return schedules;
    }

    private static ScheduleClass createSchedule(String group, String dayOfTheWeek, String time,
                                                String subjectName, String professor, String hall) {

        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "createSchedule, subjectName: {" + subjectName + "}");

        int scheduleType = subjectName.contains("(вежбе)") ? ScheduleClass.TYPE_V : ScheduleClass.TYPE_P;
        if (subjectName.contains("(вежбе)")) {
            subjectName = subjectName.substring(0, subjectName.indexOf("(вежбе)"));
        }
        subjectName = subjectName.replaceAll("\\s+$", "");

        if (DEBUG)
            Log.d(TAG, "createSchedule, subjectName: {" + subjectName + "}");

        if (DEBUG)
            Log.d(TAG, "createSchedule, time: {" + time + "}");
        if (time.length() >= 12) {
            int tStart = Integer.parseInt(time.substring(0, 2));
            if (DEBUG)
                Log.d(TAG, "createSchedule, tStart: {" + tStart + "}");
            int tEnd = Integer.parseInt(time.substring(time.length() - 5, time.length() - 3));
            if (DEBUG)
                Log.d(TAG, "createSchedule, tEnd: {" + tEnd + "}");
            String timeStart = getTime(tStart);
            String timeEnd = getTime(tEnd);
            time = timeStart.substring(0, 2) + ":15-" + timeEnd;
        }
        if (DEBUG)
            Log.d(TAG, "createSchedule, time: {" + time + "}");

        hall = hall.replaceAll("\\s+$", "");
        if (DEBUG)
            Log.d(TAG, "createSchedule, hall: {" + hall + "}");

        professor = professor.replaceAll("\\s+$", "");
        if (DEBUG)
            Log.d(TAG, "createSchedule, professor: {" + professor + "}");

        return new ScheduleClass(dayOfTheWeek, subjectName, scheduleType, group, time, hall, professor);
    }

    /**
     * @param time int
     * @return
     */
    private static String getTime(int time) {
        switch (time) {
            case 8:
                return TIME_8;
            case 9:
                return TIME_9;
            case 10:
                return TIME_10;
            case 11:
                return TIME_11;
            case 12:
                return TIME_12;
            case 13:
                return TIME_13;
            case 14:
                return TIME_14;
            case 15:
                return TIME_15;
            case 16:
                return TIME_16;
            case 17:
                return TIME_17;
            case 18:
                return TIME_18;
            case 19:
                return TIME_19;
            case 20:
                return TIME_20;
            case 21:
                return TIME_21;
            case 22:
                return TIME_22;
        }
        return "";
    }

    /**
     * Loads subjects for given sectionName.
     *
     * @param context     Context
     * @param sectionName String
     * @return Section object
     */
    protected static Section loadSection(Context context, String sectionName, String subsection) {
        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "sectionName: " + sectionName);

        String filePath = null;
        if (Section.SECTION_1_MATEMATIKA.equals(sectionName)) {
            switch (subsection) {
                case Section.SUBSECTION_11_ML:
                    filePath = SUBJECTS_MATEMATIKA_ML;
                    break;
                case Section.SUBSECTION_12_MM:
                    filePath = SUBJECTS_MATEMATIKA_MM;
                    break;
                case Section.SUBSECTION_13_MR:
                    filePath = SUBJECTS_MATEMATIKA_MR;
                    break;
                case Section.SUBSECTION_14_MP:
                    filePath = SUBJECTS_MATEMATIKA_MP;
                    break;
                case Section.SUBSECTION_15_MS:
                    filePath = SUBJECTS_MATEMATIKA_MS;
                    break;
                case Section.SUBSECTION_16_MA:
                    filePath = SUBJECTS_MATEMATIKA_MA;
                    break;
            }
        } else if (Section.SECTION_2_INFORMATIKA.equals(sectionName)) {
            filePath = SUBJECTS_INFORMATIKA;
        } else if (Section.SECTION_3_AIA.equals(sectionName)) {
            switch (subsection) {
                case Section.SUBSECTION_31_AF:
                    filePath = SUBJECTS_MATEMATIKA_AF;
                    break;
                case Section.SUBSECTION_32_AI:
                    filePath = SUBJECTS_MATEMATIKA_AI;
                    break;
            }
        }

        if (filePath == null) return null;

        String contentLoaded = loadContentFromAsset(context, filePath);
        if (contentLoaded.contains("Двосеместрални предмети")) {
            return loadSectionTwoSemesterExams(sectionName, contentLoaded);
        }

        return loadSection(sectionName, contentLoaded);
    }

    protected static Section loadSection(String sectionName, String contentLoaded) {

        if (DEBUG)
            Log.d(TAG, "loadSection: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        Section section = new Section(sectionName);
        int semesterCounter = 1;
        boolean adding = false;
        AcademicYear academicYear = new AcademicYear(semesterCounter / 2 + semesterCounter % 2, semesterCounter);

        ElectiveSubjects electiveSubjects = null;

        for (String line : lines) {
            if (line.contains(".") && (line.contains("+") || line.contains("бар")) && semesterCounter <= 8) {
                if (DEBUG)
                    Log.d(TAG, "Adding subject: " + line);

                adding = true;
                academicYear.subjects.add(createSubjectObject(semesterCounter, line));

            } else if (semesterCounter <= 8) {

                if (adding) {
                    adding = false;
                    section.academicYears.add(academicYear);
                    semesterCounter++;
                    if (DEBUG)
                        Log.d(TAG, "New semester: " + semesterCounter);
                    if (semesterCounter <= 8)
                        academicYear = new AcademicYear(semesterCounter);

                    if (DEBUG)
                        Log.d(TAG, "New academicYear, semester: " + semesterCounter);
                } else {
                    if (DEBUG)
                        Log.d(TAG, "Skipping line: " + line);
                }
            } else if (line.contains("Изборни предмети") || line.contains("Изборни блок")) {
                if (DEBUG)
                    Log.d(TAG, "New electiveSubjects: " + line);

                if (electiveSubjects != null) {
                    section.electiveSubjects.add(electiveSubjects);
                }
                // removes spaces at the end and beginning
                line = line.replaceAll("\\s+$", "");
                electiveSubjects = new ElectiveSubjects(line);

            } else if (!line.isEmpty() && electiveSubjects != null) {
                if (DEBUG)
                    Log.d(TAG, "Adding subject: " + line);

                electiveSubjects.subjects.add(createSubjectObject(0, line));
            }
        }

        if (electiveSubjects != null) {
            section.electiveSubjects.add(electiveSubjects);
        }

        if (DEBUG)
            Log.d(TAG, "Section added: " + section.toString());

        return section;
    }

    protected static Section loadSectionTwoSemesterExams(String sectionName, String contentLoaded) {

        if (DEBUG)
            Log.d(TAG, "loadSection: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        Section section = new Section(sectionName);
        int year = 1, semester = 0, counter = 0;
        boolean adding = false;
        AcademicYear academicYear = null;

        ElectiveSubjects electiveSubjects = null;

        for (String line : lines) {
            if (academicYear == null) {
                if (line.contains("Двосеместрални предмети у 1. и 2. семестру")) {
                    semester = 0;
                    year = 1;
                    academicYear = new AcademicYear(year, semester);
                } else if (line.contains("Двосеместрални предмети у 3. и 4. семестру")) {
                    semester = 0;
                    year = 2;
                    academicYear = new AcademicYear(year, semester);
                } else if (line.contains(". Семестар")) {
                    counter++;
                    semester = counter;
                    year = counter / 2 + counter % 2;
                    academicYear = new AcademicYear(year, semester);
                }


                if (academicYear != null) {
                    if (DEBUG)
                        Log.d(TAG, "New semester: " + semester);

                    if (DEBUG)
                        Log.d(TAG, "New academicYear, year: " + year);
                }
            }

            if (academicYear != null && line.contains(".") && line.contains("+") && semester <= 8) {
                if (DEBUG)
                    Log.d(TAG, "Adding subject: " + line);

                adding = true;
                academicYear.subjects.add(createSubjectObject(semester, line));

            } else if (academicYear != null) {

                if (adding) {
                    adding = false;
                    section.academicYears.add(academicYear);
                    academicYear = null;
                } else {
                    if (DEBUG)
                        Log.d(TAG, "Skipping line: " + line);
                }
            } else if (line.contains("Изборни блок (група")) {
                if (DEBUG)
                    Log.d(TAG, "New electiveSubjects: " + line);

                if (electiveSubjects != null) {
                    section.electiveSubjects.add(electiveSubjects);
                }
                // removes spaces at the end and beginning
                line = line.replaceAll("\\s+$", "");
                electiveSubjects = new ElectiveSubjects(line);

            } else if (!line.isEmpty() && electiveSubjects != null) {
                if (DEBUG)
                    Log.d(TAG, "Adding subject: " + line);

                electiveSubjects.subjects.add(createSubjectObject(0, line));
            }
        }

        if (electiveSubjects != null) {
            section.electiveSubjects.add(electiveSubjects);
        }

        if (DEBUG)
            Log.d(TAG, "Section added: " + section.toString());

        return section;
    }

    /**
     * Creates Subject object form input String line.
     *
     * @param semester int
     * @param line     String
     * @return Subject object
     */
    private static Subject createSubjectObject(int semester, String line) {
        // get number
        String number = line.substring(0, line.indexOf('.'));

        // get subjectName
        int start = 0;
        {
            // finds all letters
            Pattern pattern = Pattern.compile("\\p{L}");
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                // finds first letter
                start = matcher.start();
            }
        }

        String subjectName = "";
        if (line.contains("+")) {
            subjectName = line.substring(start, line.indexOf('+') - 1);
        } else if (line.contains("бар")) {
            subjectName = line.substring(start, line.indexOf('а') - 1);
        }
        // removes spaces at the end and beginning
        subjectName = subjectName.replaceAll("\\s+$", "");
        if (DEBUG)
            Log.d(TAG, "createSchedule, subjectName: {" + subjectName + "}");

        int end = 0;
        if (line.contains("+")) {
            // finds all letters followed by space
            Pattern pattern = Pattern.compile("[\\p{L}[\\s]+]+");
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                // finds last space
                end = matcher.start();
            }
        }

        String semesterDescription = "";
        if (line.contains("+")) {
            semesterDescription = line.substring(line.indexOf('+') - 1, end);
        } else if (line.contains("бар")) {
            //TODO: rewrite
            semesterDescription = line.substring(line.indexOf('а') - 1, line.indexOf('6'));
        }
        // removes spaces at the end and beginning
        semesterDescription = semesterDescription.replaceAll("\\s+$", "");

        // get esp
        String esp = "";
        {
            // gets last digit
            Pattern pattern = Pattern.compile("[0-9]+$");
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                esp = matcher.group();
            }
        }

        return new Subject(Integer.parseInt(number), subjectName, semester, semesterDescription, "", Integer.parseInt(esp));
    }

    /**
     * Loads file from assets.
     *
     * @param context  Context
     * @param fileName String
     * @return text as String
     */
    private static String loadContentFromAsset(Context context, String fileName) {
        String text = "";
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            text = new String(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return text;
    }

    protected static ArrayList<Term> loadMidtermSchedules(Context context) {
        ArrayList<Term> listOfSchedulesObject = new ArrayList<>();

        Term term = loadMidtermSchedules(context, MIDTERM_FIRST, "Zimska kolokvijumska nedelja");
        if (term != null) listOfSchedulesObject.add(term);
        term = loadMidtermSchedules(context, MIDTERM_SECOND, "Letnja kolokvijumska nedelja");
        if (term != null) listOfSchedulesObject.add(term);

        return listOfSchedulesObject;
    }

    private static Term loadMidtermSchedules(Context context, String filePath, String description) {

        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "filePath: " + filePath);

        if (filePath == null) return null;

        description = CyrillicLatinConverter.latinToCyrillic(description);
        Term term = new Term(description);

        String contentLoaded = loadContentFromAsset(context, filePath);
        if (contentLoaded == null) return null;
        contentLoaded = CyrillicLatinConverter.latinToCyrillic(contentLoaded);

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        ArrayList<ScheduleExam> schedules = new ArrayList<>();
        SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE);

        boolean hasDay = false;
        java.util.Date date = null;

        String subject = "";
        String note = "";
        String time = "";
        String hall = "";

        for (String line : lines) {

            if (line.isEmpty()) continue;

            if (line.contains("ПОНЕДЕЉАК") || line.contains("УТОРАК") || line.contains("СРЕДА")
                    || line.contains("ЧЕТРВРТАК") || line.contains("ПЕТАК")
                    || line.contains("СУБОТА") || line.contains("НЕДЕЉА")) {
                date = null;
                hasDay = true;
            }

            if (!hasDay) continue;

            if (date == null) {
                // date 01.04.2018.
                String startInput = line.substring(line.indexOf(".") - 2, line.indexOf(".") + 9);
                if (DEBUG)
                    Log.d(TAG, "startInput: {" + startInput + "}");
                try {
                    date = parser.parse(startInput);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } else if (subject.isEmpty()) {
                if (DEBUG) {
                    Log.d(TAG, "schedule line: {" + line + "}");
                }
                if (line.contains("(")) {
                    subject = line.substring(0, line.indexOf("(") - 1);
                    note = line.substring(line.indexOf("("));
                    // removes spaces at the end and beginning
                    note = note.replaceAll("\\s+$", "");
                    if (note.contains("И")) {
                        // keep Roman numbers (I,II,III)
                        note = note.replace('И', 'I');
                    }
                    if (DEBUG)
                        Log.d(TAG, "note: {" + note + "}");
                } else {
                    subject = line;
                }
                subject = subject.replaceAll("\\s+$", "");
                if (DEBUG) {
                    Log.d(TAG, "schedule subject: {" + subject + "}");
                }
            } else if (!subject.isEmpty() && time.isEmpty() && line.contains("-")) {
                time = line;
                time = time.replaceAll("\\s+$", "");
                if (DEBUG) {
                    Log.d(TAG, "schedule time: {" + time + "}");
                }
            } else if (!time.isEmpty() && hall.isEmpty()) {
                hall = line;
                hall = hall.replaceAll("\\s+$", "");
                if (DEBUG) {
                    Log.d(TAG, "schedule hall: {" + hall + "}");
                }

                Date day = Date.getDate(date);
                if (DEBUG)
                    Log.d(TAG, "day: " + day);

                schedules.add(new ScheduleExam(day, subject, ScheduleExam.TYPE_M, note, time, hall));

                subject = time = hall = "";
            }
        }

        term.schedules = schedules;
        return term;
    }

    protected static ArrayList<Term> loadExamsSchedules(Context context) {
        ArrayList<Term> listOfSchedulesObject = new ArrayList<>();

        Term term = loadExamsSchedules(context, EXAMS_JAN_1, "Januar 1");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_JAN_2, "Januar 2");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_JUN_1, "Jun 1");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_JUN_2, "Jun 2");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_SEP_1, "Septembar 1");
        if (term != null) listOfSchedulesObject.add(term);

        term = loadExamsSchedules(context, EXAMS_SEP_2, "Septembar 2");
        if (term != null) listOfSchedulesObject.add(term);

        return listOfSchedulesObject;
    }

    private static Term loadExamsSchedules(Context context, String filePath, String description) {
        DEBUG = false;

        if (DEBUG)
            Log.d(TAG, "filePath: " + filePath);

        if (filePath == null) return null;

        description = CyrillicLatinConverter.latinToCyrillic(description);
        Term term = new Term(description);

        String contentLoaded = loadContentFromAsset(context, filePath);
        if (contentLoaded == null) return null;
        contentLoaded = CyrillicLatinConverter.latinToCyrillic(contentLoaded);

        if (DEBUG)
            Log.d(TAG, "contentLoaded: " + contentLoaded);

        String lines[] = contentLoaded.split("\\r?\\n");

        ArrayList<ScheduleExam> schedules = new ArrayList<>();

        SimpleDateFormat parser = new SimpleDateFormat(PATTERN_DATE);

        String previousLine = "";
        for (String line : lines) {
            if (DEBUG)
                Log.d(TAG, "previousLine: " + previousLine);
            if (DEBUG)
                Log.d(TAG, "line: " + line);

            if (!previousLine.isEmpty() && line.contains(".") && line.contains(" у ")) {
                String subject = "";
                String note = "";
                java.util.Date date = null;
                String time = "";
                if (previousLine.contains("(")) {
                    subject = previousLine.substring(0, previousLine.indexOf("(") - 1);
                    note = previousLine.substring(previousLine.indexOf("("));
                    // removes spaces at the end and beginning
                    note = note.replaceAll("\\s+$", "");
                    if (note.contains("И")) {
                        // keep Roman numbers (I,II,III)
                        note = note.replace('И', 'I');
                    }
                    if (DEBUG)
                        Log.d(TAG, "note: {" + note + "}");
                } else {
                    subject = previousLine;
                }
                subject = subject.replaceAll("\\s+$", "");
                if (DEBUG) {
                    Log.d(TAG, "schedule subject: {" + subject + "}");
                }

                // date 01.04.2018.
                String startInput = line.substring(line.indexOf(".") - 2, line.indexOf(".") + 9);
                if (DEBUG)
                    Log.d(TAG, "startInput: {" + startInput + "}");
                try {
                    date = parser.parse(startInput);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (date == null) continue;
                Date day = Date.getDate(date);
                if (DEBUG)
                    Log.d(TAG, "day: " + day);

                // time
                time = line.replaceAll("\\s+$", "");
                time = time.substring(time.length() - 5, time.length());
                if (DEBUG)
                    Log.d(TAG, "time: {" + time + "}");

                int type = ScheduleExam.TYPE_P;

                schedules.add(new ScheduleExam(day, subject, type, note, time, ""));
            } else {
                previousLine = line;
            }
        }

        term.schedules = schedules;
        return term;
    }
}
