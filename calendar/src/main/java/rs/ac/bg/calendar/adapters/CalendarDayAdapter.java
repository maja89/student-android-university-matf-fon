package rs.ac.bg.calendar.adapters;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applandeo.materialcalendarview.utils.DateUtils;
import com.applandeo.materialcalendarview.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import rs.ac.bg.calendar.R;
import rs.ac.bg.calendar.CalendarProperties;
import rs.ac.bg.calendar.model.CalendarDay;
import rs.ac.bg.calendar.model.EventDay;
import rs.ac.bg.calendar.utils.DayColorsUtils;

/**
 * Created by mcekic on 4/15/18.
 */

public class CalendarDayAdapter extends ArrayAdapter<Date> {
    public static final String TAG = "CalendarDayAdapter";

    private LayoutInflater mLayoutInflater;
    private int mPageMonth;
    private Calendar mSelectedDay = DateUtils.getCalendar();

    private List<CalendarDay> mCalendarDays = new ArrayList<>();

    private CalendarProperties mCalendarProperties;

    CalendarDayAdapter(Context context, CalendarProperties calendarProperties,
                       ArrayList<Date> dates, int pageMonth, List<CalendarDay> calendarDays) {
        super(context, calendarProperties.getItemLayoutResource(), dates);
        mCalendarProperties = calendarProperties;
        mPageMonth = pageMonth < 0 ? 11 : pageMonth;
        mLayoutInflater = LayoutInflater.from(context);
        mCalendarDays = calendarDays;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.calendar_day_view, parent, false);
        }

        RelativeLayout dayLayout = (RelativeLayout) view.findViewById(R.id.dayLayout);
        TextView dayLabel = (TextView) view.findViewById(R.id.dayLabel);
        ImageView dayIcon = (ImageView) view.findViewById(R.id.dayIcon);

        Calendar day = new GregorianCalendar();
        day.setTime(getItem(position));

        // Loading an image of the event
        if (dayIcon != null) {
            loadIcon(dayIcon, day);
        }

        // sets background as selected/unselected
        /*DayColorsUtils.setCurrentMonthDaySelectorColors(day, mSelectedDay, dayLayout);*/

        setLabelColors(dayLabel, day);

        dayLabel.setText(String.valueOf(day.get(Calendar.DAY_OF_MONTH)));

        return view;
    }

    private void setLabelColors(TextView dayLabel, Calendar day) {
        CalendarDay calendarDay = getCalendarDay(rs.ac.bg.calendar.model.Date.getDate(day));
        if (calendarDay != null) {
            // Setting not current month day color
            if (!isCurrentMonthDay(day)) {
                /*int argbColor = calendarDay.getBackgroundColor();
                int alpha = 55;
                int backgroundColor = Color.argb(alpha, Color.red(Color.red(argbColor)), Color.green(Color.green(argbColor)), Color.blue(Color.blue(argbColor)));
                DayColorsUtils.setSelectedDayColors(dayLabel, mCalendarProperties, backgroundColor);*/
                DayColorsUtils.setDayColors(dayLabel, mCalendarProperties.getAnotherMonthsDaysLabelsColor(), Typeface.NORMAL, R.drawable.background_transparent);
                return;
            }

            // Set view for all SelectedDays
            if (day.get(Calendar.MONTH) == mPageMonth) {
                DayColorsUtils.setCalendarDayColors(dayLabel, mCalendarProperties, calendarDay.getBackgroundColor());
                return;
            }

        }

        if (!isCurrentMonthDay(day)) {
            DayColorsUtils.setDayColors(dayLabel, mCalendarProperties.getAnotherMonthsDaysLabelsColor(), Typeface.NORMAL, R.drawable.background_transparent);
            return;
        }
        // Setting current month day color
        DayColorsUtils.setCurrentMonthDayColors(day, mSelectedDay, dayLabel, mCalendarProperties);
    }

    private CalendarDay getCalendarDay(rs.ac.bg.calendar.model.Date day) {
        for (CalendarDay calendarDay : mCalendarDays) {
            if (calendarDay.getDate().equals(day)) return calendarDay;
        }
        return null;
    }

    private boolean isCurrentMonthDay(Calendar day) {
        return day.get(Calendar.MONTH) == mPageMonth &&
                !((mCalendarProperties.getMinimumDate() != null && day.before(mCalendarProperties.getMinimumDate()))
                        || (mCalendarProperties.getMaximumDate() != null && day.after(mCalendarProperties.getMaximumDate())));
    }

    private void loadIcon(ImageView dayIcon, Calendar day) {
        if (mCalendarProperties.getEventDays() == null) {
            dayIcon.setVisibility(View.GONE);
            return;
        }

        for (EventDay eventDay : mCalendarProperties.getEventDays()) {
            if (eventDay.getCalendar().equals(day)) {
                ImageUtils.loadResource(dayIcon, eventDay.getImageResource());

                // If a day doesn't belong to current month then image is transparent
                /*if (!isCurrentMonthDay(day)) {
                    dayIcon.setAlpha(0.12f);
                }*/
                break;
            }
        }
    }
}
