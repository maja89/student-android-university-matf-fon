package rs.ac.bg.calendar.adapters;


import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.extensions.CalendarGridView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import rs.ac.bg.calendar.listeners.DayRowClickListener;
import rs.ac.bg.calendar.R;
import rs.ac.bg.calendar.CalendarProperties;
import rs.ac.bg.calendar.model.CalendarDay;
import rs.ac.bg.calendar.model.Date;

/**
 * Created by mcekic on 4/15/18.
 */

public class CalendarPageAdapter extends PagerAdapter {
    public static final String TAG = "CalendarPageAdapter";
    /**
     * A number of months (pages) in the calendar
     * 2401 months means 1200 months (100 years) before and 1200 months after the current month
     */
    private static final int CALENDAR_SIZE = 2401;

    private static final int WEEK_DAYS = 7;

    private Context mContext;
    private CalendarGridView mCalendarGridView;

    private CalendarProperties mCalendarProperties;

    private HashMap<Integer, ArrayList<Date>> mIntervalHashMap;

    public CalendarPageAdapter(Context context, CalendarProperties calendarProperties) {
        mContext = context;
        mCalendarProperties = calendarProperties;
        mIntervalHashMap = new HashMap<>();
    }

    public void addCalendarDates(ArrayList<Date> interval, int color) {
        mIntervalHashMap.put(color, interval);
    }

    @Override
    public int getCount() {
        return CALENDAR_SIZE;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mCalendarGridView = (CalendarGridView) inflater.inflate(R.layout.calendar_view_grid, null);

        loadMonth(position);
        DayRowClickListener dayRowClickListener = new DayRowClickListener(mCalendarProperties);
        mCalendarGridView.setOnItemClickListener(dayRowClickListener);

        container.addView(mCalendarGridView);
        return mCalendarGridView;
    }

    /**
     * This method fill calendar GridView with days
     *
     * @param position Position of current page in ViewPager
     */
    private void loadMonth(int position) {
        ArrayList<java.util.Date> days = new ArrayList<>();

        // Get Calendar object instance
        Calendar calendar = (Calendar) mCalendarProperties.getCurrentDate().clone();

        // Add months to Calendar (a number of months depends on ViewPager position)
        calendar.add(Calendar.MONTH, position);

        // Set day of month as 1
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        // Get a number of the first day of the week
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        // Count when month is beginning
        int monthBeginningCell = dayOfWeek + (dayOfWeek == 1 ? 5 : -2);

        // Subtract a number of beginning days, it will let to load a part of a previous month
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        /*
        Get all days of one page (42 = 6*7) is a number of all possible cells in one page
        (a part of previous month, current month and a part of next month))
         */
        while (days.size() < 6 * WEEK_DAYS) {
            days.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        List<CalendarDay> calendarDays = new ArrayList<>();
        for (Integer color : mIntervalHashMap.keySet()) {
            ArrayList<Date> dates = mIntervalHashMap.get(color);

            for (java.util.Date day : days) {
                Date date = Date.getDate(day);
                for (Date selectedDay : dates) {
                    // skip if not current month
                    if (date.month != selectedDay.month) continue;

                    if (date.equals(selectedDay)) {
                        calendarDays.add(new CalendarDay(date, color));
                    }
                }
            }
        }

        int pageMonth = calendar.get(Calendar.MONTH) - 1;
        CalendarDayAdapter calendarDayAdapter = new CalendarDayAdapter(mContext, mCalendarProperties, days, pageMonth, calendarDays);
        mCalendarGridView.setAdapter(calendarDayAdapter);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
