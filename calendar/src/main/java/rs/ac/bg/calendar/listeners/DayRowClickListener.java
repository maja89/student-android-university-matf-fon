package rs.ac.bg.calendar.listeners;

import android.view.View;
import android.widget.AdapterView;


import java.util.Calendar;
import java.util.GregorianCalendar;

import rs.ac.bg.calendar.CalendarProperties;
import rs.ac.bg.calendar.model.EventDay;

/**
 * Created by mcekic on 4/15/18.
 */

public class DayRowClickListener implements AdapterView.OnItemClickListener {
    public static final String TAG = "DayRowClickListener";

    private CalendarProperties mCalendarProperties;

    public DayRowClickListener(CalendarProperties calendarProperties) {
        mCalendarProperties = calendarProperties;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Calendar day = new GregorianCalendar();
        day.setTime((java.util.Date) adapterView.getItemAtPosition(position));

        if (mCalendarProperties.getOnDayClickListener() != null) {

            boolean enabledDay = isBetweenMinAndMax(day);
            if (enabledDay) {
                mCalendarProperties.getOnDayClickListener().onDayClick(new EventDay(day));
            }
        }
    }

    private boolean isBetweenMinAndMax(Calendar day) {
        return !((mCalendarProperties.getMinimumDate() != null && day.before(mCalendarProperties.getMinimumDate()))
                || (mCalendarProperties.getMaximumDate() != null && day.after(mCalendarProperties.getMaximumDate())));
    }
}
