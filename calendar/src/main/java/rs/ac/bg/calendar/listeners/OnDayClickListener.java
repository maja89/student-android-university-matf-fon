package rs.ac.bg.calendar.listeners;

import rs.ac.bg.calendar.model.EventDay;

/**
 * Created by mcekic on 5/1/18.
 */

public interface OnDayClickListener {
    void onDayClick(EventDay eventDay);
}
