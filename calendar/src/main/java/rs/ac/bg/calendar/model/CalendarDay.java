package rs.ac.bg.calendar.model;

import android.view.View;

/**
 * Created by mcekic on 4/15/18.
 */

public class CalendarDay {
    /**
     * View representing selected calendar cell
     */
    private View mView;

    private Date mDate;
    private int mBackgroundColor;

    public CalendarDay(Date date, int backgroundColor) {
        mDate = date;
        mBackgroundColor = backgroundColor;
    }

    /**
     * @return View representing selected calendar cell
     */
    public View getView() {
        return mView;
    }

    public void setView(View view) {
        mView = view;
    }

    /**
     * @return Date instance representing selected cell date
     */
    public Date getDate() {
        return mDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CalendarDay) {
            return getDate().equals(((CalendarDay) obj).getDate());
        }

        return super.equals(obj);
    }

    public int getBackgroundColor() {
        return mBackgroundColor;
    }
}
