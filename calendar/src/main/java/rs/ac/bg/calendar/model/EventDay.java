package rs.ac.bg.calendar.model;

import com.applandeo.materialcalendarview.utils.DateUtils;

import java.util.Calendar;

/**
 * Created by mcekic on 5/1/18.
 */

public class EventDay {
    private Calendar mDay;
    private int mImageResource;

    /**
     * @param day Calendar object which represents a date of the event
     */
    public EventDay(Calendar day) {
        mDay = day;
    }


    /**
     * @param day           Calendar object which represents a date of the event
     * @param imageResource Resource of an image which will be displayed in a day cell
     */
    public EventDay(Calendar day, int imageResource) {
        DateUtils.setMidnight(day);
        mDay = day;
        mImageResource = imageResource;
    }


    /**
     * @return An image resource which will be displayed in the day row
     */
    public int getImageResource() {
        return mImageResource;
    }


    /**
     * @return Calendar object which represents a date of current event
     */
    public Calendar getCalendar() {
        return mDay;
    }

}
